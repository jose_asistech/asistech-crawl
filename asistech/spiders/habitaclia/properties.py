# -*- coding: utf-8 -*-

import scrapy

from asistech.utils import get_spanish_provinces
from asistech.items import HabitacliaPropertyItemLoader, HabitacliaAgencyItemLoader
from asistech.spiders.habitaclia.agencies import TRANSACTIONS_MAP
from pkg_resources import resource_filename


LISTING_URL_TEMPLATE = 'http://www.habitaclia.com/{}-{}-en-{}/listapoblaciones.htm'

TRANSACTIONS_TYPES = {
    # 'obra_nueva',
    'comprar',
    'alquiler',
    'alquiler_temporada',
    'traspaso',
}

REAL_STATES_TYPES = {
    'vivienda',
    'oficina',
    'local_comercial',
    'industrial',
    'terrenos_y_solares',
    'aparcamiento',
    'inversiones',
    'inmuebles_singulares',
    'negocio',
}

FEATURES_MAP = {
    u'Superficie: ([\d.,]*)': 'total_build_size',
    u'([\d.,]*) habitaci(ón|ones)': 'n_rooms',
    u'([\d.,]*) baños?': 'n_bathrooms',
    u'([\d.,]*) aseos?': 'n_washrooms',
    u'Ascensor: (\w*)': 'elevator',
    u'Número de planta: (\w*)': 'floor',
    u'Piscina comunitaria: (\w*)': 'swimming_pool',
    u'Piscina particular: (\w*)': 'swimming_pool',
    u'Zonas? ajardinadas?: (\w*)': 'garden',
    u'Jardín: (.*)': 'garden',
    u'Parking: (\w*)': 'parking',
    u'Aparcamiento: (\w*)': 'parking',
    u'Amueblado: (\w*)': 'furnished',
    u'Aire acondicionado: (\w*)': 'air_conditioning',
    u'Calefacción: (\w*)': 'heating',
    u'Terraza: (.*)': 'balcony',
    u'Chimenea: (\w*)': 'fireplace',
    u'Cerca de transporte público: (\w*)': 'near_public_transportation',
    u'Vistas al mar: (\w*)': 'view_over_the_sea',
    u'Vistas a la montaña: (\w*)': 'view_over_the_mountains',
    u'Vistas a la ciudad: (\w*)': 'view_over_the_city',
    u'Equipamientos deportivos: (\w*)': 'sports_facilities',
    u'Año de construcción: (\w*)': 'construction_year',
    u'Vigilancia: (\w*)': 'security_guard',
}

JAVASCRIPT_VARS_MAP = {
    'oHab.geo.nomProv = "(.*)";': 'location_provincia',
    'oHab.geo.nomCom = "(.*)";': 'location_comarca',
    'oHab.geo.nomArea = "(.*)";': 'location_area',
    'oHab.geo.nomPob = "(.*)";': 'location_municipio',
    'oHab.geo.nomDistrito = "(.*)";': 'location_distrito',
    'oHab.geo.nomZona = "(.*)";': 'location_barrio',
    'oHab.nomTipOp = "(.*)";': 'transaction_type',
    'oHab.nomTipInm = "(.*)";': 're_type',
    'oHab.nomTipInm2 = "(.*)";': 'subtype',
    'VGPSLat: parseFloat\((.*)\),': 'latitude',
    'VGPSLon: parseFloat\((.*)\),': 'longitude',
}


class HabitacliaPropertiesSpider(scrapy.Spider):
    name = 'habitaclia-properties'
    custom_settings = {
        'SPIDERMON_VALIDATION_SCHEMAS': [
            resource_filename('asistech', 'spidermon/schemas/habitaclia-properties.json')
        ],
    }

    def __init__(self, *args, **kwargs):
        super(HabitacliaPropertiesSpider, self).__init__(*args, **kwargs)

        start_urls = kwargs.get('urls', self.start_urls)
        if not isinstance(start_urls, list):
            start_urls = start_urls.split(',')
        self.start_urls = start_urls

        provinces = kwargs.get('provinces', [])
        if not isinstance(provinces, list):
            provinces = provinces.split(',')
        if not provinces:
            provinces = get_spanish_provinces()
        self.provinces = provinces

        excluded_provinces = kwargs.get('excluded-provinces', [])
        if not isinstance(excluded_provinces, list):
            excluded_provinces = excluded_provinces.split(',')
        self.provinces = set(self.provinces) - set(excluded_provinces)

    def start_requests(self):
        for req in super(HabitacliaPropertiesSpider, self).start_requests():
            yield req.replace(callback=self.parse_lista_inmuebles)

        if not self.start_urls:
            for trans in TRANSACTIONS_TYPES:
                for inm in REAL_STATES_TYPES:
                    for prov in self.provinces:
                        url = LISTING_URL_TEMPLATE.format(trans, inm, prov)
                        yield scrapy.Request(url,
                                             dont_filter=True,
                                             callback=self.parse_lista_poblaciones)

    def parse_lista_poblaciones(self, response):
        for pobl in response.xpath('//div[@id="enlacespoblacion"]/ul//@href').extract():
            yield scrapy.Request(pobl, callback=self.parse_lista_inmuebles)

    def parse_lista_inmuebles(self, response):
        for inm in response.xpath('//div[@class="listainmuebles"]/ul[@class="enlista"][1]//h3/a/@href').extract():
            headers = {
                'Referer': inm,
            }
            # Modify the URL to get additional info (phone number)
            url = inm + '&op=tlf'
            meta = {
                'property_url': inm,
            }
            yield scrapy.Request(
                url,
                callback=self.parse_inmueble,
                headers=headers,
                meta=meta,
            )

        next_link = response.xpath('//div[@class="paginacionlista"]//a[contains(.,"Siguiente")]')
        if next_link:
            yield scrapy.Request(next_link.xpath('@href').extract_first(), callback=self.parse_lista_inmuebles)

    def parse_inmueble(self, response):
        il = HabitacliaPropertyItemLoader(response=response)
        il.add_value('ad_external_id', response.meta['property_url'], re='i(\d*)')
        il.add_value('ad_url', response.meta['property_url'])
        il.add_value('reference_id', response.meta['property_url'], re='i(\d*)')
        il.add_css('ad_title', 'h1')
        scraped_seller_url = response.xpath('//a[@class="verallads"]/@href').extract_first()
        if scraped_seller_url:
            il.add_value('seller_url', response.urljoin(scraped_seller_url))
        il.add_value('seller_reference', il.get_output_value('seller_url'), re='inmobiliaria-(.*)/')

        seller_url = il.get_output_value('seller_url')
        il.add_value('seller_type', 'Profesional' if seller_url else 'Particular')

        for var, field in JAVASCRIPT_VARS_MAP.items():
            il.add_xpath(field, '//script', re=var)

        xpath = '//div[@class="caracteristicas"]//li'
        for feature, field in FEATURES_MAP.items():
            il.add_xpath(field, xpath, re=feature)

        il.add_xpath('description', '//div[@itemprop="description"]/p')
        il.add_xpath('address', '//div[contains(@class,"dir_ex")]')
        il.add_xpath('price_down', '//div[contains(@class,"mod-precio")]/strong', re='\d[\d.]*')
        il.add_xpath('images', '//a[@id="verfotos"]/text()')
        il.add_xpath('energetic_certification', '//ul[@class="efienergetica"]')
        il.add_xpath('energetic_certification',
                     '//div[@class="caracteristicas"]',  # it's not inside a li
                     re=u'Calificación energética: (.*)')

        il.add_value('total_usable_size', il.get_output_value('total_build_size'))

        il.add_xpath('promotion_url', '//a[@class="promodatos"]/@href')
        if il.get_output_value('promotion_url'):
            il.add_value('state', 'Obra nueva')

        il.add_xpath('price', '//span[@itemprop="price"]', re='\d[\d.]*')
        if il.get_output_value('price'):
            il.add_value('currency', u'€')

        il.add_css('update_date', 'span.actualizado')
        update_date = il.get_output_value('update_date')
        if update_date:
            il.add_value('posting_date_range', update_date, re='\d+/\d+/\d+')

        il.add_xpath('phone', '//span[@name="telf"]//text()')

        item = il.load_item()

        if seller_url:
            headers = {
                'Referer': response.meta['property_url'],
            }
            meta = {
                'phone': item.get('phone'),
            }
            yield scrapy.Request(
                seller_url,
                callback=self.parse_agency,
                headers=headers,
                meta=meta,
            )

        # Check required fields
        if item.get('ad_external_id'):
            yield item

    def parse_agency(self, response):
        for office in response.css('select#oficinas ::attr(value)').extract():
            url, _, _ = office.partition('listacliente.htm')
            # we're relying on dupefilter to avoid infinite requests here
            yield scrapy.Request(
                url, callback=self.parse_agency, meta=response.meta
            )

        il = HabitacliaAgencyItemLoader(response=response)
        il.add_value('url', response.url)
        il.add_value('external_id', response.url, re='inmobiliaria-(.*?)(?:/|$)')
        il.add_css('seller_name', 'h1')
        il.add_xpath('address', '//div[@id="dirOfi"]/text()[1]')
        il.add_xpath('city', '//div[@id="dirOfi"]/text()[2]', re='(.*) \(')
        il.add_css('city', 'h2', re='Empresa inmobiliaria en (.*)')
        il.add_value('zip', response.meta.get('zipcode'))
        il.add_value('region', response.meta.get('province'))
        il.add_xpath('region', '//div[@id="dirOfi"]/text()[2]', re='\(([^(]*)\)$')
        il.add_xpath('website', '//div[@class="paginaWebMinisite"]/a/@href')

        _tab_xpath = '//ul[contains(@class,"pestanas")]/li[contains(a, "{}")]//span'
        for name, field in TRANSACTIONS_MAP.items():
            il.add_xpath(field, _tab_xpath.format(name))
            il.add_value(field, '0')

        il.add_value('phone', response.meta['phone'])

        output = il.load_item()
        website = output.get('website')
        if website:
            if website == '#':
                # Link generated dynamically using javascript
                office_name = response.xpath('//a[@data-nomofi]/@data-nomofi').extract_first()
                if office_name:
                    website = 'http://www.habitaclia.com/hab_inmuebles/clickurl.asp?nom_ofi_w=' + office_name

            if website != '#':
                output = scrapy.Request(
                    website,
                    callback=self.parse_website,
                    errback=self._errback_website,
                    encoding=response.encoding,
                    meta={'item': output},
                )
            else:
                output['website'] = ''
        yield output

    def _errback_website(self, failure):
        request = failure.request
        item = request.meta['item']

        yield item

    def parse_website(self, response):
        item = response.meta.get('item')
        il = HabitacliaAgencyItemLoader(item=item, response=response)
        il.replace_xpath('website', '//script', re='window.location.href = "(.*)"')
        return il.load_item()
