# -*- coding: utf-8 -*-

# Scrapy settings for asistech project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html
import os

from fake_useragent import settings as fk_ua_settings

BOT_NAME = 'asistech'

SPIDER_MODULES = ['asistech.spiders']
NEWSPIDER_MODULE = 'asistech.spiders'

PROJECT_NAME = 'Asistech'

scrapy_job_id = os.environ.get('SCRAPY_JOB', '')
if '22080' in scrapy_job_id:
    env = 'dev'
    SPIDERMON_SENTRY_FAKE = True
    PROJECT_ID = '22080'
elif '18194' in scrapy_job_id:
    env = 'production'
    PROJECT_ID = '18194'
else:
    SPIDERMON_SENTRY_FAKE = False
    env = 'local'
    PROJECT_ID = '22080'

SHUB_APIKEY = 'e346938de57441e78666a2be4cb2f967'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mediapartners-Google/2.1'

# Configure maximum concurrent requests performed by Scrapy (default: 16)
# CONCURRENT_REQUESTS=8

# Configure a delay for requests for the same website (default: 0)
# See http://scrapy.readthedocs.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#DOWNLOAD_DELAY=3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN=16
#CONCURRENT_REQUESTS_PER_IP=16

# Disable cookies (enabled by default)
COOKIES_ENABLED=False
DUPEFILTER_DEBUG=True

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED=False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html
SPIDER_MIDDLEWARES = {
  'asistech.middlewares.Exceptions': 1000,
}

# Enable or disable downloader middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
  'asistech.middlewares.CustomRetryMiddleware': 500,
  'scrapy.downloadermiddlewares.retry.RetryMiddleware': None,
}

SPIDERMON_ENABLED = True

# Enable or disable extensions
# See http://scrapy.readthedocs.org/en/latest/topics/extensions.html
EXTENSIONS = {
    'spidermon.contrib.scrapy.extensions.Spidermon': 500,
}

# Configure item pipelines
# See http://scrapy.readthedocs.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    'asistech.pipelines.InsertDatePipeline': 300,
    'asistech.pipelines.DiscardItemsWithoutFields': 500,
    'asistech.pipelines.DiscardUndesiredVehicleItems': 510,
    'asistech.pipelines.DefaultValuePipeline': 900,
    'asistech.pipelines.FlattenListPipeline': 1000,
    'spidermon.contrib.scrapy.pipelines.ItemValidationPipeline': 800,
}

SPIDERMON_SPIDER_CLOSE_MONITORS = (
    'asistech.spidermon.monitors.SpiderCloseMonitorSuite',
)

SPIDERMON_VALIDATION_ADD_ERRORS_TO_ITEMS = False
SPIDERMON_VALIDATION_ERRORS_FIELD = 'validation_error'

# will be posted to https://sentry.io/organizations/scrapinghub/issues/?project=1444827
SPIDERMON_SENTRY_DSN = 'https://a005e028f34f4dc78cbdafc01625259a@sentry.io/1444827'
SPIDERMON_SENTRY_PROJECT_NAME = PROJECT_NAME
SPIDERMON_SENTRY_ENVIRONMENT_TYPE = env.capitalize()



# Enable and configure the AutoThrottle extension (disabled by default)
# See http://doc.scrapy.org/en/latest/topics/autothrottle.html
# NOTE: AutoThrottle will honour the standard settings for concurrency and delay
AUTOTHROTTLE_ENABLED=True
# The initial download delay
#AUTOTHROTTLE_START_DELAY=5
# The maximum download delay to be set in case of high latencies
AUTOTHROTTLE_MAX_DELAY=60
# Enable showing throttling stats for every response received:
AUTOTHROTTLE_DEBUG=False

# Enable and configure HTTP caching (disabled by default)
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED=True
#HTTPCACHE_EXPIRATION_SECS=0
#HTTPCACHE_DIR='httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES=[]
#HTTPCACHE_STORAGE='scrapy.extensions.httpcache.FilesystemCacheStorage'

DEFAULT_RESPONSE_ERROR_THRESHOLD = 1

NORMAL_ITEM_COVERAGE_PERCENTAGE = 80


# Custom settings
DEFAULT_FIELD_VALUE = ''
INSERT_DATE_FORMAT = '%d/%m/%Y %H:%M:%S'
INSERT_DATE_FIELD = 'insert_date'
FLATTENED_LIST_FIELDS = ['phone', 'name_cperson', 'position_cperson', 'phone_cperson']
MAX_FLATTENED_FIELD_COUNT = {'phone': 5, 'name_cperson': 3, 'position_cperson': 3, 'phone_cperson': 3}

if os.getenv('SCRAPY_ENV') == 'local':
    HTTPCACHE_ENABLED = True
    HTTPCACHE_IGNORE_HTTP_CODES=[405, 503, 502]

RETRY_HTTP_CODES = [500, 502, 503, 504, 522, 524, 408, 429, 405]
#################################################################
#           fake user agent settings override                   #
#################################################################
fk_ua_settings.HTTP_RETRIES = 5
fk_ua_settings.HTTP_TIMEOUT = 10
fk_ua_settings.HTTP_DELAY = 5

FAKEUSERAGENT_FALLBACK = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'

#################################################################

try:
    from local_settings import *
except ImportError:
    pass