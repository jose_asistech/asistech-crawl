# -*- coding: utf-8 -*-

from scrapy.extensions.throttle import AutoThrottle


class CustomAutoThrottle(AutoThrottle):

    def _adjust_delay(self, slot, latency, response):
        """Define delay adjustment policy"""
        # # If latency is bigger than old delay, then use latency instead of mean.
        # # It works better with problematic sites
        # new_delay = min(max(self.mindelay, latency, (slot.delay + latency) / 2.0), self.maxdelay)

        # # Dont adjust delay if response status != 200 and new delay is smaller
        # # than old one, as error pages (and redirections) are usually small and
        # # so tend to reduce latency, thus provoking a positive feedback by
        # # reducing delay instead of increase.
        # if response.status == 200 or new_delay > slot.delay:
        #     slot.delay = new_delay

        if response.status == 200:
            slot.delay = 0.5
        else:
            # Ban
            slot.delay = 45.0
