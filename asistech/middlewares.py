# -*- coding: utf-8 -*-
import copy

from scrapy.downloadermiddlewares.retry import RetryMiddleware
from scrapy.utils.response import response_status_message
from scrapy.xlib.tx import ResponseFailed
from scrapy.exceptions import IgnoreRequest
from scrapy.core.downloader.handlers.http11 import TunnelError
from twisted.internet import defer
from twisted.internet.error import TimeoutError, DNSLookupError, \
        ConnectionRefusedError, ConnectionDone, ConnectError, \
        ConnectionLost, TCPTimedOutError

from asistech.exceptions import RetryRequest
from scrapy_fake_useragent.middleware import RandomUserAgentMiddleware
from fake_useragent import UserAgent

HTTP_PROXY = 'http://46.183.116.113:8123'  # Default proxy


class ProxyMiddleware(object):

    def __init__(self, server=None):
        self.server = server or HTTP_PROXY
        self.debug_enabled = True

    @classmethod
    def from_crawler(cls, crawler):
        server = crawler.settings.get('HTTPPROXY_SERVER')
        return cls(server=server)

    def process_request(self, request, spider):
        if self.debug_enabled:
            spider.logger.info("Routing requests through this proxy: %s", self.server)
            # Disable the debug messages after the first one
            self.debug_enabled = False

        request.meta['proxy'] = self.server


class CustomRetryMiddleware(RetryMiddleware):

    def process_request(self, request, spider):
        exc = request.meta.pop('retry_exception', None)
        if exc:
            reason = '{.__class__.__name__} exception'.format(exc)
            if exc.message:
                reason = '{}: {}'.format(reason, exc.message)
            retry_request = self._retry(request, reason, spider)
            if not retry_request:
                raise IgnoreRequest
            return retry_request


class FixedRetryMiddleware(CustomRetryMiddleware):

    # IOError is raised by the HttpCompression middleware when trying to
    # decompress an empty response
    EXCEPTIONS_TO_RETRY = (defer.TimeoutError, TimeoutError, DNSLookupError,
                           ConnectionRefusedError, ConnectionDone, ConnectError,
                           ConnectionLost, TCPTimedOutError, ResponseFailed,
                           IOError, TunnelError)

    def process_response(self, request, response, spider):
        if request.meta.get('dont_retry', False):
            return response
        if response.status in self.retry_http_codes:
            try:
                # This function is broken in Scrapy 1.0.5
                reason = response_status_message(response.status)
            except AttributeError:
                reason = "Unknown Status"
            return self._retry(request, reason, spider) or response
        return response


class Exceptions(object):
    """Catch errors from spider and handle them here."""

    def process_spider_output(self, response, result, spider):
        """Process spider output and catch errors from spider if any.

        Bug - process_spider_exception() not invoked for generators:
        https://github.com/scrapy/scrapy/issues/220

        """
        try:
            for x in result:
                yield x
        except Exception as exc:
            exc_result = self.process_spider_exception(response, exc, spider)
            if exc_result is None:
                # Reraise catched exception
                raise
            for x in exc_result:
                yield x

    def process_spider_exception(self, response, exception, spider):
        """This method used if an exception was caught in spider's response
        handler, but only if it is not implemented as generator. Thus
        we should explicitly call it in process_spider_output() method.

        Bug - process_spider_exception() not invoked for generators:
        https://github.com/scrapy/scrapy/issues/220

        """
        if isinstance(exception, RetryRequest):
            return self.process_retry_exception(response, exception, spider)

        # Return None to continue exception processing
        return None

    def __deepcopy_or_copy(self, value_orig, request, spider):
        """Deepcopy mutable object with fallback to copy."""
        try:
            value = copy.deepcopy(value_orig)
        except Exception as e:
            spider.logger.warning(
                u'Cannot deepcopy %s, got exception %s %s',
                value_orig, e, request,
                exc_info=True,
            )
            value = copy.copy(value_orig)
        return value

    def process_retry_exception(self, response, exception, spider):
        # merge new meta, cookies and headers with existing ones instead
        # of overriding original values.
        request = exception.request or response.request
        # cannot use deepcopy here because kwargs could contain callback
        # if it is implemented as generator it raises error:
        # TypeError: object.__new__(generator) is not safe, use generator.__new__()
        kwargs = exception.kwargs.copy()
        for kw in ['meta', 'cookies', 'headers']:
            if kw in exception.kwargs.keys():
                value = self.__deepcopy_or_copy(
                    getattr(request, kw), request, spider)
                value_to_merge = self.__deepcopy_or_copy(
                    exception.kwargs[kw], request, spider)
                value.update(value_to_merge)
                kwargs[kw] = value
        request = request.replace(**kwargs)
        # retry_exception indicates that this request should be
        # processed by CustomRetryMiddleware
        request.meta['retry_exception'] = exception
        # workaround for correct depth handling
        if 'depth' in request.meta:
            request.meta['depth'] -= 1
        # latency will be calculated for this request
        request.meta.pop('download_latency', None)
        # don't use cached response
        request.headers['Cache-Control'] = 'no-cache'
        # in other case request will be filtered by scheduler
        request.dont_filter = True
        yield request


class CustomRandomUserAgentMiddleware(RandomUserAgentMiddleware):
    def __init__(self, crawler):
        super(RandomUserAgentMiddleware, self).__init__()
        fallback = crawler.settings.get('FAKEUSERAGENT_FALLBACK', None)
        self.ua = UserAgent(fallback=fallback, verify_ssl=False)
        self.per_proxy = crawler.settings.get('RANDOM_UA_PER_PROXY', False)
        self.ua_type = crawler.settings.get('RANDOM_UA_TYPE', 'random')
        self.proxy2ua = {}