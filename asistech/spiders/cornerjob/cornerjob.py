# -*- coding: utf-8 -*-
import json
import os
import csv
import datetime
import StringIO

from scrapy import Spider, Request

from asistech.utils import str_to_bool
from pkg_resources import resource_filename


QUERY_OBJECT = {
    'experienceNotRequired': False,
    'searchString': '',
    'reset': True,
    'numElements': 30,
    'page': 0,
    'external': False,
    'donutIdx': 0,
    'latitude': 40.416702,
    'longitude': -3.70374,
}


def get_date_from_timestamp(timestamp_ms):
    if not timestamp_ms:
        return ''

    dt = datetime.datetime.fromtimestamp(timestamp_ms/1000)
    return dt.strftime("%Y/%m/%d")


class CornerjobSpider(Spider):
    name = 'cornerjob'
    allowed_domains = [
        'cornerjob.com',
    ]
    custom_settings = {
        'COOKIES_ENABLED': False,
        'REFERER_ENABLED': False,
        'USER_AGENT': 'Parse Android SDK 1.13.0 (com.clapp.jobs/58) API Level 23',
        'DEFAULT_REQUEST_HEADERS': {
            'Accept-Encoding': 'gzip',
            'Accept-Language': 'es-ES',
            'Host': 'api-c.cornerjob.com',
            'Content-Type': 'application/json; charset=UTF-8',
            'X-Parse-Application-Id': '2XmKD3kR9UoE4J5JVxxTOYAoNOqoqByeHl7Vid7W'
        },
        'CONCURRENT_REQUESTS': 1,
        'DOWNLOAD_DELAY': 2,
        # 'CLOSESPIDER_PAGECOUNT': 1000,
        'ITEM_PIPELINES': {
            'asistech.pipelines.DuplicatesPipeline': 200,
            'asistech.pipelines.InsertDatePipeline': 300,
        },
        'ID_FIELD': 'objectId',
        'INSERT_DATE_FIELD': 'crawl_date',
        'SPIDERMON_VALIDATION_SCHEMAS': [
            resource_filename('asistech', 'spidermon/schemas/cornerjob.json')
        ],
    }

    def __init__(self, *args, **kwargs):
        self.filter_capitals = str_to_bool(kwargs.pop('capitals', 'False'))
        self.multiple_areas_in_city = str_to_bool(kwargs.pop('extended_city', 'False'))
        self.max_pages_per_location = int(kwargs.pop('pages', '1000'))

        super(CornerjobSpider, self).__init__(*args, **kwargs)

    def _create_page_request(self, location, page=0):
        url = 'https://api-c.cornerjob.com/wall/getWallOffers'
        meta = {
            'page': page,
            'location': location
        }
        body = QUERY_OBJECT.copy()
        body['latitude'] = float(location['lat'])
        body['longitude'] = float(location['lng'])
        body['page'] = page

        return Request(
            url,
            method='POST',
            body=json.dumps(body),
            meta=meta,
            callback=self.parse
        )

    def start_requests(self):
        source_path = os.path.join(os.path.dirname(__file__), '..', '..', 'resources', 'spain.csv')
        with open(source_path, 'rb') as f:
            data = f.read()

        buff = StringIO.StringIO(data)
        # reader = unicode_csv_reader(buff)
        reader = csv.reader(buff)
        first_row = True
        locations = []
        for row in reader:
            if first_row:
                first_row = False
                continue

            loc = {
                'city': row[2],
                'lat': row[3],
                'lng': row[4],
                'population': row[6],
                'capital': (row[9].strip().lower() == 'yes'),
            }
            locations.append(loc)

        capitals = [l for l in locations if l['capital']]
        extra_areas_in_capitals = []
        for capital in capitals:
            for lat_offset in [-0.10, -0.05, 0, 0.05, 0.10]:
                for lng_offset in [-0.10, -0.05, 0, 0.05, 0.10]:
                    loc = {
                        'city': capital['city'],
                        'lat': str(float(capital['lat']) + lat_offset),
                        'lng': str(float(capital['lng']) + lng_offset),
                        'population': capital['population'],
                        'capital': True,
                    }
                    extra_areas_in_capitals.append(loc)

        if self.filter_capitals:
            locations = capitals

        if self.multiple_areas_in_city:
            locations.extend(extra_areas_in_capitals)

        for location in locations:
            yield self._create_page_request(location)

    def parse(self, response):
        data = json.loads(response.body_as_unicode())

        for record in data.get('results', []):
            item = {}
            try:
                item['city'] = record.get('city', '')
                item['className'] = record.get('typeOffer')
                item['country'] = record['country']
                item['description'] = record.get('description', '')
                item['enabled'] = record.get('status', '')
                item['fromCompany_city'] = record['company'].get('city', '')
                item['fromCompany_className'] = record['company'].get('aboutCompany')
                item['fromCompany_companyName'] = record['company'].get('companyName')
                item['fromCompany_country'] = record['company'].get('country', '')
                item['fromCompany_location_latitude'] = record['company'].get('latitude', '')
                item['fromCompany_location_longitude'] = record['company'].get('longitude', '')
                item['fromCompany_objectId'] = record['company']['objectId']
                item['fromCompany_profilePicThumb_url'] = record['company'].get('companyPictureURL', '')
                item['fromCompany_region'] = record['company'].get('region', '')
                item['fromCompany_type'] = record['company'].get('companyCreatedOffersCount', '')
                item['fromCompany_createdAt'] = get_date_from_timestamp(record['company']['companyCreatedAt'])
                item['fromCompany_updatedAt'] = get_date_from_timestamp(record['company']['updatedAt'])
                item['sector_objectId'] = record.get('sectorId')
                item['locationGP_latitude'] = record['latitude']
                item['locationGP_longitude'] = record['longitude']
                item['locationString'] = record.get('locationString', '')
                item['objectId'] = record['objectId']
                item['region'] = record['region']
                item['picture_url'] = record.get('pictureURL', '')

                item['createdAt'] = get_date_from_timestamp(record['createdAt'])
                item['updatedAt'] = get_date_from_timestamp(record['updatedAt'])
                item['curl_data'] = '"latitude":{lat},"longitude":{lng},"page":{pg}'.format(
                    lat=response.meta.get('location', {}).get('lat', 0),
                    lng=response.meta.get('location', {}).get('lng', 0),
                    pg=response.meta.get('page', 0)
                )
                yield item
            except KeyError:
                self.logger.warning('Missing key: %s', record)

        next_page = response.meta.get('page', 0) + 1
        if next_page < self.max_pages_per_location:
            max_available_pages = data.get('hits', {}).get('totalPages', 0)
            if next_page < max_available_pages:
                location = response.meta['location']
                yield self._create_page_request(location, page=next_page)

            city = response.meta['location']['city']
            total_hits = data.get('hits', {}).get('total', 0)
            self.logger.debug("Location: %s, total_hits: %s", city, total_hits)


