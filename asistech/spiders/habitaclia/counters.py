# -*- coding: utf-8 -*-
import json

import scrapy

from asistech.items import HabitacliaCounterItemLoader
from asistech.utils import get_spanish_provinces, get_habitaclia_url
from pkg_resources import resource_filename


LISTING_URL_TEMPLATE = 'http://www.habitaclia.com/{}-{}-en-{}/listapoblaciones.htm'
TAB_PER_TRANSACTION_x = ('//div[@class="menu-listado"]'
                         '//li[descendant-or-self::*[@data-tipop="{}"]]')

TRANSACTIONS_TYPES = {
    # 'obra_nueva': 'Obra Nueva',
    'comprar': 'Comprar',
    'alquiler': 'Alquiler',
    'alquiler_temporada': 'Alquiler de temporada',
    'traspaso': 'Traspaso',
}

REAL_STATES_TYPES = {
    'vivienda',
    'oficina',
    'local_comercial',
    'industrial',
    'terrenos_y_solares',
    'aparcamiento',
    'inversiones',
    'inmuebles_singulares',
    'negocio',
}

URLS_VARS_MAP = {
    # 'transaction_type': 'oHab.nomTipOpBuscador = "(.*)";',
    # 're_type': 'oHab.nomTipInmBuscador = "(.*)";',
    'province': 'oHab.geo.nomProvBuscador = "(.*)";',
    'province_code': 'oHab.geo.codProv = (.*);',
    'county': 'oHab.geo.nomComBuscador = "(.*)";',
    'area_code': 'oHab.geo.codArea = (.*);',
    'town': 'oHab.geo.nomPobBuscador = "(.*)";',
    'town_code': 'oHab.geo.codPob = (.*);',
    # 'district': 'oHab.geo.nomDistritoBuscador',
    # 'neighbourhood': 'oHab.geo.nomZonaBuscador'
}

FIELDS_VARS_MAP = {
    'oHab.geo.nomProv = "(.*)";': 'location_provincia',
    'oHab.geo.nomCom = "(.*)";': 'location_comarca',
    'oHab.geo.nomArea = "(.*)";': 'location_area',
    'oHab.geo.nomPob = "(.*)";': 'location_municipio',
    'oHab.geo.nomDistrito = "(.*)";': 'location_distrito',
    'oHab.geo.nomZona = "(.*)";': 'location_barrio',
    # 'oHab.nomTipOp = "(.*)";': 'transaction_type',
    'oHab.nomTipInm = "(.*)";': 're_type',
}


class HabitacliaCountersSpider(scrapy.Spider):
    name = 'habitaclia-counters'
    custom_settings = {
        'INSERT_DATE_FIELD': 'photo_date',
        'FETCH_EMPTY_COUNTERS': False,
        'SPIDERMON_VALIDATION_SCHEMAS': [
            resource_filename('asistech', 'spidermon/schemas/habitaclia-counters.json')
        ],
    }

    def __init__(self, *args, **kwargs):
        super(HabitacliaCountersSpider, self).__init__(*args, **kwargs)

        start_urls = kwargs.get('urls', self.start_urls)
        if not isinstance(start_urls, list):
            start_urls = start_urls.split(',')
        if not start_urls:
            provinces = get_spanish_provinces()
            start_urls = [LISTING_URL_TEMPLATE.format('comprar', 'vivienda', prov)
                          for prov in provinces]
        self.start_urls = start_urls

    def parse(self, response):
        for pobl in response.xpath('//div[@id="enlacespoblacion"]/ul//@href').extract():
            yield scrapy.Request(pobl, callback=self.parse_poblacion)

    def parse_poblacion(self, response):
        if self.settings.getbool('FETCH_EMPTY_COUNTERS'):
            for value in response.css('select#cod_pob option').xpath('@value').extract():
                town, _, _ = value.split('-')
                if town.lower() == 'x':
                    continue
                # we rely on dupefiltering to stop fetching urls in the same region
                url = 'http://www.habitaclia.com/viviendas-{}.htm'.format(town)
                yield scrapy.Request(url, callback=self.parse_poblacion)

        url_args = {}
        for arg, var in URLS_VARS_MAP.items():
            url_args[arg] = response.xpath('//script').re_first(var)

        # 'province_code' and 'town_code' are popped on purpose, since those
        # aren't valid arguments for get_habitaclia_url
        formdata = {
            'codProv': url_args.pop('province_code'),
            'codPob': url_args.pop('town_code'),
        }

        yield scrapy.FormRequest(
            'http://www.habitaclia.com/hab_inmuebles/ajax/getZonas.asp',
            method='POST',
            formdata=formdata,
            headers={'Referer': 'http://www.habitaclia.com/'},
            callback=self.parse_zonas,
            meta={'url_args': url_args},
            dont_filter=True,
        )

    def parse_zonas(self, response):
        result = json.loads(response.body_as_unicode())
        args = response.meta.get('url_args')
        if not result['datos']:
            for re_type in REAL_STATES_TYPES:
                url = get_habitaclia_url('listainmuebles.htm',
                                         transaction_type='comprar',
                                         re_type=re_type,
                                         **args)
                yield scrapy.Request(url, callback=self.parse_contadores)

        for dato in result['datos']:
            if not dato['items']:
                for re_type in REAL_STATES_TYPES:
                    url = get_habitaclia_url('listainmuebles.htm',
                                             transaction_type='comprar',
                                             re_type=re_type,
                                             neighbourhood=dato['value'],
                                             **args)
                    yield scrapy.Request(url, callback=self.parse_contadores)

            for neighbourhood in dato['items']:
                for re_type in REAL_STATES_TYPES:
                    url = get_habitaclia_url('listainmuebles.htm',
                                             transaction_type='comprar',
                                             re_type=re_type,
                                             district=dato['value'],
                                             neighbourhood=neighbourhood['value'],
                                             **args)
                    yield scrapy.Request(url, callback=self.parse_contadores)

    def parse_contadores(self, response):
        il = HabitacliaCounterItemLoader(response=response)

        for var, field in FIELDS_VARS_MAP.items():
            il.add_xpath(field, '//script', re=var)
        base_item = il.load_item()

        for transaction_code, transaction_type in TRANSACTIONS_TYPES.items():
            _xpath = TAB_PER_TRANSACTION_x.format(transaction_code)
            il = HabitacliaCounterItemLoader(selector=response.xpath(_xpath),
                                             item=base_item.copy())
            il.add_value('transaction_type', transaction_type)
            il.add_xpath('num_ads', '.', re="(\d[\d.]*)")
            il.add_value('num_ads', '0')

            if not self.settings.getbool('FETCH_EMPTY_COUNTERS'):
                if il.get_output_value('num_ads') == '0':
                    continue

            yield il.load_item()
