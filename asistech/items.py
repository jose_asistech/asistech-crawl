# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Compose, MapCompose, Join, Identity, TakeFirst
from scrapylib.processors import default_input_processor, default_output_processor

from .processors import (unquote_urlquoted_unicode, get_number, to_number,
                         cast_to_boolean, cast_to_date, get_date_range,
                         strip_international_phone_code, remove_whitespace_from_phone,
                         filter_spanish_price, convert_spanish_price, clean_phone,
                         clean_seller_url)


#  habitaclia.com

class HabitacliaAgencyItem(scrapy.Item):
    external_id = scrapy.Field()
    url = scrapy.Field()
    seller_name = scrapy.Field()
    address = scrapy.Field()
    city = scrapy.Field()
    zip = scrapy.Field()
    region = scrapy.Field()
    phone = scrapy.Field()
    website = scrapy.Field()
    since = scrapy.Field()
    sell_counter = scrapy.Field()
    rent_counter = scrapy.Field()
    newconstruction_counter = scrapy.Field()
    seasonalrent_counter = scrapy.Field()
    transfer_counter = scrapy.Field()
    insert_date = scrapy.Field()


class HabitacliaPropertyItem(scrapy.Item):
    ad_external_id = scrapy.Field()
    ad_url = scrapy.Field()
    reference_id = scrapy.Field()
    posting_date_range = scrapy.Field()
    insert_date = scrapy.Field()
    ad_title = scrapy.Field()
    seller_url = scrapy.Field()
    seller_reference = scrapy.Field()
    seller_type = scrapy.Field()
    promotion_url = scrapy.Field()
    description = scrapy.Field()
    transaction_type = scrapy.Field()
    re_type = scrapy.Field()
    subtype = scrapy.Field()
    address = scrapy.Field()
    zip_code = scrapy.Field()
    location_barrio = scrapy.Field()
    location_distrito = scrapy.Field()
    location_municipio = scrapy.Field()
    location_area = scrapy.Field()
    location_comarca = scrapy.Field()
    location_provincia = scrapy.Field()
    latitude = scrapy.Field()
    longitude = scrapy.Field()
    price = scrapy.Field()
    currency = scrapy.Field()
    price_down = scrapy.Field()
    images = scrapy.Field()
    total_build_size = scrapy.Field()
    total_usable_size = scrapy.Field()
    n_rooms = scrapy.Field()
    n_bathrooms = scrapy.Field()
    n_washrooms = scrapy.Field()
    state = scrapy.Field()
    elevator = scrapy.Field()
    floor = scrapy.Field()
    ext_or_int = scrapy.Field()
    energetic_certification = scrapy.Field()
    swimming_pool = scrapy.Field()
    garden = scrapy.Field()
    parking = scrapy.Field()
    furnished = scrapy.Field()
    air_conditioning = scrapy.Field()
    heating = scrapy.Field()
    balcony = scrapy.Field()
    fireplace = scrapy.Field()
    near_public_transportation = scrapy.Field()
    view_over_the_sea = scrapy.Field()
    view_over_the_mountains = scrapy.Field()
    view_over_the_city = scrapy.Field()
    sports_facilities = scrapy.Field()
    construction_year = scrapy.Field()
    security_guard = scrapy.Field()
    update_date = scrapy.Field()
    visits = scrapy.Field()
    favorites = scrapy.Field()
    contact_emails = scrapy.Field()
    phone = scrapy.Field()


class HabitacliaCounterItem(scrapy.Item):
    photo_date = scrapy.Field()
    transaction_type = scrapy.Field()
    re_type = scrapy.Field()
    location_barrio = scrapy.Field()
    location_distrito = scrapy.Field()
    location_municipio = scrapy.Field()
    location_area = scrapy.Field()
    location_comarca = scrapy.Field()
    location_provincia = scrapy.Field()
    num_ads = scrapy.Field()


class DefaultItemLoader(ItemLoader):
    default_input_processor = default_input_processor
    default_output_processor = default_output_processor


class HabitacliaAgencyItemLoader(DefaultItemLoader):
    default_item_class = HabitacliaAgencyItem
    external_id_in = MapCompose(
        DefaultItemLoader.default_input_processor,
        unquote_urlquoted_unicode
    )
    phone_in = Identity()


class HabitacliaPropertyItemLoader(DefaultItemLoader):
    default_item_class = HabitacliaPropertyItem
    garden_in = MapCompose(
        DefaultItemLoader.default_input_processor,
        cast_to_boolean
    )
    balcony_in = MapCompose(
        DefaultItemLoader.default_input_processor,
        cast_to_boolean
    )
    near_public_transportation_in = MapCompose(
        DefaultItemLoader.default_input_processor,
        cast_to_boolean
    )
    posting_date_range_in = MapCompose(
        DefaultItemLoader.default_input_processor,
        cast_to_date,
        get_date_range
    )
    phone_in = MapCompose(clean_phone)
    phone_out = Join(u', ')


class HabitacliaCounterItemLoader(DefaultItemLoader):
    default_item_class = HabitacliaCounterItem


#  autoscout24.es

number_processor = MapCompose(get_number, to_number)
phone_processor = MapCompose(strip_international_phone_code)
seller_url_processor = MapCompose(clean_seller_url)


class AutoScout24SellersItem(scrapy.Item):
    external_id = scrapy.Field()
    url = scrapy.Field()
    seller_name = scrapy.Field()
    address = scrapy.Field()
    city = scrapy.Field()
    zip = scrapy.Field()
    region = scrapy.Field()
    phone = scrapy.Field()
    phone_1 = scrapy.Field()
    phone_2 = scrapy.Field()
    phone_3 = scrapy.Field()
    phone_4 = scrapy.Field()
    phone_5 = scrapy.Field()
    website = scrapy.Field()
    since = scrapy.Field()
    total_counter = scrapy.Field()
    insert_date = scrapy.Field()
    name_cperson = scrapy.Field()
    name_cperson_1 = scrapy.Field()
    name_cperson_2 = scrapy.Field()
    name_cperson_3 = scrapy.Field()
    position_cperson = scrapy.Field()
    position_cperson_1 = scrapy.Field()
    position_cperson_2 = scrapy.Field()
    position_cperson_3 = scrapy.Field()
    phone_cperson = scrapy.Field()
    phone_cperson_1 = scrapy.Field()
    phone_cperson_2 = scrapy.Field()
    phone_cperson_3 = scrapy.Field()


class AutoScout24CarsItem(scrapy.Item):
    # 1st priority
    ad_external_id = scrapy.Field()
    ad_url = scrapy.Field()
    posting_date_range = scrapy.Field()
    insert_date = scrapy.Field()
    ad_title = scrapy.Field()
    seller_url = scrapy.Field()
    mark = scrapy.Field()
    model = scrapy.Field()
    seller_reference = scrapy.Field()
    seller_type = scrapy.Field()
    vehicle_category = scrapy.Field()
    vehicle_subcategory = scrapy.Field()
    price = scrapy.Field()
    currency = scrapy.Field()
    updated_date = scrapy.Field()
    seller_name = scrapy.Field()
    address = scrapy.Field()
    city = scrapy.Field()
    zip = scrapy.Field()
    region = scrapy.Field()
    phone = scrapy.Field()
    name_cperson = scrapy.Field()
    reference_id = scrapy.Field()
    # 2nd priority
    power = scrapy.Field()
    km = scrapy.Field()
    registration_date = scrapy.Field()
    fuel = scrapy.Field()
    bodyworktype = scrapy.Field()
    doors = scrapy.Field()
    seats = scrapy.Field()
    # 3rd priority
    co2_emissions = scrapy.Field()
    warranty = scrapy.Field()
    transmision = scrapy.Field()
    ad_description = scrapy.Field()


class AutoScout24SellersItemLoader(DefaultItemLoader):
    default_item_class = AutoScout24SellersItem
    phone_out = phone_processor
    total_counter_in = number_processor
    name_cperson_out = Identity()
    address_out = Join('[+]')
    position_cperson_out = Identity()
    phone_cperson_out = phone_processor


class AutoScout24CarsItemLoader(DefaultItemLoader):
    default_item_class = AutoScout24CarsItem
    ad_description_out = Join()
    phone_out = phone_processor
    price_out = number_processor
    seller_url_in = seller_url_processor


# autocasion.com

class AutocasionSellerItem(scrapy.Item):
    external_id = scrapy.Field()
    url = scrapy.Field()
    seller_name = scrapy.Field()
    address = scrapy.Field()
    city = scrapy.Field()
    zip = scrapy.Field()
    region = scrapy.Field()
    phone = scrapy.Field()
    website = scrapy.Field()
    since = scrapy.Field()
    seller_counter = scrapy.Field()
    insert_date = scrapy.Field()


class AutocasionCarItem(scrapy.Item):
    # 1st priority
    ad_external_id = scrapy.Field()
    ad_url = scrapy.Field()
    insert_date = scrapy.Field()
    ad_title = scrapy.Field()
    seller_url = scrapy.Field()
    mark = scrapy.Field()
    seller_reference = scrapy.Field()
    seller_type = scrapy.Field()
    vehicle_type = scrapy.Field()
    vehicle_category = scrapy.Field()
    price = scrapy.Field()
    currency = scrapy.Field()
    updated_date = scrapy.Field()
    seller_name = scrapy.Field()
    address = scrapy.Field()
    city = scrapy.Field()
    zip = scrapy.Field()
    region = scrapy.Field()
    phone = scrapy.Field()
    name_cperson = scrapy.Field()
    # 2nd priority
    reference_id = scrapy.Field()
    power = scrapy.Field()
    km = scrapy.Field()
    registration_date = scrapy.Field()
    fuel = scrapy.Field()
    bodyworktype = scrapy.Field()
    doors = scrapy.Field()
    seats = scrapy.Field()
    # 3rd priority
    model = scrapy.Field()
    co2_emissions = scrapy.Field()
    warranty = scrapy.Field()
    transmision = scrapy.Field()
    ad_description = scrapy.Field()

    _vehicle_subtype = scrapy.Field()


class AutocasionSellerItemLoader(DefaultItemLoader):
    default_item_class = AutocasionSellerItem

    phone_in = MapCompose(remove_whitespace_from_phone)
    phone_out = Join(u', ')
    seller_counter_in = number_processor


class AutocasionCarItemLoader(DefaultItemLoader):
    default_item_class = AutocasionCarItem

    ad_title_out = Join(u' ')
    phone_in = MapCompose(remove_whitespace_from_phone)
    price_in = MapCompose(filter_spanish_price, convert_spanish_price)
    ad_description_out = Join(u'\n')


# enalquiler.com

class EnalquilerProfessionalItem(scrapy.Item):
    external_id = scrapy.Field()
    url = scrapy.Field()
    seller_name = scrapy.Field()
    address = scrapy.Field()
    city = scrapy.Field()
    zip = scrapy.Field()
    region = scrapy.Field()
    phone = scrapy.Field()
    type_dealer = scrapy.Field()
    contact_name = scrapy.Field()
    contact_time = scrapy.Field()
    website = scrapy.Field()
    since = scrapy.Field()
    rent_counter = scrapy.Field()
    insert_date = scrapy.Field()


class EnalquilerPropertyItem(scrapy.Item):
    # 1st priority
    ad_external_id = scrapy.Field()
    ad_url = scrapy.Field()
    posting_date_range = scrapy.Field()
    ad_title = scrapy.Field()
    seller_url = scrapy.Field()
    seller_reference = scrapy.Field()
    seller_type = scrapy.Field()
    transaction_type = scrapy.Field()
    subtype = scrapy.Field()
    zip_code = scrapy.Field()
    location_municipio = scrapy.Field()
    location_provincia = scrapy.Field()
    price = scrapy.Field()
    currency = scrapy.Field()
    images = scrapy.Field()
    total_built_size = scrapy.Field()
    insert_date = scrapy.Field()
    updated_date = scrapy.Field()
    # 2nd priority
    reference_id = scrapy.Field()
    address = scrapy.Field()
    location_barrio = scrapy.Field()
    location_distrito = scrapy.Field()
    price_down = scrapy.Field()
    deposit = scrapy.Field()
    visits = scrapy.Field()
    favorites = scrapy.Field()
    contact_emails = scrapy.Field()
    # 3rd priority
    description = scrapy.Field()
    n_rooms = scrapy.Field()
    n_bathrooms = scrapy.Field()
    elevator = scrapy.Field()
    floor = scrapy.Field()
    ext_or_int = scrapy.Field()
    energetic_certification = scrapy.Field()
    swimming_pool = scrapy.Field()
    garden = scrapy.Field()
    zone_info = scrapy.Field()

    phone = scrapy.Field()
    contact_hours = scrapy.Field()
    contact_person = scrapy.Field()
    features = scrapy.Field()

    seller_microsite = scrapy.Field()


class EnalquilerProfessionalItemLoader(DefaultItemLoader):
    default_item_class = EnalquilerProfessionalItem


class EnalquilerPropertyItemLoader(DefaultItemLoader):
    default_item_class = EnalquilerPropertyItem

    posting_date_range_in = MapCompose(
        DefaultItemLoader.default_input_processor,
        cast_to_date,
        get_date_range
    )
    deposit_in = MapCompose(filter_spanish_price, convert_spanish_price)
    # price_in = MapCompose(filter_spanish_price, convert_spanish_price)
    price_down_in = MapCompose(to_number)
    description_in = MapCompose(lambda s: s.strip() or None)
    description_out = Join(u'\n')
    zone_info_in = MapCompose(lambda s: s.strip() or None)
    zone_info_out = Join(u'\n')
    features_in = MapCompose(lambda s: s.strip() or None)
    features_out = Join(u'\n')
    address_out = Join('')


# Indeed

class IndeedJobItem(scrapy.Item):
    id_offer = scrapy.Field()
    url_offer = scrapy.Field()
    title_offer = scrapy.Field()
    synopsis_offer = scrapy.Field()
    salary_offer = scrapy.Field()
    city_offer = scrapy.Field()
    province_offer = scrapy.Field()
    original_offer = scrapy.Field()
    source_offer = scrapy.Field()
    sponsored_offer = scrapy.Field()
    firstdate_offer = scrapy.Field()
    id_company = scrapy.Field()
    name_company = scrapy.Field()
    description_offer = scrapy.Field()
    zip_code_offer = scrapy.Field()
    education_offer = scrapy.Field()
    experience_type_offer = scrapy.Field()
    experience_years_offer = scrapy.Field()
    languages_offer = scrapy.Field()
    job_type_offer = scrapy.Field()
    insert_date = scrapy.Field()


class IndeedCompanyItem(scrapy.Item):
    id_company = scrapy.Field()
    name_company = scrapy.Field()
    url_company = scrapy.Field()
    info_company = scrapy.Field()
    rating_company = scrapy.Field()
    valorations_company = scrapy.Field()
    rating_eq_company = scrapy.Field()
    rating_sal_company = scrapy.Field()
    rating_est_company = scrapy.Field()
    rating_ges_company = scrapy.Field()
    rating_cult_company = scrapy.Field()
    office_company = scrapy.Field()
    revenue_company = scrapy.Field()
    employee_company = scrapy.Field()
    sector_company = scrapy.Field()
    web_company = scrapy.Field()
    numjobs_company = scrapy.Field()
    insert_date = scrapy.Field()


class IndeedJobItemLoader(DefaultItemLoader):
    default_item_class = IndeedJobItem

    title_offer_out = Join()
    synopsis_offer_out = Join()
    description_offer_out = Join(u'\n')
    languages_offer_out = Join(u'\n')


class IndeedCompanyItemLoader(DefaultItemLoader):
    default_item_class = IndeedCompanyItem

    info_company_out = Join(u'\n')
    office_company_out = Join(u'\n')


# Google for jobs
class GoogleForJobsItem(scrapy.Item):
    id_offer = scrapy.Field()
    url_offer = scrapy.Field()
    description_offer = scrapy.Field()
    title_offer = scrapy.Field()
    name_company = scrapy.Field()
    id_company = scrapy.Field()
    location_offer = scrapy.Field()
    category_offer = scrapy.Field()
    firstdate_offer = scrapy.Field()
    insert_date = scrapy.Field()
    job_type_offer = scrapy.Field()
    salary_offer = scrapy.Field()
    original_offer = scrapy.Field()
    source_offer = scrapy.Field()
    rating_company = scrapy.Field()
    sector_company = scrapy.Field()
    web_company = scrapy.Field()
    search_company = scrapy.Field()


class GoogleForJobsItemItemLoader(DefaultItemLoader):
    default_item_class = GoogleForJobsItem
    default_output_processor = TakeFirst()

    description_offer_out = Compose(lambda d: [x for x in d if x not in [u'...', u'Read more']], Join(u'\n'))
    source_offer_out = Join(', ')
    rating_company_in = MapCompose(Join('-'))
    rating_company_out = Join(',')


# Yaencontre

class YaencontrePropertyItem(EnalquilerPropertyItem):
    promo = scrapy.Field()


class YaencontreProfessionalItem(EnalquilerProfessionalItem):
    pass


class YaencontrePropertyItemLoader(EnalquilerPropertyItemLoader):
    default_item_class = YaencontrePropertyItem


class YaEncontreProfessionalItemLoader(EnalquilerProfessionalItemLoader):
    default_item_class = YaencontreProfessionalItem

    rent_counter_in = Identity()
    contact_time_out = Join('\n')
