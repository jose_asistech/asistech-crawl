
class RetryRequest(Exception):
    """Raise this exception to retry request from callback."""

    def __init__(self, message='', request=None, **kwargs):
        """Initialize exception.

        :param message: exception message
        :param request: request to retry

        """
        super(RetryRequest, self).__init__(message)
        self.request = request
        self.kwargs = kwargs
