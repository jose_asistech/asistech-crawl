# -*- coding: utf-8 -*-
import csv
import os
import re
from itertools import product
from urllib import quote
from urlparse import urljoin

from scrapy import Spider, Request
from scrapy.http import HtmlResponse

from asistech.items import GoogleForJobsItemItemLoader
from pkg_resources import resource_filename

BASE_URL = (
    'https://www.google.es/search?q={query}&ibp=htl;jobs'
)

ITEMLIST_URL = (
    'https://www.google.es/search?vet=1{dataved}..i&chips={chips}&yv=3&rciv=jb&nfpr=0&q={query}&start={start}&asearch=tl_af&async=_id:gws-horizon-textlists__tl-af,_pms:hts,_fmt:pc'
)


class GoogleForJobsSpider(Spider):
    name = 'google'
    num_pages = 15
    search_prefix = ''
    company_ratings = {}
    allowed_domains = [
        'google.es',
    ]
    custom_settings = {
        'COOKIES_ENABLED': False,
        'DEFAULT_REQUEST_HEADERS': {
            'Accept-Language': 'en-GB,en;q=0.5'
        },
        'DOWNLOADER_MIDDLEWARES': {
            'asistech.middlewares.ProxyMiddleware': 740,
            'scrapy.downloadermiddlewares.retry.RetryMiddleware': None,
            'asistech.middlewares.FixedRetryMiddleware': 500,
            'scrapy_crawlera.CrawleraMiddleware': 750,
        },
        'ITEM_PIPELINES': {
            'asistech.pipelines.DuplicatesPipeline': 200,
            'asistech.pipelines.InsertDatePipeline': 300,
            'asistech.pipelines.DefaultValuePipeline': 900,
        },
        'ID_FIELD': 'id_offer',
        'SPIDERMON_VALIDATION_SCHEMAS': [
            resource_filename('asistech', 'spidermon/schemas/google.json')
        ],
    }

    def __init__(self, name=None, **kwargs):
        super(GoogleForJobsSpider, self).__init__(name=None, **kwargs)
        from_date = getattr(self, 'from_date', '').lower()
        valid_dates = {'all': '', 'last_day': 'date_posted:today,', 'last_three': 'date_posted:3days,',
                       'last_week': 'date_posted:week,', 'last_month': 'date_posted:month,'}
        self.chips = ''
        if from_date:
            if from_date not in valid_dates:
                raise Exception('Invalid from date supplied, please use one of %s' % (tuple(valid_dates.keys()),))
            self.chips = valid_dates[from_date]

        location = getattr(self, 'location', 'spain').lower()
        valid_locations = ('all', 'spain', 'capitals')
        if location not in valid_locations:
            raise Exception('Invalid location supplied, please use one of %s' % (valid_locations,))

        try:
            self.search_prefix = getattr(self, 'prefix', '').lower()
        except:
            raise Exception('Invalid prefix supplied')

        try:
            self.num_pages = int(getattr(self, 'num_pages', '15'))
        except:
            raise Exception('Invalid value for num_pages supplied')

        if location == 'spain':
            self.search_locations = {'spain'}
        if location == 'all':
            source_path = os.path.join(os.path.dirname(__file__), '..', '..', 'resources', 'spain.csv')
            with open(source_path, 'rb') as f:
                reader = csv.DictReader(f)
                self.search_locations = set([row['Poblaci\xc3\xb3n'] for row in reader])
        elif location == 'capitals':
            source_path = os.path.join(os.path.dirname(__file__), '..', '..', 'resources', 'spain.csv')
            with open(source_path, 'rb') as f:
                reader = csv.DictReader(f)
                self.search_locations = set([row['Poblaci\xc3\xb3n'] for row in reader if
                                             row['Capital'].lower() == 'yes'])

    def start_requests(self):

        self.logger.info('Total locations to search %s' % len(self.search_locations))
        for location in self.search_locations:
            yield Request(
                BASE_URL.format(query=self.search_prefix + location),
                meta={
                    'query': self.search_prefix + location,
                },
                dont_filter=True)

    def parse(self, response):
        # dataved is the main jobsearch ID
        dataved = response.xpath('//div[@id="gws-horizon-textlists__tl-af"]/@data-ved').extract_first()
        if not dataved:
            self.logger.warning('"dataved" was not found in the response body. No ads found. Skipping...')
            return

        # Pagination with Filters
        filters = re.findall(r'<div jsname="Emidac".*?</span></span></div>', response.body, re.S)
        filters_html = [HtmlResponse(url=response.url, body=i) for i in filters]

        industries = []
        categories = []
        for filter_html in filters_html:
            data_facet = filter_html.xpath('.//div[@jsname="Emidac"]/@data-facet').extract_first()
            data_values = filter_html.xpath('.//span[@data-value!="__placeholder__"]/@data-value').extract()
            data_names = filter_html.xpath('.//span[@data-value!="__placeholder__"]/span/text()').extract()

            if data_facet == 'industry.id':
                industries.extend(zip(data_values, data_names))
            if data_facet == 'gcat_category.id':
                categories.extend(zip(data_values, data_names))

        for industry in industries:
            industry_value = industry[0]
            industry_name = industry[1]
            chips = self.chips + '{}:{}'.format('industry.id', industry_value)
            yield Request(
                url=ITEMLIST_URL.format(dataved=dataved, chips=chips, start=0, query=response.meta['query']),
                          callback=self.parse_itemlist,
                          meta=dict(dataved=dataved,
                                    chips=chips,
                                    query=response.meta['query'],
                                    sector_company=industry_name,
                                    page=0))
        for category in categories:
            category_value = category[0]
            category_name = category[1]
            chips = self.chips + '{}:{}'.format('gcat_category.id', category_value)
            yield Request(
                url=ITEMLIST_URL.format(dataved=dataved, chips=chips, start=0, query=response.meta['query']),
                          callback=self.parse_itemlist,
                          meta=dict(dataved=dataved,
                                    chips=chips,
                                    query=response.meta['query'],
                                    category_offer=category_name,
                                    page=0))

        yield Request(
            url=ITEMLIST_URL.format(dataved=dataved, chips=self.chips, start=0, query=response.meta['query']),
            callback=self.parse_itemlist,
            priority=-100,  # Make sure these are called in the end.
            meta=dict(
                dataved=dataved,
                chips=self.chips,
                query=response.meta['query'],
                page=0
            )
        )

    def parse_itemlist(self, response):
        ids = re.findall(r'\"\[\\\"(.*?)\\\\u003d\\\\u003d\\\",\\\"jobs\\\",null,0,0,', response.body)
        alldata = re.findall(r'\[\".*?".\"(\[\\.*?)\\n\"\]\s?,?(\[\".*?".\"\[\\.*?\\\\u003d\\\\u003d\\\",\\\"jobs\\\",0.*?\\n\"\])?', response.body)
        company_ids = []
        company_names = []

        for i, data in enumerate(alldata):
            if data[1]:
                company_ids.append(re.findall(r'\\"(.*?)\\"', data[1])[1])
                company_names.append(re.findall(r'\\"(.*?)\\"', data[1])[0])
            else:
                if 'null,0,0' not in data[0]:
                    company_ids.append(re.findall(r'\\"(.*?)\\"', data[0])[1])
                    company_names.append(re.findall(r'\\"(.*?)\\"', data[0])[0])

        try:
            html_body = re.search(r'\[2\].*?(<style>.*</span>).*?\[5\]', response.body, re.S).group(1)
        except AttributeError:
            return

        items_html = HtmlResponse(url=response.url, body=html_body).xpath('//ul/li')
        for i, item in enumerate(items_html):
            # Instance a new itemloader
            itemloader = GoogleForJobsItemItemLoader(selector=item)

            # Id, URL, CompanyId
            itemloader.add_value('id_offer', ids[i])
            url_job_params = '#fpstate=tldetail&htidocid={id_offer}%3D%3D&htivrt=jobs'.format(id_offer=ids[i])
            itemloader.add_value('url_offer',
                                 '{}{}'.format(BASE_URL.format(query=response.meta['query']), url_job_params))
            if len(company_ids)>i:
                itemloader.add_value('id_company', company_ids[i])

            # Title, Company, Location
            itemloader.add_css('title_offer', 'div.BjJfJf::text')
            itemloader.add_xpath('name_company', './/div[@class="wozmme"]/div[1]/text()')
            itemloader.add_xpath('location_offer', './/div[@class="tcoBdd"]/text()')

            # Filters: Category, Company Type
            itemloader.add_value('sector_company', response.meta.get('sector_company', ''))
            itemloader.add_value('category_offer', response.meta.get('category_offer', ''))

            # Date, Salary, Job type
            jobdetails_xpath = './/div[@class="edeiNb"]/span/span[{}]/following-sibling::span/text()'
            itemloader.add_xpath('firstdate_offer', jobdetails_xpath.format('@aria-label="Posted"'))
            itemloader.add_xpath('salary_offer', jobdetails_xpath.format('@aria-label="Salary"'))
            itemloader.add_xpath('job_type_offer', jobdetails_xpath.format('@aria-label=""'))

            # Description
            for description in item.xpath('.//div[@class="lhewOe"]//span//text()'):
                itemloader.add_value('description_offer', description.extract())

            # Apply (first and others)
            itemloader.add_xpath('source_offer', './/g-scrolling-carousel/div[1]//text()', re='Apply on (.*)')
            applications_dataved_xpath = './/div[@data-async-type="applyOptionsItemsDetailAsync"]/@data-ved'
            applications_dataved = item.xpath(applications_dataved_xpath).extract_first()

            # Ratings
            ratings_dataved_xpath = './/div[@data-async-type="reviewItemsDetailAsync"]/@data-ved'
            ratings_dataved = item.xpath(ratings_dataved_xpath).extract_first()

            # Company URL, Search URL
            itemloader.add_xpath('web_company', './/div[@class="L4FTLe"]/a[@class="F3hKEb" and not(starts-with(@href,"/search"))]/@href')
            itemloader.add_value('search_company',
                                 urljoin(response.url, item.xpath('.//div[@class="X3Rrid Fogx6e nluEHe" and @style="z-index:0"]/div/a/@href').extract_first()))

            yield Request(
                url='https://www.google.es/search?vet=1{}..i&yv=3&q={}&asearch=applyOptionsItemsDetailAsync&async=ibp:jobs,htidocid:{}%3D%3D,aoc:%234285f4,_fmt:prog'.format(
                    applications_dataved, response.meta['query'], ids[i]),
                callback=self.parse_additional_applications,
                meta=dict(itemloader=itemloader,
                          ratings_dataved=ratings_dataved,
                          dataved=response.meta.get('dataved'),
                          chips=response.meta.get('chips'),
                          sector_company=response.meta.get('sector_company'),
                          category_offer=response.meta.get('category_offer'),
                          page=response.meta.get('page'),
                          query=response.meta['query'],
                          id=ids[i]
                          )
            )

        if len(items_html) == 10 and response.meta.get('page') < int(self.num_pages) - 1:
            yield Request(url=ITEMLIST_URL.format(dataved=response.meta.get('dataved'),
                                                  chips=response.meta.get('chips'),
                                                  query=response.meta['query'],
                                                  start=(response.meta.get('page') + 1) * 10),
                          callback=self.parse_itemlist,
                          meta=dict(dataved=response.meta.get('dataved'),
                                    chips=response.meta.get('chips'),
                                    query=response.meta['query'],
                                    sector_company=response.meta.get('sector_company'),
                                    category_offer=response.meta.get('category_offer'),
                                    page=response.meta.get('page') + 1))

    def parse_additional_applications(self, response):
        itemloader = response.meta.get('itemloader', None)
        try:
            html_body = re.search(r'<div>.*?</div>', response.body).group(0)
            applications = HtmlResponse(url=response.url, body=html_body)
            applications_names = applications.xpath('.//div//a/text()').re('Apply on (.*)')
            itemloader.add_value('source_offer', applications_names)
        except AttributeError:
            pass

        if response.meta.get('ratings_dataved') and itemloader.get_output_value('id_company'):
            if itemloader.get_output_value('id_company') in self.company_ratings:
                itemloader.add_value('rating_company', self.company_ratings[itemloader.get_output_value('id_company')])
                yield itemloader.load_item()
            else:
                yield Request(
                    url='https://www.google.es/search?vet=1{}..i&yv=3&q={}&asearch=reviewItemsDetailAsync&async=ibp:jobs,htidocid:{}%3D%3D,company_name:{},company_mid:{},_fmt:prog'.format(
                        response.meta.get('ratings_dataved'),
                        response.meta['query'],
                        itemloader.get_output_value('id_offer'),
                        quote(itemloader.get_output_value('name_company').encode('utf-8')),
                        itemloader.get_output_value('id_company')),
                    callback=self.parse_ratings,
                    meta=dict(itemloader=itemloader,
                              dataved=response.meta.get('dataved'),
                              chips=response.meta.get('chips'),
                              query=response.meta['query'],
                              sector_company=response.meta.get('sector_company'),
                              category_offer=response.meta.get('category_offer'),
                              page=response.meta.get('page')
                              )
                )
        else:
            yield itemloader.load_item()

    def parse_ratings(self, response):
        itemloader = response.meta.get('itemloader', None)

        try:
            html_body = re.search(r'<div>.*</div>', response.body).group(0)
            ratings = HtmlResponse(url=response.url, body=html_body)
            ratings_names = ratings.xpath('.//a/div/div[1]/text()').extract()
            ratings_stars = ratings.xpath('.//a/div/div[2]/text()').re('\xa0(\d\.\d)\xa0')
            ratings_reviewcount = [r.replace(',', '') for r in
                                   ratings.xpath('.//a/div/div[2]/text()').re('\xa0(\d*,*\d*,*\d*) review')]
            self.company_ratings[itemloader.get_output_value('id_company')] = zip(ratings_names, ratings_stars,
                                                                                  ratings_reviewcount)
            itemloader.add_value('rating_company', self.company_ratings[itemloader.get_output_value('id_company')])
        except AttributeError:
            pass

        yield itemloader.load_item()
