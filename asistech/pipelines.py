# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import datetime

import w3lib
from scrapy.exceptions import DropItem

from asistech.items import AutoScout24SellersItem, AutocasionCarItem, IndeedJobItem


class InsertDatePipeline(object):
    def process_item(self, item, spider):
        now = datetime.datetime.now()
        field = spider.settings.get('INSERT_DATE_FIELD')
        item[field] = now.strftime(spider.settings.get('INSERT_DATE_FORMAT'))
        return item


class DefaultValuePipeline(object):
    def process_item(self, item, spider):
        if isinstance(item, AutocasionCarItem):
            for field_name in item.fields:
                if not field_name.startswith('_') and item.get(field_name) is None:
                    item[field_name] = spider.settings.get('DEFAULT_FIELD_VALUE')
        else:
            for field_name in item.fields:
                if item.get(field_name) is None:
                    item[field_name] = spider.settings.get('DEFAULT_FIELD_VALUE')
        return item


class DiscardItemsWithoutFields(object):
    def process_item(self, item, spider):
        for field in spider.settings.getlist('REQUIRED_FIELDS'):
            if not item.get(field):
                raise DropItem('Missing {} field'.format(field))
        return item


class FlattenListPipeline(object):
    def process_item(self, item, spider):
        if isinstance(item, AutoScout24SellersItem):
            for field_name in item.fields:
                if isinstance(item.get(field_name), list):
                    for item_idx, item_value in enumerate(item.get(field_name)):
                        if item_idx < spider.settings.get('MAX_FLATTENED_FIELD_COUNT')[field_name]:
                            item['{}_{}'.format(field_name, (item_idx+1))] = item_value
                # Remove the List fields
                if field_name in spider.settings.get('FLATTENED_LIST_FIELDS'):
                    del item[field_name]

        return item


class DiscardUndesiredVehicleItems(object):

    UNDESIRED_VEHICLE_TYPES = [
        'otros', 'barcos', 'alquiler', 'accesorios y repuestos',
    ]

    def process_item(self, item, spider):
        if isinstance(item, AutocasionCarItem):
            if not item.get('ad_external_id'):
                raise DropItem('Missing ad_external_id: {}'.format(item.get('ad_url')))

            category = item.get('_vehicle_subtype')
            if category and category.lower() in self.UNDESIRED_VEHICLE_TYPES:
                raise DropItem('Undesired vehicle type: {}'.format(category))
            try:
                del item['_vehicle_subtype']
            except KeyError:
                pass

        return item


class DuplicatesPipeline(object):
    def __init__(self):
        self.ids_seen = set()

    def process_item(self, item, spider):
        field = spider.settings.get('ID_FIELD')
        if not field:
            return item

        if item[field] in self.ids_seen:
            raise DropItem(u"Duplicate item found: %s" % item[field])
        else:
            self.ids_seen.add(item[field])
            return item


class IndeedDuplicatesPipeline(object):
    def __init__(self):
        self.job_keys = set()
        self.ad_keys = set()

    def process_item(self, item, spider):
        if not isinstance(item, IndeedJobItem):
            return item

        url_offer = item.get('url_offer')
        if not url_offer:
            raise DropItem(u"Discarding item without url_offer")
        else:
            job_key = w3lib.url.url_query_parameter(url_offer, 'jk')

            if job_key:
                item['id_offer'] = job_key

                if job_key in self.job_keys:
                    raise DropItem(u"Duplicate item found: jk=%s" % job_key)
                else:
                    self.job_keys.add(job_key)
                    return item
            else:
                ad_key = w3lib.url.url_query_parameter(url_offer, 'ad')

                if ad_key:
                    ad_key = ad_key[:149]

                    if ad_key in self.ad_keys:
                        raise DropItem(u"Duplicate item found: ad=%s" % ad_key)
                    else:
                        self.ad_keys.add(ad_key)
                        return item
                else:
                    return item
