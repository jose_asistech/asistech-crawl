import scrapy
from w3lib.url import url_query_parameter, add_or_replace_parameter

from asistech.items import HabitacliaAgencyItemLoader
from asistech.utils import get_spanish_provinces
from pkg_resources import resource_filename


TRANSACTIONS_MAP = {
    'Compra': 'sell_counter',
    'Alquiler': 'rent_counter',
    'Obra nueva': 'newconstruction_counter',
    'Alquiler de temporada': 'seasonalrent_counter',
    'Traspaso': 'transfer_counter',
}


LISTING_URL_TEMPLATE = 'http://www.habitaclia.com/inmobiliarias-en-{}/inmobiliarias.htm'


class HabitacliaAgenciesSpider(scrapy.Spider):
    name = 'habitaclia-agencies'
    custom_settings = {
        'SPIDERMON_VALIDATION_SCHEMAS': [
            resource_filename('asistech', 'spidermon/schemas/habitaclia-agencies.json')
        ],
    }

    def __init__(self, *args, **kwargs):
        super(HabitacliaAgenciesSpider, self).__init__(*args, **kwargs)

        start_urls = kwargs.get('urls', self.start_urls)
        if not isinstance(start_urls, list):
            start_urls = start_urls.split(',')
        if not start_urls:
            start_urls = [LISTING_URL_TEMPLATE.format(p) for p in get_spanish_provinces()]
        self.start_urls = start_urls

    def parse(self, response):
        province = response.xpath('//h1/text()').re_first('Inmobiliarias provincia (.*)')
        for inmb in response.xpath('//div[contains(@class,"listinmo")]//div[@class="datos"]'):
            zipcode = inmb.xpath('./span[contains(@class,"dir_ex")]').re_first(u'- (\d{3,})')
            link = inmb.xpath('./h3/a/@href').extract_first()
            yield scrapy.Request(link, callback=self.parse_inmobiliaria,
                                 meta={'zipcode': zipcode, 'province': province})

        next_link = response.xpath('//div[@class="paginacionlista"]//a[contains(.,"Siguiente")]')
        if next_link:
            page = url_query_parameter(response.url, 'pag', '0')
            next_link = add_or_replace_parameter(response.url, 'pag', str(int(page) + 1))
            yield scrapy.Request(next_link, callback=self.parse)

    def parse_inmobiliaria(self, response):

        for office in response.css('select#oficinas ::attr(value)').extract():
            url, _, _ = office.partition('listacliente.htm')
            # we're relying on dupefilter to avoid infinite requests here
            yield scrapy.Request(
                url, callback=self.parse_inmobiliaria, meta=response.meta
            )

        il = HabitacliaAgencyItemLoader(response=response)
        il.add_value('url', response.url)
        il.add_value('external_id', response.url, re='inmobiliaria-(.*?)(?:/|$)')
        il.add_css('seller_name', 'h1')
        il.add_xpath('address', '//div[@id="dirOfi"]/text()[1]')
        il.add_xpath('city', '//div[@id="dirOfi"]/text()[2]', re='(.*) \(')
        il.add_css('city', 'h2', re='Empresa inmobiliaria en (.*)')
        il.add_value('zip', response.meta.get('zipcode'))
        il.add_value('region', response.meta.get('province'))
        il.add_xpath('region', '//div[@id="dirOfi"]/text()[2]', re='\(([^(]*)\)$')
        il.add_xpath('website', '//div[@class="paginaWebMinisite"]/a/@href')

        _tab_xpath = '//ul[contains(@class,"pestanas")]/li[contains(a, "{}")]//span'
        for name, field in TRANSACTIONS_MAP.items():
            il.add_xpath(field, _tab_xpath.format(name))
            il.add_value(field, '0')

        output = il.load_item()
        website = output.get('website')
        if website:
            if website == '#':
                # Link generated dynamically using javascript
                office_name = response.xpath('//a[@data-nomofi]/@data-nomofi').extract_first()
                if office_name:
                    website = 'http://www.habitaclia.com/hab_inmuebles/clickurl.asp?nom_ofi_w=' + office_name

            if website != '#':
                output = scrapy.Request(website,
                                        callback=self.parse_website,
                                        errback=self._errback_website,
                                        encoding=response.encoding,
                                        meta={'item': output})
            else:
                output['website'] = ''
        yield output

    def _errback_website(self, failure):
        response = failure.value.response
        return response.meta['item']

    def parse_website(self, response):
        item = response.meta.get('item')
        il = HabitacliaAgencyItemLoader(item=item, response=response)
        il.replace_xpath('website', '//script', re='window.location.href = "(.*)"')
        return il.load_item()
