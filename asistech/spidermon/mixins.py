# -*- coding: utf-8 -*-
from scrapinghub import ScrapinghubClient
from spidermon.contrib.monitors.mixins import SpiderMonitorMixin

from asistech.settings import PROJECT_ID
from asistech.settings import SHUB_APIKEY


class Comparison(object):

    def __init__(self, spider):
        conn = ScrapinghubClient(SHUB_APIKEY)
        self.project = conn.get_project(PROJECT_ID)
        self.spider_name = spider.name
        self.spider = spider

    def get_last_item_count(self):
        jobs = self.project.jobs.iter(spider=self.spider_name)
        for j in jobs:
            if j.get('close_reason', '') == u'finished':
                return j.get('items')

    def get_latest_10_jobs_max_count(self):
        last_10_jobs = self.project.jobs.iter(state='finished', spider=self.spider_name, count=10)
        latest_jobs_stats = [(job.get('items'), job.get('key')) for job in last_10_jobs if
                             'finished' in job.get('close_reason')]
        return max(latest_jobs_stats) if latest_jobs_stats else (0, None)


class ComparisonMonitorMixin(SpiderMonitorMixin):
    @property
    def comparison(self):
        if not hasattr(self, "_comparison"):
            self._comparison = Comparison(self.spider)
        return self._comparison
