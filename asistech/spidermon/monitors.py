from spidermon import Monitor, MonitorSuite, monitors
from spidermon.contrib.actions.sentry import SendSentryMessage
from spidermon.contrib.monitors.mixins import (
    StatsMonitorMixin, JobMonitorMixin, ValidationMonitorMixin, SpiderMonitorMixin
)

from asistech.settings import (
    DEFAULT_RESPONSE_ERROR_THRESHOLD,
    NORMAL_ITEM_COVERAGE_PERCENTAGE,
)
from asistech.spidermon.mixins import ComparisonMonitorMixin


@monitors.name('Job stats monitor')
class JobStatsMonitor(Monitor, SpiderMonitorMixin, StatsMonitorMixin, JobMonitorMixin):

    @monitors.name('Items scraped')
    @monitors.description('Checks that the item count not equal to 0')
    def test_item_count(self):
        item_count = self.stats.get('item_scraped_count', 0)
        self.assertNotEqual(item_count, 0, msg='Item count should not be 0')

    @monitors.name('Log errors')
    def test_log_errors(self):
        error_count = (self.stats.get('log_count/ERROR', 0) +
                       self.stats.get('log_count/CRITICAL', 0))
        msg = 'The job contains {error_count} errors'.format(error_count=error_count)
        self.assertEqual(error_count, 0, msg=msg)

    @monitors.name('Job outcome')
    def test_job_outcome(self):
        finish_reason = self.stats.get('finish_reason', '')
        msg = 'Job has an unexpected outcome value - {finish_reason}'
        self.assertIn(finish_reason, ["finished", "shutdown"], msg=msg)

    @monitors.name('Too many requests given up after repeated failures')
    def test_requests_given_up(self):
        total_request_count = self.stats.get('downloader/request_count', 0)
        retry_count = self.stats.get('retry/count', 0)
        requests_given_up = max(
            self.stats.get('retry/max_reached', 0),
            self.stats.get('httperror/response_ignored_count', 0)
        )
        original_requests = max(total_request_count - retry_count, 1)
        percent_requests_given_up = requests_given_up * 100.0 / original_requests

        msg = 'Too many requests given up after failed retries: {percent_requests_given_up}%'.format(
            percent_requests_given_up=percent_requests_given_up
        )
        threshold = getattr(self.spider, 'allow_response_error_threshold', DEFAULT_RESPONSE_ERROR_THRESHOLD)
        self.assertLess(percent_requests_given_up, threshold, msg=msg)


@monitors.name('Job comparison')
@monitors.description('Compare output of job with other jobs.')
class JobComparisonMonitor(Monitor, ComparisonMonitorMixin, StatsMonitorMixin):

    @monitors.name('Item coverage check with previous jobs')
    @monitors.description('Checks the item coverage with previous jobs.')
    def test_item_coverage(self):
        expected_coverage = getattr(self.spider, 'item_coverage_percentage', NORMAL_ITEM_COVERAGE_PERCENTAGE)
        last_job, job_id = self.comparison.get_latest_10_jobs_max_count()
        current_job_count = self.stats.get('item_scraped_count', 0)
        crawled_percentage = self.calculate_percentage(last_job, current_job_count)

        msg = 'Items coverage {}% as compared with job {}. Expected coverage {}%'.format(round(crawled_percentage, 2),
                                                                                         job_id, expected_coverage)
        self.assertGreaterEqual(crawled_percentage, expected_coverage, msg=msg)

    def calculate_percentage(self, previous_count, current_count):
        if not previous_count:
            return 100
        return float(current_count * 100) / previous_count


class SpiderOpenMonitorSuite(MonitorSuite):
    monitors_finished_actions = [
    ]


class SpiderCloseMonitorSuite(MonitorSuite):
    monitors = [
        JobStatsMonitor,
        JobComparisonMonitor
    ]
    monitors_finished_actions = [
    ]
    monitors_failed_actions = [
        SendSentryMessage
    ]