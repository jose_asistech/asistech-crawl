# -*- coding: utf-8 -*-
import re
import unicodedata

import requests
from scrapy.selector import Selector


def get_spanish_provinces():
    url = 'http://www.habitaclia.com/comprar-vivienda-en-x/selinmueble.htm'
    sel = Selector(text=requests.get(url).text)
    return sel.xpath('//div[@id="enlacesmapa"]//@href').re('comprar-vivienda-en-(\w*)')


def get_habitaclia_url(resource, transaction_type, re_type, subtype=None,
                       province=None, county=None, area_code=None, town=None,
                       district=None, neighbourhood=None):
    """
    Simplified GenerarPathFiltrosJS() translation, found here:
    http://static3.habimg.com/js/functions-20150730.js
    """
    if int(area_code) < 0:
        area_code = None

    ArrayNuevaURL = [''] * 6
    ArrayNuevaURL[0] = 'http://www.habitaclia.com'
    ArrayNuevaURL[1] = transaction_type

    if not subtype or subtype == 'm':
        ArrayNuevaURL[1] += '-' + re_type
        ArrayNuevaURL[4] = ''
    else:
        ArrayNuevaURL[1] += '-' + subtype
        ArrayNuevaURL[4] = re_type

    if town:
        ArrayNuevaURL[1] += '-' + 'en' + '-' + town
    elif county:
        ArrayNuevaURL[1] += '-' + 'en' + '-' + county
    elif province:
        ArrayNuevaURL[1] += '-' + 'en' + '-' + province
    else:
        ArrayNuevaURL[1] += '-' + 'en' + '-' + 'X'

    if county or town:
        ArrayNuevaURL[2] = 'provincia' + '_' + province
    if town:
        ArrayNuevaURL[2] += '-' + (county or 'X') + '-' + 'area' + '_' + (area_code or '0')

    if district and neighbourhood:
        ArrayNuevaURL[1] += '-' + 'barrio' + '_' + neighbourhood
        ArrayNuevaURL[2] += '-' + district
    elif district and not neighbourhood:
        ArrayNuevaURL[1] += '-' + 'distrito' + '_' + district
    elif neighbourhood and not district:
        ArrayNuevaURL[1] += '-' + 'zona' + '_' + neighbourhood

    ArrayNuevaURL[5] = resource.replace('.asp', '.htm')
    return '/'.join(i for i in ArrayNuevaURL if i)


def strip_accents(unicode_string):
    # This replacement is from habitaclia, otherwise 'ç' is translated as 's'
    unicode_string = unicode_string.replace(u'ç', u's')
    return ''.join(c for c in unicodedata.normalize('NFD', unicode_string)
                   if unicodedata.category(c) != 'Mn')


def get_town_code_from_name(town_name):
    """Try to guess a town code from its name in habitaclia"""
    town_code = re.sub('^[A-Z][a-z ]+', '', town_name)
    town_code = town_code.lower()
    town_code = re.sub(' \(.*\)$', '', town_code)
    town_code = strip_accents(town_code)
    town_code = re.sub('[-/]', '_', town_code)
    town_code = re.sub('\W+', ' ', town_code)
    town_code = re.sub(' ', '_', town_code)
    return town_code


def str_to_bool(value):
    value = value.lower()
    if value in ('true', 'yes', '1'):
        return True
    elif value in ('false', 'no', '0'):
        return False
    else:
        raise ValueError
