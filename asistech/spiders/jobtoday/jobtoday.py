# -*- coding: utf-8 -*-
import json
import os
import csv
import datetime
import StringIO

from scrapy import Spider, Request

from asistech.utils import str_to_bool
from pkg_resources import resource_filename


BASE_URL = (
    'https://api.jobtodayapp.com/v1/jobs?pageSize=%s&pageIndex=%s&lat=%.2f&lng=%.2f&'
    'fromRadius=%s&toRadius=%s&keyword='
)
MAX_PAGES = 1500


def unicode_csv_reader(unicode_csv_data, dialect=csv.excel, **kwargs):
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.reader(utf_8_encoder(unicode_csv_data),
                            dialect=dialect, **kwargs)
    for row in csv_reader:
        # decode UTF-8 back to Unicode, cell by cell:
        yield [unicode(cell, 'utf-8') for cell in row]


def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        yield line.encode('utf-8')


def get_date_from_timestamp(value):
    if not value:
        return ''

    timestamp = value / 1000.0
    dt = datetime.datetime.utcfromtimestamp(timestamp)
    return dt.strftime("%Y/%m/%d")


class JobtodaySpider(Spider):
    name = 'jobtoday'
    # start_urls = [
    #     'https://api.jobtodayapp.com/v1/jobs?pageSize=32&pageIndex=0&lat=41.39&lng=2.18&fromRadius=0&toRadius=50000&keyword=',
    # ]
    allowed_domains = [
        'jobtodayapp.com',
    ]
    custom_settings = {
        'COOKIES_ENABLED': False,
        'REFERER_ENABLED': False,
        'USER_AGENT': 'okhttp/3.2.0',
        'DEFAULT_REQUEST_HEADERS': {
            'Accept': 'application/json',
            'Accept-Encoding': 'gzip',
        },
        'CONCURRENT_REQUESTS': 1,
        'DOWNLOAD_DELAY': 5,
        # 'CLOSESPIDER_PAGECOUNT': 1000,
        'ITEM_PIPELINES': {
            'asistech.pipelines.DuplicatesPipeline': 200,
            'asistech.pipelines.InsertDatePipeline': 300,
        },
        'ID_FIELD': 'ad_id',
        'INSERT_DATE_FIELD': 'crawl_date',
        'SPIDERMON_VALIDATION_SCHEMAS': [
            resource_filename('asistech', 'spidermon/schemas/jobtoday.json')
        ],
    }

    def __init__(self, *args, **kwargs):
        self.page_size = int(kwargs.pop('pageSize', '32'))
        self.from_radius = int(kwargs.pop('fromRadius', '0'))
        self.to_radius = int(kwargs.pop('toRadius', '50000'))
        self.filter_capitals = str_to_bool(kwargs.pop('capitals', 'False'))

        super(JobtodaySpider, self).__init__(*args, **kwargs)

    def start_requests(self):
        source_path = os.path.join(os.path.dirname(__file__), '..', '..', 'resources', 'spain.csv')
        with open(source_path, 'rb') as f:
            data = f.read()

        buff = StringIO.StringIO(data)
        # reader = unicode_csv_reader(buff)
        reader = csv.reader(buff)
        first_row = True
        locations = []
        for row in reader:
            if first_row:
                first_row = False
                continue

            loc = {
                'city': row[2],
                'lat': row[3],
                'lng': row[4],
                'population': row[6],
                'capital': (row[9].strip().lower() == 'yes'),
            }
            locations.append(loc)

        # Filter a few locations for testing purposes
        # locations = [l for l in locations if int(l['population']) > 1000000]

        if self.filter_capitals:
            locations = [l for l in locations if l['capital']]

        for location in locations:
            url = (
                BASE_URL % (
                    self.page_size,
                    0,
                    float(location['lat']),
                    float(location['lng']),
                    self.from_radius,
                    self.to_radius,
                )
            )
            meta = {
                'page': 0,
                'location': location,
            }
            yield Request(url, meta=meta)

    def parse(self, response):
        data = json.loads(response.body_as_unicode())

        # "meta":{"code":200,"errors":[],"totalRecords":2224,"pageSize":32,"pageIndex":0}
        if data['meta']['code'] == 200:
            for record in data['data']:
                try:
                    item = {}
                    item['ad_id'] = record['id']
                    item['image_url'] = record.get('mainImageUrl', '')

                    item['latitude'] = record.get('location', {}).get('lat', '')
                    item['longitude'] = record.get('location', {}).get('lng', '')

                    item['address'] = record.get('address', '')

                    item['companyName'] = record.get('companyName')

                    owner = record.get('owner', {})

                    item['owner_id'] = owner.get('id', '')
                    item['owner_firstName'] = owner.get('firstname', '')
                    item['owner_lastName'] = owner.get('lastname', '')
                    item['owner_avatar'] = owner.get('avatarUrl', '')
                    item['owner_lastOnline'] = get_date_from_timestamp(owner.get('lastOnline', ''))
                    item['owner_hasPhone'] = owner.get('hasPhone', '')
                    item['owner_phoneVerified'] = owner.get('isPhoneVerified', False)
                    # isBanned has been removed (March 2017)
                    # item['owner_isBanned'] = owner['isBanned']
                    item['createDate'] = get_date_from_timestamp(record.get('createDate'))
                    item['updateDate'] = get_date_from_timestamp(record.get('updateDate'))
                    item['isActive'] = record.get('isActive')
                    item['description'] = record.get('description')
                    yield item
                except KeyError:
                    self.logger.warning('Missing key: %s', record)

            # July 2016: totalRecords contains wrong information, so we have
            # changed the original check

            next_url = data.get('meta', {}).get('requestSpecificData', {}).get('nextUrl', '')
            if next_url:
                yield Request(next_url)

        else:
            self.logger.warning('Response code is not 200')
