# -*- coding: utf-8 -*-
import json
import datetime
import os
import csv
import StringIO

from scrapy import Spider, Request
from pkg_resources import resource_filename


# PARAMETERS:
# Sort by date: sortBy=DD
# Sort by relevance: sort=R
# Experience (levels from 0 to 6): f_E=0
# Date posted (24 hours): f_TP=1
# Date posted (past week): f_TP=1,2


BASE_URL = (
    # 'https://www.linkedin.com/jobs2/api/jobPostings/search/?decorateFacets=false&distance=%s&count=%s&f_L=%s&sortBy=DD&start=%s'
    'https://www.linkedin.com/jobs2/api/jobPostings/search/?decorateFacets=false&distance=%s&count=%s&f_L=%s&sortBy=DD&start=%s&f_TP=1&f_E=%s'
)

POSTAL_CODE_BASE_URL = (
    # 'https://www.linkedin.com/jobs2/api/jobPostings/search/?decorateFacets=false&distance=%s&count=%s&postalCode=%s&countryCode=es&sortBy=DD&start=%s'
    'https://www.linkedin.com/jobs2/api/jobPostings/search/?decorateFacets=false&distance=%s&count=%s&postalCode=%s&countryCode=es&sortBy=DD&start=%s&f_TP=1&f_E=%s'
)


def get_date_from_timestamp(value):
    if not value:
        return ''

    timestamp = value / 1000.0
    dt = datetime.datetime.utcfromtimestamp(timestamp)
    return dt.strftime("%Y/%m/%d")


class LinkedinSpider(Spider):
    name = 'linkedin'
    # start_urls = [
    #     'https://www.linkedin.com/jobs2/api/jobPostings/search?sortBy=R&distance=25&keywords=software',
    # ]
    allowed_domains = [
        'linkedin.com',
    ]
    custom_settings = {
        'COOKIES_ENABLED': False,
        'REFERER_ENABLED': False,
        # 'USER_AGENT': 'JobSeeker/32 (Android OS/6.0.1 LGE_Nexus 5)',
        'USER_AGENT': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:40.0) Gecko/20100101 Firefox/40.0',
        'DEFAULT_REQUEST_HEADERS': {
            'Accept': 'application/vnd.linkedin.jobs.v1+json',
            'Accept-Encoding': 'gzip',
        },
        'DOWNLOADER_MIDDLEWARES': {
            'asistech.middlewares.ProxyMiddleware': 740,
            'scrapy.downloadermiddlewares.retry.RetryMiddleware': None,
            'asistech.middlewares.FixedRetryMiddleware': 500,
        },
        'EXTENSIONS': {
            'scrapy.extensions.throttle.AutoThrottle': None,
            'asistech.extensions.CustomAutoThrottle': 0,
        },
        'AUTOTHROTTLE_ENABLED': True,
        'AUTOTHROTTLE_DEBUG': True,
        # 'AUTOTHROTTLE_MAX_DELAY': 60.0,
        # 'AUTOTHROTTLE_START_DELAY': 5.0,
        # 'AUTOTHROTTLE_TARGET_CONCURRENCY': 1.0,
        'CONCURRENT_REQUESTS': 1,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 1,
        'DOWNLOAD_DELAY': 1,
        'RETRY_TIMES': 29,
        'RETRY_HTTP_CODES': [500, 502, 503, 504, 408, 999],
        # 'RETRY_PRIORITY_ADJUST': -1,
        # 'CLOSESPIDER_PAGECOUNT': 1000,
        'ITEM_PIPELINES': {
            'asistech.pipelines.DuplicatesPipeline': 200,
            'asistech.pipelines.InsertDatePipeline': 300,
        },
        'ID_FIELD': 'jobPosting_id',
        'INSERT_DATE_FIELD': 'crawl_date',
        'SPIDERMON_VALIDATION_SCHEMAS': [
            resource_filename('asistech', 'spidermon/schemas/linkedin.json')
        ],
    }

    def __init__(self, *args, **kwargs):
        self.distance = float(kwargs.pop('distance', '25'))
        self.count = int(kwargs.pop('count', '50'))

    def start_requests(self):
        locations = {
            'madrid': 'es:5113',
            'barcelona': 'es:5064',
            'valencia': 'es:5152',
            'sevilla': 'es:5142',
            'zaragoza': 'es:5160',
            'malaga': 'es:5119',
            'murcia': 'es:5118',
            'palma de mallorca': 'postalCode=07011',
            'las palmas de gran canaria': 'es:5102',
            'bilbao': 'es:5065',
            'alicante': 'postalCode=03070',
            'cordoba': 'es:5079',
            'valladolid': 'es:5154',
            'la coruña': 'es:5046',
            'vitoria': 'es:5157',
            'granada': 'es:5086',
            'oviedo': 'es:5124',
            'santa cruz de tenerife': 'es:5136',
            'pamplona': 'es:5096',
            'almeria': 'es:5054',
            'san sebastian': 'postalCode=20015',
            'burgos': 'es:5066',
            'santander': 'es:5138',
            'castellon de la plana': 'postalCode=12071',
            'albacete': 'es:5048',
            'logroño': 'es:5110',
            'badajoz': 'es:5060',
            'salamanca': 'es:5134',
            'huelva': 'es:5092',
            'lerida': 'es:5109',
            'tarragona': 'es:5145',
            'leon': 'es:5106',
            'cadiz': 'es:5078',
            'jaen': 'es:5097',
            'orense': 'es:5123',
            'lugo': 'es:5112',
            'gerona': 'es:5085',
            'caceres': 'es:5076',
            'santiago de compostela': 'es:5139',
            'ceuta': 'es:5071',
            'melilla': 'es:5117',
            'guadalajara': 'es:5090',
            'toledo': 'es:5148',
            'pontevedra': 'postalCode=36164',
            'palencia': 'es:5125',
            'ciudad real': 'es:5072',
            'zamora': 'es:5158',
            'merida': 'es:5120',
            'avila': 'postalCode=05003',
            'cuenca': 'es:5075',
            'segovia': 'es:5140',
            'huesca': 'es:5094',
            'soria': 'es:5143',
            'teruel': 'es:5147',
        }

        for _, id_ in locations.items():
            for level in range(0, 7):
                if not id_.startswith('postalCode'):
                    meta = {
                        'location_id': id_,
                        'start': 0,
                        'experience': level,
                    }
                    url = BASE_URL % (
                        self.distance,
                        self.count,
                        meta['location_id'],
                        meta['start'],
                        meta['experience'],
                    )
                else:
                    meta = {
                        'postal_code': id_.replace('postalCode=', ''),
                        'start': 0,
                        'experience': level,
                    }
                    url = POSTAL_CODE_BASE_URL % (
                        self.distance,
                        self.count,
                        meta['postal_code'],
                        meta['start'],
                        meta['experience'],
                    )
                yield Request(url, meta=meta)

        # # Add postal codes
        # source_path = os.path.join(os.path.dirname(__file__), '..', '..', 'resources', 'spain_postal_codes.csv')
        # with open(source_path, 'rb') as f:
        #     data = f.read()

        # buff = StringIO.StringIO(data)
        # reader = csv.reader(buff)
        # first_row = True
        # locations = []
        # for row in reader:
        #     if first_row:
        #         first_row = False
        #         continue

        #     loc = {
        #         'postal_code': row[0].zfill(5),
        #         'lat': row[1],
        #         'lng': row[2],
        #     }
        #     locations.append(loc)

        # for loc in locations:
        #     meta = {
        #         'postal_code': loc['postal_code'],
        #         'start': 0,
        #     }
        #     url = POSTAL_CODE_BASE_URL % (
        #         self.distance,
        #         self.count,
        #         meta['postal_code'],
        #         meta['start'],
        #     )
        #     yield Request(url, meta=meta)

    def parse(self, response):
        data = json.loads(response.body_as_unicode())

        for record in data['decoratedJobPostings']['elements']:
            # {
            #     "decoratedCompany": {
            #         "isFollowing": false,
            #         "formattedEmployeeCount": "501-1000 employees",
            #         "company": {
            #             "companyId": 359933
            #         },
            #         "canonicalName": "Manpower Espa\u00f1a",
            #         "heroImageLink": "https://media.licdn.com/media/AAEAAQAAAAAAAAVuAAAAJDg0NDkxMmQ4LTQ2MWMtNDBlOC1iNjk4LTJmYzAyYTBlZTdhZg.png",
            #         "logoMediaLink": "https://media.licdn.com/mpr/mpr/shrink_100_100/AAEAAQAAAAAAAAXLAAAAJDk2MzI1OTY1LTAzN2YtNDQ0Ni1hNThkLTIzZDQ2MjZlY2IxZA.png",
            #         "formattedCompanyType": "Privately Held",
            #         "formattedCompanySize": "501-1000",
            #         "formattedIndustries": [
            #             "Human Resources"
            #         ],
            #         "hasPaidLcp": true,
            #         "squareLogoMediaLink": "https://media.licdn.com/mpr/mpr/shrink_100_100/AAEAAQAAAAAAAAW5AAAAJDFhNTc1MWQ2LWJhMmQtNGUzZS05YmJhLTFkZmY3YzQ1MWI4MQ.png"
            #     },
            #     "companyName": "Manpower Espa\u00f1a",
            #     "hasDecoratedCompany": true,
            #     "applicationSetting": {
            #         "jobPosting": "urn:li:jobPosting:132398569",
            #         "applyMethod": {
            #             "com.linkedin.jobs.SimpleOnsiteApply": {}
            #         }
            #     },
            #     "offsiteApply": false,
            #     "jobPosting": {
            #         "applicationRouting": "LINKEDIN",
            #         "title": "Consultora/or RRHH Comercial",
            #         "listDate": 1462968456000,
            #         "expirationDate": 1465560455000,
            #         "id": 132398569
            #     },
            #     "formattedLocation": "Lugo y alrededores, Espa\u00f1a"
            # }
            try:
                item = {}
                item['jobPosting_listDate'] = get_date_from_timestamp(record['jobPosting']['listDate'])
                item['jobPosting_id'] = record['jobPosting']['id']
                item['jobPosting_applicationRouting'] = record['jobPosting']['applicationRouting']
                item['jobPosting_title'] = record['jobPosting']['title']
                item['jobPosting_expirationDate'] = get_date_from_timestamp(record['jobPosting']['expirationDate'])
                item['companyName'] = record['companyName']
                item['formattedLocation'] = record['formattedLocation']
                item['decoratedCompany_companyId'] = record.get('decoratedCompany', {}).get('company', {}).get('companyId', '')
                item['decoratedCompany_canonicalName'] = record.get('decoratedCompany', {}).get('canonicalName', '')
                item['decoratedCompany_isFollowing'] = record.get('decoratedCompany', {}).get('isFollowing', '')
                item['decoratedCompany_hasPaidLcp'] = record.get('decoratedCompany', {}).get('hasPaidLcp', '')
                item['decoratedCompany_heroImageLink'] = record.get('decoratedCompany', {}).get('heroImageLink', '')
                item['decoratedCompany_formattedEmployeeCount'] = record.get('decoratedCompany', {}).get('formattedEmployeeCount', '')
                item['decoratedCompany_formattedCompanySize'] = record.get('decoratedCompany', {}).get('formattedCompanySize', '')
                item['decoratedCompany_squareLogoMediaLink'] = record.get('decoratedCompany', {}).get('squareLogoMediaLink', '')
                item['decoratedCompany_formattedIndustries'] = record.get('decoratedCompany', {}).get('formattedIndustries', [])
                item['decoratedCompany_formattedCompanyType'] = record.get('decoratedCompany', {}).get('formattedCompanyType', '')
                item['decoratedCompany_logoMediaLink'] = record.get('decoratedCompany', {}).get('logoMediaLink', '')
                item['hasDecoratedCompany'] = record['hasDecoratedCompany']
                item['offsiteApply'] = record['offsiteApply']
                yield item
            except KeyError:
                self.logger.warning('Missing key: %s', record)

        # Paging object
        # {u'count': 20, u'start': 1, u'total': 134,
        # u'links': {u'next': u'/jobs2/api/jobPostings/search?distance=50.0&sortBy=R&f_L=es:5112&start=21&count=20&decorateFacets=false',
        # u'first': u'/jobs2/api/jobPostings/search?distance=50.0&sortBy=R&f_L=es:5112&start=0&count=20&decorateFacets=false'}}

        # LinkedIn's API returns an empty list when we go over the pagination
        # limit (1000 results per location)
        if data['decoratedJobPostings']['elements']:
            total = data['decoratedJobPostings']['paging']['total']
            start = response.meta['start'] + self.count

            if start < total:
                try:
                    meta = {
                        'location_id': response.meta['location_id'],
                        'start': start,
                        'experience': response.meta['experience'],
                    }
                    url = BASE_URL % (
                        self.distance,
                        self.count,
                        meta['location_id'],
                        meta['start'],
                        meta['experience'],
                    )
                except KeyError:
                    meta = {
                        'postal_code': response.meta['postal_code'],
                        'start': start,
                        'experience': response.meta['experience'],
                    }
                    url = POSTAL_CODE_BASE_URL % (
                        self.distance,
                        self.count,
                        meta['postal_code'],
                        meta['start'],
                        meta['experience'],
                    )
                yield Request(url, meta=meta)
