# -*- coding: utf-8 -*-
import json
import re

import scrapy
import w3lib

from asistech.items import AutocasionCarItemLoader, AutocasionSellerItemLoader
from pkg_resources import resource_filename


SELLER_CACHE = {}
SELLER_REFERENCE_RE = r'/([^/]+)/?$'


def iterloc(it, alt=False):
    for d in it:
        yield d['loc']


def str_to_bool(value):
    value = value.lower()
    if value in ('true', 'yes', '1'):
        return True
    elif value in ('false', 'no', '0'):
        return False
    else:
        raise ValueError


def parse_address_lines(address):
    if not address:
        return {}

    for line in address:
        if line.strip():
            value = line.strip()
            value = re.sub(r'[ \n]+', ' ', value)
            value = re.sub(r', [ \n]+', ' ', value)

            output = {}

            zipcode = re.search(r'(\d{3,5})\s*$', value)
            if zipcode:
                output['zip'] = zipcode.group(1)
                value = re.sub(r' *, (\d{3,5})\s*$', '', value)

            city = re.search(r', ([^,]+)$', value)
            if city:
                output['city'] = city.group(1)
                value = re.sub(r', ([^,]+)$', '', value)

            output['address'] = value.strip(' ,')

            return output

    return {}


class AutocasionSpider(scrapy.Spider):
    name = 'autocasion'

    allowed_domains = [
        'autocasion.com',
    ]

    custom_settings = {
        'COOKIES_ENABLED': False,
        # 'COOKIES_DEBUG': True,
        'USER_AGENT': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:56.0) Gecko/20100101 Firefox/56.0',
        'DEFAULT_REQUEST_HEADERS': {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3',
        },
        'CONCURRENT_REQUESTS': 1,
        # 'DOWNLOAD_DELAY': 1,
        # 'CLOSESPIDER_PAGECOUNT': 1000,
        'DOWNLOADER_MIDDLEWARES': {
            'asistech.middlewares.ProxyMiddleware': 740,
            'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
            'asistech.middlewares.CustomRandomUserAgentMiddleware': 399,
        },
        'SPIDERMON_VALIDATION_SCHEMAS': [
            resource_filename('asistech', 'spidermon/schemas/autocasion.json')
        ],
    }

    details_mappings = {
        'power': {'Potencia \(CV\)', 'Potencia'},
        'km': {u'Kilómetros'},
        'fuel': {'Combustible'},
        'bodyworktype': {u'Carrocería'},
        'doors': {u'Número de puertas'},
        'seats': {u'Número de plazas'},
        'co2_emissions': {'CO2 \(g/km\)', 'Co2'},
        'warranty': {u'Garantía'},
        'transmision': {u'Tipo de transmisión', 'Tipo de cambio'},

    }

    def __init__(self, *args, **kwargs):
        self.scrape_sellers = str_to_bool(kwargs.pop('scrape_sellers', 'True'))
        self.scrape_new_cars = str_to_bool(kwargs.pop('scrape_new_cars', 'True'))
        self.scrape_used_cars = str_to_bool(kwargs.pop('scrape_used_cars', 'True'))

        super(AutocasionSpider, self).__init__(*args, **kwargs)

    def start_requests(self):
        if self.scrape_used_cars:
            yield scrapy.Request(
                'https://www.autocasion.com/search/aggregation?aggs%5B%5D=brandsAggs&aggs%5B%5D=certificateAggs&aggs%5B%5D=guaranteedAggs&aggs%5B%5D=metallicColordAggs&filters%5Bclass_name%5D=AdUsedCar&buildUrls=false',
                callback=self.parse_filters,
                headers={
                    'X-Requested-With': 'XMLHttpRequest',
                },
                meta={
                    'type': 'used',
                }
            )

            industrial_vehicles = [
                'https://www.autocasion.com/vehiculos-industriales-segunda-mano/transporte',
                'https://www.autocasion.com/vehiculos-industriales-segunda-mano/agricultura-forestales-y-jardineria',
                'https://www.autocasion.com/vehiculos-industriales-segunda-mano/construccion',
                'https://www.autocasion.com/vehiculos-industriales-segunda-mano/almacenaje',
                'https://www.autocasion.com/vehiculos-industriales-segunda-mano/transporte/accesorios-y-repuestos-transporte',
                'https://www.autocasion.com/vehiculos-industriales-segunda-mano/agricultura-forestales-y-jardineria/accesorios-y-repuestos-agricultura',
                'https://www.autocasion.com/vehiculos-industriales-segunda-mano/construccion/accesorios-y-repuestos-construccion',
                'https://www.autocasion.com/vehiculos-industriales-segunda-mano/almacenaje/accesorios-y-repuestos-almacenaje',
            ]
            for url in industrial_vehicles:
                yield scrapy.Request(
                    url,
                    callback=self.parse_used_car_section,
                    meta={
                        'type': 'used',
                    }
                )

        if self.scrape_new_cars:
            yield scrapy.Request(
                'https://www.autocasion.com/search/aggregation?aggs%5B%5D=brandsAggs&aggs%5B%5D=featuredNewAggs&filters%5Bclass_name%5D=AdNewCar&buildUrls=false',
                callback=self.parse_filters,
                headers={
                    'X-Requested-With': 'XMLHttpRequest',
                },
                meta={
                    'type': 'new',
                }
            )

        # # For debugging a single URL/ seller
        # url = 'https://www.autocasion.com/vehiculos-industriales-segunda-mano-furgonetas/iveco-daily-ocasion/daily-daily-35s12-10-8-m3-1-ref3089474'
        # url = 'https://www.autocasion.com/coches-segunda-mano/volvo-xc90-ocasion/xc90-2-4-d5-kinetic-185-ref2596391'
        # meta = {'region': '', 'download_timeout': 180.0, 'depth': 117, 'proxy': 'http://185.166.214.99:8123',
        #         'download_latency': 0.3781309127807617, 'type': 'used', 'download_slot': 'www.autocasion.com',
        #         'brand_label': u'VOLVO'}
        # yield scrapy.Request(
        #     url,
        #     callback=self.parse_car,
        #     meta=meta,
        # )

    def parse_filters(self, response):
        json_response = json.loads(response.body)
        for brand in json_response['aggs']['brandsAggs']:
            self.crawler.stats.set_value('{}/{}/real'.format(response.meta['type'], brand['label']), brand['count'])
            yield scrapy.Request(
                response.urljoin(brand['url']['url']),
                callback=self.parse_used_car_section if response.meta['type'] =='used' else self.parse_new_car_section,
                meta={
                    'type': response.meta['type'],
                    'brand_label': brand['label'],
                }
            )

    def parse_used_car_section(self, response):
        # Get cars
        cars = response.xpath('//article[contains(@class, "anuncio")]')
        for car in cars:
            car_url = car.xpath('./a/@href').extract_first(default='').strip()
            if car_url:
                self.crawler.stats.inc_value('{}/{}/found'.format(response.meta['type'], response.meta.get('brand_label', '*')))
                meta = response.meta.copy()
                if car.xpath('.//li[./span[@title="Kilometros"] and contains(., "Demo")]').extract_first():
                    meta['vehicle_type'] = 'Demo'
                elif car.xpath('.//li[./span[@title="Kilometros"] and contains(., "Km0")]').extract_first():
                    meta['vehicle_type'] = 'Km 0'

                region = car.xpath('.//p[contains(@class, "provincia")]/text()').extract_first(default='')
                meta['region'] = region

                url = response.urljoin(car_url)
                yield scrapy.Request(
                    url,
                    callback=self.parse_car,
                    meta=meta,
                )

        # Get next page
        next_page = response.xpath('//div[contains(@class, "paginacion")]//a[contains(text(), "Siguiente")]/@href').extract_first(default='').strip()
        if next_page:
            yield scrapy.Request(
                response.urljoin(next_page),
                callback=self.parse_used_car_section,
                meta=response.meta.copy(),
            )

    def parse_new_car_section(self, response):
        # Get cars
        cars = response.xpath('//article[contains(@class, "anuncio")]')
        for car in cars:
            car_url = car.xpath('./a/@href').extract_first(default='').strip()
            if car_url:
                self.crawler.stats.inc_value('{}/{}/found'.format(response.meta['type'], response.meta.get('brand_label', '*')))
                meta = response.meta.copy()
                meta['vehicle_type'] = 'Nuevo'

                region = car.xpath('.//p[contains(@class, "provincia")]/text()').extract_first(default='')
                meta['region'] = region

                url = response.urljoin(car_url)
                yield scrapy.Request(
                    url,
                    callback=self.parse_car,
                    meta=meta,
                )

        # Get next page
        next_page = response.xpath('//div[contains(@class, "paginacion")]//a[contains(text(), "Siguiente")]/@href').extract_first(default='').strip()
        if next_page:
            url = response.urljoin(next_page)
            yield scrapy.Request(
                url,
                callback=self.parse_new_car_section,
                meta=response.meta.copy(),
            )

    def parse_car(self, response):
        self.crawler.stats.inc_value('{}/{}/confirmed'.format(response.meta['type'], response.meta.get('brand_label', '*')))
        loader = AutocasionCarItemLoader(response=response)

        loader.add_value('ad_external_id', response.url, re=r'-(?:ref)?(\d+)/?$')
        loader.add_value('ad_url', response.url)
        loader.add_xpath('ad_title', '//h1//text()')
        if response.meta.get('brand_label'):
            loader.add_value('mark', response.meta['brand_label'])
        else:
            loader.add_xpath('mark', '//script/text()', re=r"setTargeting\('marca', '([^']+)'")

        vehicle_type = response.meta.get('vehicle_type')
        if vehicle_type:
            loader.add_value('vehicle_type', vehicle_type)
        else:
            loader.add_value('vehicle_type', u'Ocasión')

        if 'vehiculos-industriales' in response.url:
            category = u'Industrial'
        else:
            category = u'Coche'
        loader.add_value('vehicle_category', category)

        loader.add_xpath('price', '//li[@class="precio"]/span/text()', re=r'([\d.,]+)')
        loader.add_xpath('price', '//div[contains(@class, "contactar-arriba")]/p[@class="precio"]/text()', re=r'([\d.,]+)')
        loader.add_css('price', '.titulo-ficha .precio p::text', re=r'([\d.,]+)')
        loader.add_css('price', '.barra-oscura .precio::text', re=r'([\d.,]+)')
        loader.add_value('currency', u'€')
        loader.add_value('updated_date', u'')  # not available
        loader.add_xpath('seller_name', '//div[contains(@class, "propietario-ficha") and contains(@class, "particular")]/p/span/text()')

        loader.add_xpath('region', '//div[contains(@class, "propietario-ficha") and contains(@class, "particular")]/p/text()', re=r'Vendedor particular de (.+)')
        loader.add_xpath('region', '//div[contains(@class, "propietario-ficha")]/h2[text()="Profesional"]/following-sibling::div//span/text()', re=r'\((.+)\)')
        loader.add_css('region', '.datos-concesionario p:not(.tit) span::text', re=r'\((.+)\)')
        loader.add_css('region', '.contactar-arriba p:not(.precio)::text', re='\s*en (.+?)\s*$')
        loader.add_value('region', response.meta.get('region', ''))

        # loader.add_value('seller_reference', u'')  # not available
        loader.add_value('seller_type', 'Particular' if response.xpath(
            '//div[contains(@class, "propietario-ficha") and contains(@class, "particular")]') else 'Profesional')

        loader.add_xpath('phone', '//div[contains(@class, "contactar-arriba")]//span/@data-phone')
        loader.add_xpath('phone', '//span[@data-phone-clicked]/@data-phone-clicked')
        loader.add_xpath('phone', '//span[contains(@class, "icon-operadora")]/text()')
        loader.add_xpath('phone', '//span[./span[contains(@class, "icon-telefono")]]/text()')
        loader.add_css('phone', '::attr(data-phone)')
        loader.add_css('phone', '.telefono ::text')
        loader.add_value('name_cperson', u'')  # not available

        loader.add_css('power', '[itemprop=vehicleEngine] [itemprop=name]::text')
        loader.add_xpath('power', '//li[./span[contains(@class, "icon-motor")]]/span[2]/text()')
        loader.add_xpath('km', '//li[./span[contains(@class, "icon-velocimetro")]]/text()')
        loader.add_xpath('registration_date', '//li[./span[contains(@class, "icon-calendario")]]//text()')
        loader.add_xpath('registration_date', '//p[contains(@class, "year") and ./span[contains(@class, "icon-calendario")]]//text()')
        loader.add_css('registration_date', '[itemprop=productionDate]::text')
        loader.add_xpath('fuel', '//li[./span[contains(@class, "icon-combustible")]]/text()')
        loader.add_css('fuel', '[itemprop=fuelType]::text')
        loader.add_xpath('doors', '//li[./span[contains(@class, "icon-puerta")]]/text()')

        loader.add_xpath('model', '//ul[contains(@class, "breadcrumb")]/li[@class="active"]/text()')
        loader.add_xpath('model', '//ul[contains(@class, "breadcrumb")]/li[@class="active"]/span[@itemprop="name"]/text()')
        loader.add_xpath('warranty', '//li[./span[contains(@class, "icon-llave-inglesa")]]/text()')
        loader.add_xpath('transmision', '//li[./span[contains(@class, "icon-cambio")]]/text()')
        loader.add_css('transmision', '[itemprop=numberOfForwardGears]::text')

        for field, aliases in self.details_mappings.items():
            for text in aliases:
                for node in ('.', '@title'):
                    base_regex = u'//li/span[re:test({}, "{}\s*:?\s*$", "i")]'.format(node, text)
                    loader.add_xpath(field, base_regex + '/following-sibling::span/text()')
                    loader.add_xpath(field, base_regex + '/parent::*/text()')

        loader.add_xpath('ad_description', '//h2[text()="Comentario del vendedor"]/following-sibling::*//text()')
        if not loader.get_output_value('ad_description'):
            loader.add_xpath('ad_description', '//div[@itemprop="description"]//text()')

        seller_url = response.xpath('//div[contains(@class, "propietario-ficha")]//a[contains(@href, "/profesional/")]/@href').extract_first(default='').strip()
        if seller_url:
            url = response.urljoin(seller_url)
            url = w3lib.url.url_query_cleaner(url, parameterlist=['tab'], remove=True)
            url = url.rstrip('?')
            if '?' in url:
                self.logger.info('Additional parameters on URL: %s', url)
                url = url.split('?', 1)[0]

            # loader.add_value('reference_id', url, re=r'-(\d+)(:?/?|\?[^/]+)$')
            # loader.add_value('seller_reference', url, re=r'-(\d+)(:?/?|\?[^/]+)$')
            loader.add_value('reference_id', url, re=SELLER_REFERENCE_RE)
            loader.add_value('seller_reference', url, re=SELLER_REFERENCE_RE)

            address_text = response.xpath('//div[contains(@class, "propietario-ficha")]/h2[text()="Profesional"]/following-sibling::div/p/text()').extract()
            if not address_text:
                address_text = response.css('.datos-concesionario p.direccion::text').extract()

            if self.scrape_sellers:
                meta = {
                    'address_text': address_text,
                    'region': loader.get_output_value('region'),
                }

                yield scrapy.Request(
                    url,
                    callback=self.parse_seller,
                    meta=meta,
                    priority=50,
                )

            if url.lower() in SELLER_CACHE:
                self.logger.debug('Loading data from the seller cache: %s', url)

                # Load data from the seller cache to avoid additional requests
                seller_info = SELLER_CACHE[url.lower()]

                if seller_info.get('seller_name'):
                    loader.add_value('seller_name', seller_info['seller_name'])
                if seller_info.get('website'):
                    loader.add_value('seller_url', seller_info['website'])
                if seller_info.get('address'):
                    loader.add_value('address', seller_info['address'])
                if seller_info.get('city'):
                    loader.add_value('city', seller_info['city'])
                if seller_info.get('zip'):
                    loader.add_value('zip', seller_info['zip'])

                yield loader.load_item()
            else:
                # Get the additional fields for the car item
                meta = {
                    'car_item': loader.load_item(),
                    'address_text': address_text,
                }
                yield scrapy.Request(
                    url,
                    callback=self.parse_seller,
                    meta=meta,
                    priority=25,
                    dont_filter=True,
                )
        else:
            yield loader.load_item()

    def parse_seller(self, response):
        if response.meta.get('car_item') is not None:
            loader = AutocasionCarItemLoader(
                item=response.meta['car_item'],
                response=response
            )

            loader.add_xpath('seller_name', '//div[contains(@class, "info")]/h1/text()')
            loader.add_xpath('seller_url', u'//h2[text()="Conócenos"]/following-sibling::p/text()', re=r'^((?:www\.|https?:).+)')

            # loader.add_xpath('address', '//h2[./span[contains(@class, "icon-ubicacion")]]/following-sibling::p/text()')
            address_components = parse_address_lines(response.meta.get('address_text', []))
            loader.add_value('address', address_components.get('address', ''))
            loader.add_value('city', address_components.get('city', ''))
            loader.add_value('zip', address_components.get('zip', ''))

            yield loader.load_item()
        else:
            loader = AutocasionSellerItemLoader(response=response)

            # loader.add_value('external_id', response.url, re=r'-(\d+)(:?/?|\?[^/]+)$')
            loader.add_value('external_id', response.url, re=SELLER_REFERENCE_RE)
            loader.add_value('url', response.url)

            loader.add_xpath('seller_name', '//div[contains(@class, "info")]/h1/text()')

            # loader.add_xpath('address', '//h2[./span[contains(@class, "icon-ubicacion")]]/following-sibling::p/text()')
            address_components = parse_address_lines(response.meta.get('address_text', []))
            loader.add_value('address', address_components.get('address', ''))
            loader.add_value('city', address_components.get('city', ''))
            loader.add_value('zip', address_components.get('zip', ''))

            loader.add_value('region', response.meta.get('region') or '')

            loader.add_xpath('phone', '//span[@data-phone-clicked]/@data-phone-clicked')
            loader.add_xpath('website', u'//p[@itemprop="url"]/text()')

            loader.add_value('since', '')  # not available
            # loader.add_value('seller_counter', '')  # not available

            item = loader.load_item()
            SELLER_CACHE[item['url'].lower()] = item

            yield item
