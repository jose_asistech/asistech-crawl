# Automatically created by: shub deploy

from setuptools import setup, find_packages

setup(
    name         = 'asistech',
    version      = '1.0',
    packages     = find_packages(),
    package_data={
        'asistech': ['resources/*.csv', 'spidermon/schemas/*.json']
    },
    entry_points = {'scrapy': ['settings = asistech.settings']},
)
