# -*- coding: utf-8 -*-
import re
import json
import math
from datetime import datetime

import requests
import scrapy
from scrapy.xlib.pydispatch import dispatcher
from w3lib.url import url_query_parameter, add_or_replace_parameter

from asistech.items import AutoScout24CarsItemLoader, AutoScout24SellersItemLoader
from asistech.exceptions import RetryRequest
from pkg_resources import resource_filename

CAR = 'C'
MOTORCYCLE = 'B'

class AutoScout24Spider(scrapy.Spider):
    name = 'autoscout24'
    custom_settings = {
        'COOKIES_ENABLED': True,
        'COOKIES_DEBUG': True,
        'USER_AGENT': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
        'DEFAULT_REQUEST_HEADERS': {
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
           'Accept-Encoding': 'gzip, deflate, sdch',
           'Accept-Language': 'en-US,en',
        },
        'SPIDERMON_VALIDATION_SCHEMAS': [
            resource_filename('asistech', 'spidermon/schemas/autoscout24.json')
        ],
    }

    vehicle_detail_link = 'https://www.autoscout24.es/anuncios/-{}'

    seller_types = {
        # 'P': 'Particular',
        # 'D': 'Vendedor'
        'P': 'Vendedor privado',
        'D': 'Vendedor profesional'
    }
    fuel_types = {
        "D": 'Diésel',
        "E": 'Eléctrico',
        "3": 'Electro/Diésel',
        "2": 'Electro/Gasolina',
        "M": 'Etanol',
        "G": 'Gas',
        "L": 'Gas licuado (GLP)',
        "C": 'Gas natural (CNG)',
        "B": 'Gasolina',
        "H": 'Hidrógeno',
        "O": 'Otros',
        "T": 'Gasolina 2T',
    }
    transmision_types = {
        "A": 'Automático',
        "M": 'Manual',
        "S": 'Semiautomático',
    }
    offer_types = {
        "D": 'Demostración',
        "S": 'KM0',
        "N": 'Nuevo',
        "U": 'Ocasión',
        "J": 'Seminuevo',
        "O": 'Clásico',
    }

    body_types_coche = {
        # Coche
        1: 'Coche pequeño',
        2: 'Cabrio',
        3: 'Coupé',
        4: 'SUV/4x4',
        5: 'Familiar',
        6: 'Sedán',
        7: 'Otros',
        12: 'Monovolumen',
        13: 'Furgoneta'
    }
    body_types_moto = {
        112: 'Scooter',
        103: 'Custom',
        101: 'Supersport',
        102: 'Sport touring',
        121: 'Racing',
        106: 'Moto de Enduro',
        122: 'Tourer',
        114: 'Super Moto',
        111: 'Moto de 3 ruedas',
        109: 'Sidecar',
        119: 'Rally',
        117: 'Naked',
        110: 'Clásica',
        104: 'Trail',
        113: 'Ciclomotor',
        118: 'Quad/ATV',
        116: 'Otros',
        108: 'Moto de Cross',
        120: 'Moto de Trial',
        105: 'Streetfighter',
        115: 'Minimoto',
    }

    vehicle_categories = {
        'C': 'coche',
        'B': 'moto',
    }

    filters_cars = {
        'gear': ['A', 'M', 'S'],
        'fuel': ['2', '3', 'B', 'C', 'D', 'E', 'H', 'L', 'M', 'O'],
        'offer': ['N', 'U,J,O,D', 'S'],
        'body': ['1', '2', '3', '4', '5', '6', '7', '12', '13'],
        'bcol': ['1','2','3','4','5','6','7','10','11','12','13','14','15','16'],
        'doors': [
            {'doorfrom': '1', 'doorto': '4'},
            {'doorfrom': '5', 'doorto': '5'},
            {'doorfrom': '6', 'doorto': '7'}
            ],
        'seats': [
            {'seatsfrom': '0', 'seatsto': '2'},
            {'seatsfrom': '3', 'seatsto': '3'},
            {'seatsfrom': '4', 'seatsto': '4'},
            {'seatsfrom': '5', 'seatsto': '5'},
            {'seatsfrom': '6', 'seatsto': '6'},
            {'seatsfrom': '7', 'seatsto': '7'},
            {'seatsfrom': '8', 'seatsto': '8'},
            {'seatsfrom': '9', 'seatsto': '75'},
            ]
    }

    filter_names_cars = ['bcol', 'seats', 'doors', 'body', 'gear', 'fuel', 'offer']

    filters_bikes = {
        'gear': ['A', 'M', 'S'],
        'fuel': ['2','B','D','E','L','O','T'],
        'offer': ['N', 'U,O,D', 'S'],
        'body': [str(body_type) for body_type in body_types_moto.keys()],
        'bcol': ['1','2','3','4','5','6','7','10','11','12','13','14','15','16'],
    }

    filter_names_bikes = ['gear', 'body', 'offer', 'bcol', 'fuel']

    unique_vehicles = set()

    def __init__(self, *args, **kwargs):
        super(AutoScout24Spider, self).__init__(*args, **kwargs)
        dispatcher.connect(self.spider_closed, scrapy.signals.spider_closed)

        vehicles_list_url = 'https://www.autoscout24.es/lst?sort=price&desc=1&ustate=N%2CU&size=20&page=1&cy=E&'
        self.start_urls = []

        # generate url for all filter combinations combinations of
        # vehicle category (car vs motor bike),
        # seller type (private vs dealer)
        # and registration year (for each year from from 1975 to the current year)
        for vehicle_category in self.vehicle_categories.keys():
            listing_url_cat = add_or_replace_parameter(vehicles_list_url, 'atype', vehicle_category)
            for seller_type in self.seller_types.keys():
                listing_url_cat_typ = add_or_replace_parameter(listing_url_cat, 'custtype', seller_type)
                for year in range(1975, datetime.today().year + 1):
                    listing_url_cat_typ_yr_f = add_or_replace_parameter(listing_url_cat_typ, 'fregfrom', year)
                    listing_url_cat_typ_yr_f_t = add_or_replace_parameter(listing_url_cat_typ_yr_f, 'fregto', year)
                    self.start_urls.append(listing_url_cat_typ_yr_f_t)
                    if year == datetime.today().year:
                        # For current year, skip the fregto param. seems to increase coverage.
                        self.start_urls.append(listing_url_cat_typ_yr_f)

        # self.start_urls = ['https://www.autoscout24.es/lst?sort=price&desc=1&ustate=N%2CU&size=20&page=10&cy=E&atype=C&custtype=D&fregfrom=2019&offer=N&fuel=E']

    def spider_closed(self, spider):
        print('total self.unique_vehicles: {}'.format(len(self.unique_vehicles)))

    def parse(self, response):
        is_first_page = True if url_query_parameter(response.url, 'page', '1') == '1' else False
        vehicle_listings = response.xpath('//*[contains(@class,"cl-list-element-gap")]')
        search_results_json = response.xpath('//*[contains(@class,"cl-listing-elements")]/div/as24-tracking[contains(@as24-tracking-value, "search_numberOfArticles")]/@as24-tracking-value').extract_first()
        if search_results_json:
            total_search_results = int(json.loads(search_results_json)['search_numberOfArticles'])
        else:
            total_search_results = 0
        is_in_desc_order = True if url_query_parameter(response.url, 'desc', '1') == '1' else False

        if not vehicle_listings:
            if is_first_page and is_in_desc_order and total_search_results==0:
                max_retry_count = 1
            else:
                max_retry_count = 5

            # we retry the page if no vehicles listed
            meta = response.meta
            retries_left = response.meta.get('retries_left', max_retry_count)
            if retries_left > 0:
                retries_left -= 1
                if max_retry_count == 5:
                    self.logger.warning("Vehicles expected. retries_left: %s. Retrying: %s. ",
                                        retries_left, response.url)
                else:
                    self.logger.debug("No vehicles. retries_left: %s. Retrying: %s. ",
                                      retries_left, response.url)

                meta['retries_left'] = retries_left
                yield scrapy.Request(
                    response.url,
                    callback=self.parse,
                    meta=meta,
                    dont_filter=True
                )
            else:
                if max_retry_count == 5:
                    self.logger.warning("Giving up retrying the non-first page: %s", response.url)
                else:
                    self.logger.debug("Giving up retrying the first page: %s", response.url)

            return
        vehicle_category = None
        print 'total vehicle_listings on {}: {}'.format(response.url, len(vehicle_listings))
        for vehicle_listing in vehicle_listings:
            # http://ww3.autoscout24.es/classified/276486228?asrc=st|as

            if vehicle_listing.xpath('.//div[@class="cl-list-item-error"]'):
                # Don't try to parse the item with errors.
                continue

            vehicle_detail_link = self.vehicle_detail_link.format(vehicle_listing.xpath('./@data-guid').extract_first())
            vehicle_category = vehicle_listing.xpath('./*/@data-vehicle-type').extract_first()
            if vehicle_detail_link in self.unique_vehicles:
                print '  {} already exists in self.unique_vehicles, found on {}'.format(vehicle_detail_link, response.url)
                continue
            else:
                print '  {} does not exist in self.unique_vehicles, found on {}'.format(vehicle_detail_link, response.url)
            self.unique_vehicles.add(vehicle_detail_link)

            il = AutoScout24CarsItemLoader(response=response)
            il.add_value('vehicle_category', self.vehicle_categories.get(vehicle_category)) # article type, C is car / coche, B is bike / moto.
            il.add_value('ad_url', vehicle_detail_link)
            il.add_value('posting_date_range', '')
            il.add_value('seller_reference', '')
            il.add_value('reference_id', '')

            region = vehicle_listing.xpath('.//span[@class="cldf-summary-seller-contact-country"]/text()').extract_first()
            il.add_value('region', region)

            title_p1 = vehicle_listing.xpath('.//h2[contains(@class,"cldt-summary-makemodel")]/text()').extract_first()
            title_p2 = vehicle_listing.xpath('.//h2[contains(@class,"cldt-summary-version")]/text()').extract_first()

            if title_p1:
                if title_p2:
                    title = "%s %s"%(title_p1, title_p2)
                else:
                    title = title_p1
            else:
                if title_p2:
                    title = title_p2
                else:
                    title = None

            il.add_value('ad_title', title)

            zip_city = vehicle_listing.xpath('.//span[contains(@class,"zip-city")]/text()').extract_first()
            if zip_city:
                zip_city = zip_city.split(' ')
                il.add_value('zip',zip_city[0] )
                il.add_value('city', ' '.join(zip_city[1:]))

            seller_type = vehicle_listing.xpath('.//div[@data-test="seller-type"]/text()').extract_first()
            il.add_value('seller_type', seller_type)

            seller_name = vehicle_listing.xpath('.//div[@data-test="company-name"]/text()').extract_first()
            il.add_value('seller_name', seller_name)

            name_cperson = vehicle_listing.xpath('.//div[@data-test="contact-name"]/text()').extract_first()
            il.add_value('name_cperson', name_cperson)

            vehicle_details = vehicle_listing.xpath('.//ul[@data-item-name="vehicle-details"]//li')
            KM_INDEX = 0
            REG_DATE_INDEX = 1
            POWER_INDEX = 2
            TRANSMISSION_INDEX = 5
            FUEL_INDEX = 6

            if vehicle_details[KM_INDEX].xpath('./@data-placeholder').extract_first()!='':
                il.add_value('km', vehicle_details[KM_INDEX].xpath('.//text()').extract_first())

            if vehicle_details[REG_DATE_INDEX].xpath('./@data-placeholder').extract_first()!='':
                il.add_value('registration_date', vehicle_details[REG_DATE_INDEX].xpath('.//text()').extract_first())

            if vehicle_details[POWER_INDEX].xpath('./@data-placeholder').extract_first()!='':
                il.add_value('power', vehicle_details[POWER_INDEX].xpath('.//text()').extract_first())

            if vehicle_details[TRANSMISSION_INDEX].xpath('./@data-placeholder').extract_first()!='':
                il.add_value('transmision', vehicle_details[TRANSMISSION_INDEX].xpath('.//text()').extract_first())

            if vehicle_details[FUEL_INDEX].xpath('./@data-placeholder').extract_first()!='':
                il.add_value('fuel', vehicle_details[FUEL_INDEX].xpath('.//text()').extract_first())

            price_details = ''.join(vehicle_listing.xpath('.//span[@data-item-name="price"]/text()').extract())
            price_details = price_details.split(' ')
            il.add_value('currency',price_details[0])
            il.add_value('price',price_details[1])

            yield scrapy.Request(
                vehicle_detail_link,
                callback=self.parse_vehicle_detail,
                errback=self._errback_vehicle_detail,
                meta={'item': il.load_item()},
            )

        filters_not_applied = True

        if is_in_desc_order and is_first_page:
            if total_search_results > 800:
                # dont paginate. add more filters.
                unapplied_filters = []
                selected_filter = ''
                filter_values = []
                filters_already_applied = response.meta.get('filters_already_applied', False)

                if filters_already_applied:
                    unapplied_filters = response.meta.get('unapplied_filters', [])
                    if unapplied_filters:
                        selected_filter = unapplied_filters.pop()
                        if vehicle_category == MOTORCYCLE:
                            filter_values = self.filters_bikes[selected_filter]
                        elif vehicle_category == CAR:
                            filter_values = self.filters_cars[selected_filter]
                    else:
                        selected_filter = ''
                        filter_values = []
                else:
                    if vehicle_category == MOTORCYCLE:
                        unapplied_filters = self.filter_names_bikes[:]
                        selected_filter = unapplied_filters.pop()
                        filter_values = self.filters_bikes[selected_filter]
                    elif vehicle_category == CAR:
                        unapplied_filters = self.filter_names_cars[:]
                        selected_filter = unapplied_filters.pop()
                        filter_values = self.filters_cars[selected_filter]

                if selected_filter:
                    filters_not_applied = False
                    for filter_value in filter_values:
                        listing_url = response.url
                        if isinstance(filter_value, dict):
                            for selected_filter_pair in filter_value.keys():
                                filter_pair_value = filter_value[selected_filter_pair]
                                listing_url = add_or_replace_parameter(listing_url, selected_filter_pair, filter_pair_value)
                        else:
                            listing_url = add_or_replace_parameter(listing_url, selected_filter, filter_value)
                        yield scrapy.Request(
                            listing_url,
                            callback=self.parse,
                            meta={
                                'filters_already_applied': True,
                                'unapplied_filters': unapplied_filters,
                            }
                        )
                else:
                    self.logger.warning("Possible loss of coverage. %s results on %s. no more filters left to apply", total_search_results, response.url)
                    filters_not_applied = True

            if total_search_results > 400 and filters_not_applied:
                # fetch all pages with reverse pagination. If 410 items, then fetch just page 1 with reverse
                self.logger.debug("Trying with reverse pagination. %s results on %s", total_search_results, response.url)
                remaining_search_results = min(total_search_results, 800) - 400
                total_pages = int(math.ceil(remaining_search_results / 20.0))
                listing_url_asc = add_or_replace_parameter(response.url, 'desc', '0')
                for page in range(1, total_pages + 2):
                    listing_url = add_or_replace_parameter(listing_url_asc, 'page', page)
                    yield scrapy.Request(listing_url, callback=self.parse)

            if filters_not_applied:
                # Just paginate and fetch all the pages.
                total_pages = int(math.ceil(min(total_search_results,400) / 20.0))
                for page in range(2, total_pages + 1):
                    listing_url = add_or_replace_parameter(response.url, 'page', page)
                    yield scrapy.Request(listing_url, callback=self.parse)

    def parse_vehicle_detail(self, response):
        il = AutoScout24CarsItemLoader(item=response.meta.get('item'), response=response)

        il.add_css('seller_url', 'a[data-type=dealerInfo]::attr(href)')
        il.add_css('seller_url', '.cdlt-dealer-rating a::attr(href)')
        il.add_xpath('ad_external_id', '//input[@name="classifiedId"]/@value')

        il.add_xpath('seller_type', '//div[contains(@class, "cldt-stage-vendor-data")]//span[.="Vendedor privado" or .="Vendedor profesional"]/text()')

        il.add_xpath('mark', u'(//dt[contains(text(), "Marca")]/following-sibling::dd)[1]//text()')
        il.add_xpath('model', u'(//dt[contains(text(), "Modelo")]/following-sibling::dd)[1]//text()')
        il.add_xpath('bodyworktype', u'(//dt[contains(text(), "Categoría")]/following-sibling::dd)[1]//text()')
        il.add_xpath('warranty', u'(//dt[contains(text(), "Garantía")]/following-sibling::dd)[1]//text()')
        il.add_xpath('doors', '(//dt[contains(text(), "Puertas")]/following-sibling::dd)[1]//text()', re=r'\d+')
        il.add_xpath('seats', '(//dt[contains(text(), "Plazas")]/following-sibling::dd)[1]//text()', re=r'\d+')

        il.add_xpath('phone', '//main//div[contains(@class, "cldt-stage-data")]//a[@name="stickyCallButton"]/@href', re=r'tel:(.+)') # use TakeFirst output processor, only take the first phone number. wait, but in the doc it shows all phone numbers. another phone_1 and so on?
        il.add_xpath('ad_description', '//div[@data-type="description"]//text()') # Join()
        il.add_xpath('co2_emissions', '(//dt[contains(text(), "Emisiones de CO2")]/following-sibling::dd)[1]//text()')

        # Because this part is wrapped in a <script> tag, it was not parsed properly by the Selector
        # we need to do a replace and create another selector to extract the address after
        # every other fields to avoid this replacement mangle / affect other fields' extractions
        # x = Selector(response.replace(body=response.body.replace('<script type="text/ng-template" id="contactTabTemplate">', '')))
        # address = x.xpath('//div[@data-test="customerAddress"]/text()').extract()
        # il.add_value('address', address)
        il.add_xpath('address', '(//div[@data-item-name="vendor-contact-street"])[1]/text()')
        item = il.load_item()

        if item.get('seller_url'):
            yield scrapy.Request(
                item['seller_url'],
                callback=self.parse_seller_detail
            )
            # pass
        elif item.get('seller_type') == u'Vendedor profesional':
            raise RetryRequest('Missing seller_url', meta={'item': item})

        yield item

    def _errback_vehicle_detail(self, failure):
        output = getattr(failure.value, 'response', failure.request)
        return output.meta.get('item')

    def parse_seller_detail(self, response):
        sil = AutoScout24SellersItemLoader(response=response)

        sil.add_value('url', response.url)
        sil.add_xpath('external_id', '//link[@rel="canonical"]/@href', re=r'https?://www\.autoscout24\.es/profesionales/(.+)')

        sil.add_value('region', 'Spain')
        sil.add_xpath('website', '//div[contains(@class, "dp-section")]/a[@name="contact-data"]/..//a[@rel="nofollow" and @target="_blank"]/@href')
        sil.add_value('since', '')  # intentional to stop breakage at customer side

        json_script = response.xpath('//script[contains(text(), "vehiclesAmount")]//text()').extract_first()
        try:
            data = json.loads(json_script)
        except Exception as ex:
            data = {}
            self.logger.warning('json not found!')

        dealer_info_page = data.get('dealerInfoPage', {})

        sil.add_value('seller_name', dealer_info_page.get('customerName', ''))
        sil.add_value('phone', dealer_info_page.get('customerPhoneNumbers', ''))

        address = dealer_info_page.get('customerAddress', {})
        if address:
            sil.add_value('address', address.get('street', ''))
            sil.add_value('city', address.get('city', ''))
            sil.add_value('zip', address.get('zipCode', ''))

        contact_persons = dealer_info_page.get('contactPersons', {})
        for person in contact_persons:
            sil.add_value('phone_cperson', person.get('phone', ''))
            sil.add_value('name_cperson', person.get('name', ''))
            sil.add_value('position_cperson', person.get('position', ''))

        vehicles_amount = data.get('list', {}).get('vehiclesAmount', {})
        vehicles_amount = int(vehicles_amount.get('cars', 0)) + int(vehicles_amount.get('bikes', 0))
        sil.add_value('total_counter', str(vehicles_amount))

        yield sil.load_item()
