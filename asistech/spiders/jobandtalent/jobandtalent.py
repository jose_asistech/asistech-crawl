# -*- coding: utf-8 -*-
import json
import datetime

from scrapy import Spider, Request
from pkg_resources import resource_filename


BASE_URL = (
    'https://api.jobandtalent.com/job_openings?auth_token=%s&page=%s&country_id=56&sort_by=_score'
)


def get_date_from_iso_string(value):
    if not value:
        return ''

    dt = datetime.datetime.strptime(value.split('+', 1)[0], "%Y-%m-%dT%H:%M:%S")
    return dt.strftime("%Y/%m/%d")


class JobandtalentSpider(Spider):
    name = 'jobandtalent'
    # start_urls = [
    #     'https://api.jobandtalent.com/job_openings?auth_token=VpzB7QZyDS6kqpzZ5qbW&page=1&country_id=56&province_id=7161&sort_by=_score',
    #     'https://api.jobandtalent.com/job_openings?auth_token=VpzB7QZyDS6kqpzZ5qbW&page=1&country_id=56&sort_by=_score',
    # ]
    allowed_domains = [
        'jobandtalent.com',
    ]
    custom_settings = {
        'COOKIES_ENABLED': False,
        'REFERER_ENABLED': False,
        'USER_AGENT': None,
        'DEFAULT_REQUEST_HEADERS': {
            'Accept': 'application/vnd.jobandtalent.v1+json',
            'Accept-Language': 'es_ES',
            'Content-Type': 'application/json',
            'X-Client': 'Jobandtalent-Android-v3.1.0',
        },
        'CONCURRENT_REQUESTS': 1,
        'DOWNLOAD_DELAY': 10,
        # 'CLOSESPIDER_PAGECOUNT': 1000,
        'ITEM_PIPELINES': {
            'asistech.pipelines.DuplicatesPipeline': 200,
            'asistech.pipelines.InsertDatePipeline': 300,
        },
        'ID_FIELD': 'ad_id',
        'INSERT_DATE_FIELD': 'crawl_date',
        'SPIDERMON_VALIDATION_SCHEMAS': [
            resource_filename('asistech', 'spidermon/schemas/jobandtalent.json')
        ],
    }

    def __init__(self, *args, **kwargs):
        self.auth_token = kwargs.pop('auth_token', 'VpzB7QZyDS6kqpzZ5qbW')

        super(JobandtalentSpider, self).__init__(*args, **kwargs)

    def start_requests(self):
        page = 1
        url = BASE_URL % (self.auth_token, page)
        yield Request(url, meta={'page': page})

    def parse(self, response):
        data = json.loads(response.body_as_unicode())

        for record in data:
            item = {}
            item['ad_id'] = record['id']
            item['name'] = record['name']
            item['location'] = record['location']
            item['company_name'] = record['company_name']
            item['avatar_url'] = record['avatar_url']
            item['contract_type'] = record['contract_type_name']
            item['areas_of_activity'] = record['areas_of_activity_names']
            item['synopsis'] = record['synopsis']
            item['url'] = record['url']
            item['external_url'] = record['external_url']
            item['external_mobile_url'] = record['external_mobile_url']
            item['job_source'] = record['job_source']
            item['external'] = record['external']
            item['created_at'] = get_date_from_iso_string(record['created_at'])
            item['expires_on'] = get_date_from_iso_string(record['expires_on'])
            item['potential_referrers'] = record['potential_referrers']
            item['potential_referrers_count'] = record['potential_referrers_count']
            item['applicable'] = record['applicable']
            item['external_applicable'] = record['external_applicable']
            item['premium_applicable'] = record['premium_applicable']
            item['job_application_id'] = record['job_application_id']
            yield item

        # per_page = int(response.headers['X-Per-Page'])
        per_page = 15
        total = int(response.headers['X-Total-Count'])
        page = response.meta['page']

        if total > page * per_page:
            page += 1
            url = BASE_URL % (self.auth_token, page)
            yield Request(url, meta={'page': page})
