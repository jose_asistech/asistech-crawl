# -*- coding: utf-8 -*-
import base64
import json
import re
from urllib import quote
from urlparse import urljoin

import demjson
import scrapy
from pkg_resources import resource_filename

from asistech.items import EnalquilerProfessionalItemLoader, EnalquilerPropertyItemLoader

# Provices info from enalquiler.com:
# "provinces": [
#                     {
#         "name"  : "A Coruña",
#         "value" : "18",
#         "quantity" : 3871
#     },
#                     {
#         "name"  : "Alava",
#         "value" : "1",
#         "quantity" : 215
#     },
#                     {
#         "name"  : "Albacete",
#         "value" : "2",
#         "quantity" : 934
#     },
#                     {
#         "name"  : "Alicante / Alacant",
#         "value" : "3",
#         "quantity" : 16806
#     },
#                     {
#         "name"  : "Almería",
#         "value" : "4",
#         "quantity" : 3050
#     },
#                     {
#         "name"  : "Andorra",
#         "value" : "53",
#         "quantity" : 223
#     },
#                     {
#         "name"  : "Asturias",
#         "value" : "5",
#         "quantity" : 2941
#     },
#                     {
#         "name"  : "Ávila",
#         "value" : "6",
#         "quantity" : 234
#     },
#                     {
#         "name"  : "Badajoz",
#         "value" : "7",
#         "quantity" : 1447
#     },
#                     {
#         "name"  : "Barcelona",
#         "value" : "9",
#         "quantity" : 7760
#     },
#                     {
#         "name"  : "Burgos",
#         "value" : "10",
#         "quantity" : 1254
#     },
#                     {
#         "name"  : "Cáceres",
#         "value" : "11",
#         "quantity" : 586
#     },
#                     {
#         "name"  : "Cádiz",
#         "value" : "12",
#         "quantity" : 5353
#     },
#                     {
#         "name"  : "Cantabria",
#         "value" : "13",
#         "quantity" : 2620
#     },
#                     {
#         "name"  : "Castellón / Castelló",
#         "value" : "14",
#         "quantity" : 2286
#     },
#                     {
#         "name"  : "Ceuta",
#         "value" : "15",
#         "quantity" : 46
#     },
#                     {
#         "name"  : "Ciudad Real",
#         "value" : "16",
#         "quantity" : 2206
#     },
#                     {
#         "name"  : "Córdoba",
#         "value" : "17",
#         "quantity" : 1928
#     },
#                     {
#         "name"  : "Cuenca",
#         "value" : "19",
#         "quantity" : 589
#     },
#                     {
#         "name"  : "Girona",
#         "value" : "20",
#         "quantity" : 1224
#     },
#                     {
#         "name"  : "Granada",
#         "value" : "21",
#         "quantity" : 4761
#     },
#                     {
#         "name"  : "Guadalajara",
#         "value" : "22",
#         "quantity" : 366
#     },
#                     {
#         "name"  : "Guipúzcoa",
#         "value" : "23",
#         "quantity" : 247
#     },
#                     {
#         "name"  : "Huelva",
#         "value" : "24",
#         "quantity" : 1219
#     },
#                     {
#         "name"  : "Huesca",
#         "value" : "25",
#         "quantity" : 220
#     },
#                     {
#         "name"  : "Illes Balears",
#         "value" : "8",
#         "quantity" : 1381
#     },
#                     {
#         "name"  : "Jaén",
#         "value" : "26",
#         "quantity" : 757
#     },
#                     {
#         "name"  : "La Rioja",
#         "value" : "39",
#         "quantity" : 468
#     },
#                     {
#         "name"  : "Las Palmas",
#         "value" : "37",
#         "quantity" : 1580
#     },
#                     {
#         "name"  : "León",
#         "value" : "27",
#         "quantity" : 2137
#     },
#                     {
#         "name"  : "Lleida",
#         "value" : "29",
#         "quantity" : 1454
#     },
#                     {
#         "name"  : "Lugo",
#         "value" : "28",
#         "quantity" : 368
#     },
#                     {
#         "name"  : "Madrid",
#         "value" : "30",
#         "quantity" : 9805
#     },
#                     {
#         "name"  : "Málaga",
#         "value" : "31",
#         "quantity" : 12031
#     },
#                     {
#         "name"  : "Melilla",
#         "value" : "32",
#         "quantity" : 51
#     },
#                     {
#         "name"  : "Murcia",
#         "value" : "33",
#         "quantity" : 3789
#     },
#                     {
#         "name"  : "Navarra",
#         "value" : "34",
#         "quantity" : 842
#     },
#                     {
#         "name"  : "Ourense",
#         "value" : "35",
#         "quantity" : 912
#     },
#                     {
#         "name"  : "Palencia",
#         "value" : "36",
#         "quantity" : 280
#     },
#                     {
#         "name"  : "Pontevedra",
#         "value" : "38",
#         "quantity" : 2852
#     },
#                     {
#         "name"  : "Salamanca",
#         "value" : "40",
#         "quantity" : 3841
#     },
#                     {
#         "name"  : "Santa Cruz de Tenerife",
#         "value" : "41",
#         "quantity" : 2015
#     },
#                     {
#         "name"  : "Segovia",
#         "value" : "42",
#         "quantity" : 1117
#     },
#                     {
#         "name"  : "Sevilla",
#         "value" : "43",
#         "quantity" : 4993
#     },
#                     {
#         "name"  : "Soria",
#         "value" : "44",
#         "quantity" : 107
#     },
#                     {
#         "name"  : "Tarragona",
#         "value" : "45",
#         "quantity" : 2188
#     },
#                     {
#         "name"  : "Teruel",
#         "value" : "46",
#         "quantity" : 68
#     },
#                     {
#         "name"  : "Toledo",
#         "value" : "47",
#         "quantity" : 2087
#     },
#                     {
#         "name"  : "Valencia / València",
#         "value" : "48",
#         "quantity" : 10795
#     },
#                     {
#         "name"  : "Valladolid",
#         "value" : "49",
#         "quantity" : 870
#     },
#                     {
#         "name"  : "Vizcaya",
#         "value" : "50",
#         "quantity" : 1672
#     },
#                     {
#         "name"  : "Zamora",
#         "value" : "51",
#         "quantity" : 354
#     },
#                     {
#         "name"  : "Zaragoza",
#         "value" : "52",
#         "quantity" : 1033
#     },
#                 ]

# Test urls
# properties
_TEST_PROPERTIES = [
    'http://www.enalquiler.com/alquiler_piso_cordoba/alquiler-piso-ascensor-poniente-sur_4455802.html',
    'http://www.enalquiler.com/alquiler_piso_corredoria-oviedo/alquilo-piso-en-oviedo-zona-palacio-de-los-deportes_4405582.html',
]
# proffesional profiles
_TEST_PROFILES = [
    'http://www.enalquiler.com/microsite/pisos-en-alquiler-por-grupo-torresol_3426.html',
]


PROFESSIONAL_CACHE = {}


class EnalquilerSpider(scrapy.Spider):
    name = 'enalquiler'
    start_urls = [
        'http://www.enalquiler.com/',
    ]
    allowed_domains = [
        'enalquiler.com',
    ]
    custom_settings = {
        'COOKIES_ENABLED': False,
        # 'COOKIES_DEBUG': True,
        'USER_AGENT': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:40.0) Gecko/20100101 Firefox/40.0',
        'DEFAULT_REQUEST_HEADERS': {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3',
        },
        'CONCURRENT_REQUESTS': 1,
        'SPIDERMON_VALIDATION_SCHEMAS': [
            resource_filename('asistech', 'spidermon/schemas/enalquiler.json')
        ],
        #'DOWNLOAD_DELAY': 0.5,
        #'CLOSESPIDER_PAGECOUNT': 1000,
    }

    SHUFFLE_PROVINCES = True

    def __init__(self, *args, **kwargs):
        super(EnalquilerSpider, self).__init__(*args, **kwargs)

        provinces = kwargs.get('provinces', [])
        if not isinstance(provinces, list):
            provinces = provinces.split(',')
        self.provinces = provinces

        self.test_property_link = kwargs.pop('property_link', None)
        self.test_profile_link = kwargs.pop('profile_link', None)

        excluded_provinces = kwargs.get('excluded-provinces', [])
        if not isinstance(excluded_provinces, list):
            excluded_provinces = excluded_provinces.split(',')
        self.excluded_provinces = excluded_provinces

    def start_requests(self):
        if self.test_property_link is not None:
            if self.test_property_link == 'all':
                for link in _TEST_PROPERTIES:
                    yield scrapy.Request(link, callback=self.parse_property_page, dont_filter=True)
            else:
                yield scrapy.Request(self.test_property_link, callback=self.parse_property_page, dont_filter=True)
        elif self.test_profile_link is not None:
            if self.test_profile_link == 'all':
                for link in _TEST_PROFILES:
                    yield scrapy.Request(link, callback=self.parse_professional_profile_page, dont_filter=True)
            else:
                yield scrapy.Request(self.test_profile_link, callback=self.parse_professional_profile_page, dont_filter=True)
        else:
            for request in super(EnalquilerSpider, self).start_requests():
                yield request

    def parse(self, response):
        m = re.search(r'"provinces" *: +(\[[^\]]+\])', response.body_as_unicode())
        if m:
            provinces = m.group(1)
            province_ids = re.findall(r'"value" +: +"(\d+)"', provinces)
        else:
            province_ids = []

        self.logger.info("Provinces: %s", province_ids)

        if self.SHUFFLE_PROVINCES:
            import random
            random.shuffle(province_ids)
        for pid in province_ids:
            if not self.provinces or pid in self.provinces:
                if not self.excluded_provinces or pid not in self.excluded_provinces:
                    url = 'http://www.enalquiler.com/search?provincia=' + pid
                    yield scrapy.Request(
                        url,
                        callback=self.parse_search_page,
                        priority=10,
                    )

    def parse_search_page(self, response):
        # Get all properties
        # Different pages show different classes for the property items.
        property_items = response.xpath('//li[contains(@class, "property-item")]')
        if not property_items:
            property_items = response.xpath('//li[contains(@class, "propertyCard")]')
        for prop in property_items:
            # Follow the property link
            href = prop.xpath('.//a[@itemprop="url"]/@href').extract_first(default='').strip()
            if href:
                yield scrapy.Request(
                    href,
                    callback=self.parse_property_page,
                    priority=40,
                )

        # Go to the next page
        for url in response.css('.pagination ::attr(href)').extract():
            url = urljoin(response.url, url)
            yield scrapy.Request(
                url,
                callback=self.parse_search_page,
                priority=20,
            )

    def parse_property_page(self, response):
        hxs = scrapy.Selector(response=response)
        loader = EnalquilerPropertyItemLoader(response=response)
        # property_data = hxs.re('mainConfig = ({.+?};)')
        property_data = re.findall('mainConfig = ({.+?});', response.body_as_unicode(), re.S)
        if property_data:
            try:
                property_data = demjson.decode(property_data[0])['propertyData']
            except demjson.JSONDecodeError:
                self.logger.warning("JSON decode error: %s"%response.url)
                pass

        # 1st priority
        loader.add_value('ad_external_id', response.url, re=r'_(\d+)\.html$')
        loader.add_value('ad_url', response.url)
        loader.add_xpath('posting_date_range', '//p[contains(@class, "detail-info-update")]//text()', re=r': *(\d+/\d+/\d+)')
        loader.add_value('ad_title', property_data.get('title'))
        loader.add_css('seller_reference', '.contact-box-bottom a::attr(href)', re=r'_(\d+)\.html$')

        buy_choice = response.xpath(u'//div[contains(@class, "detail-buy-choices") and contains(.//text(), "Alquiler con opción a compra")]').extract()
        if buy_choice:
            loader.add_value('transaction_type', u'Alquiler con opción a compra')
        else:
            loader.add_value('transaction_type', u'Alquiler')

        loader.add_value('subtype', property_data.get('type'))

        loader.add_value('zip_code', u'')  # not available
        loader.add_xpath('location_municipio', u'//address//div[./b[contains(text(), "Población:")]]/text()')
        loader.add_xpath('location_provincia', u'//address//div[./b[contains(text(), "Provincia:")]]/text()')

        if property_data.get('price', ''):
            float_price = float(property_data['price'])
            if int(float_price) == float_price:
                loader.add_value('price', str(int(float_price)))
            else:
                loader.add_value('price', str(property_data['price']))
        if loader.get_output_value('price'):
            loader.add_value('currency', u'€')
        loader.add_value('images', str(len(property_data['imagesList'])))
        loader.add_xpath('updated_date', '//p[contains(@class, "detail-info-update")]//text()', re=r': *(\d+/\d+/\d+)')

        # 2nd priority
        loader.add_value('reference_id', property_data.get('propertyRef'))

        loader.add_xpath('address', u'//address//div[./b[contains(text(), "Dirección:")]]/text()')
        loader.add_xpath('location_barrio', u'//address//div[./b[contains(text(), "Barrio:")]]/text()')
        loader.add_xpath('location_distrito', u'//address//div[./b[contains(text(), "Distrito/Zona:")]]/text()')


        if property_data.get('previousSellingPrice', 0):
            loader.add_value('price_down', str(property_data.get('previousSellingPrice', 0) - property_data.get('price', 0)))

        loader.add_xpath('deposit', '//div[contains(@class, "detail-deposit")]/text()', re=r'([\d.,]+)')
        loader.add_value('visits', u'')  # not available
        loader.add_value('favorites', u'')  # not available
        loader.add_value('contact_emails', u'')  # not available

        # 3rd priority
        loader.add_css('description', '#description::text')

        loader.add_xpath('total_built_size', '//ul[contains(@class, "infoResume")]/li[1]/text()', re=r'\d+')
        loader.add_xpath('n_rooms', '//ul[contains(@class, "infoResume")]/li[2]/text()', re=r'\d+')
        loader.add_xpath('n_bathrooms', '//ul[contains(@class, "infoResume")]/li[3]/text()', re=r'\d+')


        features = [''.join(feature.xpath('(./span/text() | ./text())').extract()).strip() for feature in
                    response.css('ul.property-characteristics-block-list li:not(.disabled)')]

        if 'Ascensor' in features:
            loader.add_value('elevator', u'Sí')

        for feature in features:
            if 'planta' in feature:
                loader.add_value('floor', feature)
                break

        if 'Exterior' in features:
            loader.add_value('ext_or_int', u'Exterior')
        elif 'Interior' in features:
            loader.add_value('ext_or_int', u'Interior')

        if 'Piscina' in features:
            loader.add_value('swimming_pool', u'Sí')

        if u'Jardín' in features:
            loader.add_value('garden', u'Sí')

        loader.add_xpath('zone_info', '//div[contains(@class, "property-location-info")]/*[contains(., "sobre esta zona")]/following-sibling::div/text()')

        loader.add_value('features', '\n'.join('- ' + feature for feature in features))

        item = loader.load_item()

        # Get some data used in the next step
        property_id = re.search(r'_(\d+)\.html$', response.url)
        if property_id:
            property_id = property_id.group(1)
        else:
            property_id = None
        control_key = property_data.get('controlKey')
        meta = {
            'property_id': property_id,
            'control_key': control_key,
            'referrer': response.url,
        }

        has_agency_info = False
        for prop in response.css('.contact-box-bottom a'):
            has_agency_info = True

            # it has a link to the agency
            item['seller_type'] = 'Agencia acreditada'

            # Get some data used in the next step
            trusted = prop.xpath('.//div[contains(., "Agencia acreditada")]').extract()
            meta['trusted_agency'] = trusted

            # Follow the professional link
            href = prop.xpath('./@href').extract_first(default='').strip()
            if href:
                yield scrapy.Request(
                    href,
                    callback=self.parse_professional_profile_page,
                    meta=meta,
                    priority=60,
                )

                meta['item'] = item

                if item.get('seller_reference') and item['seller_reference'] in PROFESSIONAL_CACHE:
                    self.logger.info("Read from cached professional: %s", item['seller_reference'])
                    professional_item = PROFESSIONAL_CACHE[item['seller_reference']]
                    item['seller_url'] = professional_item.get('website')
                    item['seller_microsite'] = professional_item.get('url')

                    req = self.build_phone_request(self.parse_property_phone_info, meta)
                    if req:
                        yield req
                    else:
                        yield item
                else:
                    # Get some additional fields for the property item
                    yield scrapy.Request(
                        href,
                        callback=self.parse_professional_profile_page,
                        errback=self.handle_error,
                        meta=meta,
                        priority=50,
                        dont_filter=True
                    )
            else:
                meta['item'] = item
                req = self.build_phone_request(self.parse_property_phone_info, meta)
                if req:
                    yield req
                else:
                    yield item
            break

        if not has_agency_info:
            if property_data['userType'] == 2:
                item['seller_type'] = 'Agencia'
            else:
                item['seller_type'] = 'Particular'
            meta['item'] = item
            req = self.build_phone_request(self.parse_property_phone_info, meta)
            if req:
                yield req
            else:
                yield item

    def parse_professional_profile_page(self, response):
        if response.meta.get('item') is not None:
            loader = EnalquilerPropertyItemLoader(item=response.meta['item'], response=response)

            loader.add_xpath('seller_url', '//a[@class="microsite-url"]/@href')
            loader.add_value('seller_microsite', response.url)

            item = loader.load_item()

            meta = response.meta
            meta['item'] = item

            req = self.build_phone_request(self.parse_property_phone_info, meta)
            if req:
                yield req
            else:
                yield item
        else:
            loader = EnalquilerProfessionalItemLoader(response=response)

            loader.add_value('external_id', response.url, re=r'_(\d+)\.html$')
            loader.add_value('url', response.url)
            loader.add_css('seller_name', 'div.microsite-name')

            address = response.xpath('//div[contains(@class, "microsite-address")]/text()').extract()
            if address:
                if len(address) > 1:
                    loader.add_value('address', '\n'.join(address[:-1]))

                zipcode = re.search(r'^(\d{4,5})', address[-1])
                if zipcode:
                    loader.add_value('zip', zipcode.group(1))

                city = re.search(r'- +(.+) +\(.+\)$', address[-1])
                if city:
                    loader.add_value('city', city.group(1))

                region = re.search(r'\(([^(]+)\)$', address[-1])
                if region:
                    loader.add_value('region', region.group(1))
            else:
                self.logger.info("Address not found: %s", response.url)

            loader.add_css('phone', 'div.microsite-phone')
            if response.meta.get('trusted_agency'):
                loader.add_value('type_dealer', u'Agencia acreditada')
            else:
                loader.add_value('type_dealer', u'Agencia')
            loader.add_xpath('website', '//a[@class="microsite-url"]/@href')
            loader.add_value('since', u'')  # not available
            loader.add_css('rent_counter', 'span.property-list-title-count', re=r'[\d.,]+')

            control_key = response.meta.get('control_key')
            property_id = response.meta.get('property_id')
            if control_key and property_id:
                # AJAX request to get the phone and the contact name
                # http://www.enalquiler.com/property/ajax/phones/4094836/0/ficha/MTMxZjM1MTk4M2NhNzM2ZTRkZGY2ZDIyMzQ4ZDM3MDE%3D
                url = 'http://www.enalquiler.com/property/ajax/phones/' + property_id + '/0/ficha/'
                url += quote(base64.b64encode(control_key))

                headers = {
                    'Accept': 'application/json, text/javascript, */*; q=0.01',
                    'X-Requested-With': 'XMLHttpRequest',
                }
                referrer = response.meta.get('referrer')
                if referrer:
                    headers['Referer'] = referrer
                meta = {
                    'item': loader.load_item(),
                }
                yield scrapy.Request(
                    url,
                    headers=headers,
                    callback=self.parse_phone_info,
                    errback=self.handle_error,
                    meta=meta,
                    priority=100,
                )
            else:
                item = loader.load_item()
                professional_id = item.get('external_id')
                if professional_id:
                    PROFESSIONAL_CACHE[professional_id] = item
                yield item

    def parse_phone_info(self, response):
        item = response.meta['item']
        try:
            result = json.loads(response.body_as_unicode())
        except ValueError:
            yield item
        else:
            contact_name = result.get('contact')
            if contact_name:
                item['contact_name'] = contact_name
            contact_time = result.get('contactTime')
            if contact_time:
                item['contact_time'] = contact_time

            main_phones = item.get('phone', '')
            contact_phones = result.get('phones', [])
            self.logger.debug("Main phones: %s", main_phones.split(','))
            self.logger.debug("Property contact phones: %s", contact_phones)

            all_phones = []
            for mphone in main_phones.split(','):
                mphone = mphone.strip()
                if mphone and mphone not in all_phones:
                    all_phones.append(mphone)

            for cphone in contact_phones:
                if cphone and cphone not in all_phones:
                    all_phones.append(cphone)

            item['phone'] = ', '.join(all_phones)

            professional_id = item.get('external_id')
            if professional_id:
                PROFESSIONAL_CACHE[professional_id] = item
            yield item

    def parse_property_phone_info(self, response):
        item = response.meta['item']
        try:
            result = json.loads(response.body_as_unicode())
        except ValueError:
            yield item
        else:
            contact_name = result.get('contact')
            if contact_name:
                item['contact_person'] = contact_name
            contact_time = result.get('contactTime')
            if contact_time:
                item['contact_hours'] = contact_time

            contact_phones = result.get('phones', [])
            # self.logger.debug("Property contact phones: %s", contact_phones)

            all_phones = []
            for cphone in contact_phones:
                if cphone and cphone not in all_phones:
                    all_phones.append(cphone)

            item['phone'] = ', '.join(all_phones)

            yield item

    def handle_error(self, failure):
        request = failure.request
        item = request.meta['item']

        yield item

    def build_phone_request(self, callback, meta, priority=100):
        control_key = meta.get('control_key')
        property_id = meta.get('property_id')

        if control_key and property_id:
            # AJAX request to get the phone and the contact name
            # http://www.enalquiler.com/property/ajax/phones/4094836/0/ficha/MTMxZjM1MTk4M2NhNzM2ZTRkZGY2ZDIyMzQ4ZDM3MDE%3D
            url = 'http://www.enalquiler.com/property/ajax/phones/' + property_id + '/0/ficha/'
            url += quote(base64.b64encode(control_key))

            headers = {
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'X-Requested-With': 'XMLHttpRequest',
            }

            referrer = meta['referrer']
            if referrer:
                headers['Referer'] = referrer

            return scrapy.Request(
                url,
                headers=headers,
                callback=callback,
                errback=self.handle_error,
                meta=meta,
                priority=priority,
                dont_filter=True,
            )
