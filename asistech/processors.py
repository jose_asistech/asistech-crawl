# -*- coding: utf-8 -*-

import re
import datetime
import locale

from six.moves.urllib.parse import unquote


# FIXME: a better and more Pythonic way to do this?
phone_code = '+34 - '
def strip_international_phone_code(value):
    if isinstance(value, basestring):
        value = value.replace(phone_code, '')
    return value


num_re = re.compile(r'\d+')
def get_number(value):
    return ''.join(re.findall(num_re, value))


def to_number(value):
    try:
        return int(value)
    except ValueError:
        return float(value)


def unquote_urlquoted_unicode(value):
    original_value = value
    try:
        if not isinstance(value, bytes):
            value = value.encode('ascii')
        value = unquote(value)
        return value.decode('utf-8')
    except UnicodeError:
        return original_value


def cast_to_boolean(value):
    if value != 'No':
        return u'Sí'
    return value


def cast_to_date(value):
    try:
        return datetime.datetime.strptime(value, '%d/%m/%Y').date()
    except (ValueError, TypeError):
        pass


def get_date_range(value):
    today = datetime.date.today()
    if today - datetime.timedelta(days=1) <= value:
        return u'Últimas 24 horas'
    elif today - datetime.timedelta(days=3) <= value:
        return u'Últimos tres días'
    elif today - datetime.timedelta(days=7) <= value:
        return u'Última semana'
    elif today - datetime.timedelta(days=30) <= value:
        return u'Último mes'
    return u'Otros'


def remove_whitespace_from_phone(value):
    if not value:
        return value

    return re.sub(r'\s', '', value)


def filter_spanish_price(value):
    if value and re.match(r'[\d.]+(?:\,\d{1,2})?', value):
        return value


def convert_spanish_price(value):
    if value:
        # Spanish locale is not supported on Scrapy Cloud
        # locale.setlocale(locale.LC_NUMERIC, 'es_ES.UTF-8')
        # try:
        #     return locale.atoi(value)
        # except ValueError:
        #     return locale.atof(value)
        # finally:
        #     locale.resetlocale(locale.LC_NUMERIC)

        value = value.replace('.', '')
        value = value.replace(',', '.')

        return to_number(value)


def clean_phone(value):
    if value:
        # Remove all non-digit characters
        value = re.sub(r'[^0-9/]', '', value)
        # Assume that '/' is used to separate phone numbers and
        # join them using a comma
        value = value.replace('/', ', ')
        return value


def clean_seller_url(value):
    if value:
        return re.sub('/valoraciones$', '', value)
