# -*- coding: utf-8 -*-

import StringIO
import codecs
import csv
import json
import logging
import os
import re
import urlparse
from datetime import date

import demjson
import scrapy
import unidecode
from bs4 import UnicodeDammit
from pkg_resources import resource_filename

from asistech.items import YaencontrePropertyItemLoader, YaEncontreProfessionalItemLoader


# SELLER_DICT = {}

def get_next_page(url, cur_page):
    url_parts = list(urlparse.urlparse(url))
    path = url_parts[2]
    splitted_path = path.split('/')
    next_page = 'pag-{}'.format(cur_page + 1)
    if 'pag-' in splitted_path[-1]:
        splitted_path[-1] = next_page
    else:
        splitted_path.append(next_page)
    url_parts[2] = '/'.join(splitted_path)
    return urlparse.urlunparse(url_parts)


def regularize_json_quotation(single_quoted):
    '''
    https://stackoverflow.com/a/63862387/1599229
    '''
    c_list = list(single_quoted)
    in_single, in_double = False, False
    for i, c in enumerate(c_list):
        if c == u"'":
            if not in_double:
                in_single = not in_single
                c_list[i]= u'"'
        elif c == u'"':
            in_double = not in_double
    double_quoted = u"".join(c_list)
    return double_quoted


def remove_ctrl_chars(given_str):
    bad_chars = frozenset([0, 1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
        21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 127]
    )
    return u"".join([char for char in given_str if ord(char) not in bad_chars])


def unescape_json_unicode(json_str):
    py2_bytes = codecs.encode(json_str, 'utf-8', 'backslashreplace')
    py2_unicode = codecs.decode(py2_bytes, 'unicode-escape')
    return py2_unicode


def load_valid_json(byte_string):
    text = UnicodeDammit(byte_string).unicode_markup

    if not text:
        detwingled = UnicodeDammit.detwingle(byte_string)
        text = detwingled.decode('utf-8')

    text = unescape_json_unicode(text)
    text = remove_ctrl_chars(text)

    initial_error = None

    try:
        return json.loads(text)
    except Exception as e:
        initial_error = e

    logging.warning('Could not load json: trying operations')
    median_text = text

    try:
        #  the return value is always a 2-tuple: (object, error_list)
        return demjson.decode(text, encoding='utf-8', return_errors=True)[0]
    except:
        pass

    text = text.replace(u'}},"operation":', u'},"operation":') + u"}"

    try:
        return json.loads(text)
    except Exception as e:
        pass

    text = regularize_json_quotation(median_text)

    try:
        return json.loads(text)
    except Exception as e:
        pass

    logging.error('all operations failed: {} * json still bad: {}'.format(
        initial_error, median_text.encode('utf8')
    ))

    return {}


class YaEncontreSpider(scrapy.Spider):
    name = "yaencontre"

    custom_settings = {
        'COOKIES_ENABLED': True,
        'USER_AGENT': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
        'DOWNLOADER_MIDDLEWARES': {
            'asistech.middlewares.ProxyMiddleware': 740,
            'scrapy.downloadermiddlewares.retry.RetryMiddleware': None,
            'asistech.middlewares.FixedRetryMiddleware': 500,
        },
        'RETRY_TIMES': 3,
        'RETRY_HTTP_CODES': [410, 429, 405, 500, 502, 503, 504, 408, 999],
        'SPIDERMON_VALIDATION_SCHEMAS': [
            resource_filename('asistech', 'spidermon/schemas/yaencontre.json')
        ],
    }

    possible_choices = {
        'venta': {'pisos', 'viviendas', 'terrenos', 'oficinas', 'negocios', 'naves', 'garajes'},
        'alquiler': {'pisos', 'viviendas', 'terrenos', 'oficinas', 'negocios', 'naves', 'garajes'},
    }

    pagination_url = 'https://api.yaencontre.com/v1/search?orderBy=RELEVANCE&realEstateAgencyId={agency}&pageNumber={page}&pageSize=24&features=&lang=es&discarded='
    base_url = 'https://www.yaencontre.com/'

    initial_json_re = r'__INITIAL_STATE__\s*=\s*JSON.parse\("({.+})"\);'

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(YaEncontreSpider, cls).from_crawler(crawler, *args, **kwargs)
        # crawler.signals.connect(spider.spider_idle, signal=scrapy.signals.spider_idle)
        return spider

    # def spider_idle(self, spider):
    #     for _, item in SELLER_DICT.items():
    #         self.crawler.engine.scraper._process_spidermw_output(
    #             item,
    #             None,
    #             scrapy.http.Response(str(item['url'])),
    #             spider,
    #         )

    unique_urls = set()
    debug_search = False

    def start_requests(self):
        if hasattr(self, 'item_url'):
            yield scrapy.Request(self.item_url, callback=self.parse_item_page)
            return

        if hasattr(self, 'province'):
            for categ, proptypes in self.possible_choices.items():
                for prop in proptypes:
                    the_prov = self.province.replace('/', '-').lower()
                    url = 'https://www.yaencontre.com/{}/{}/{}-provincia'.format(categ, prop, the_prov)
                    yield scrapy.Request(
                        url,
                        callback=self.parse_muni_article,
                        meta={
                            'categ': categ,
                            'prop': prop,
                            'prov': the_prov,
                        }
                    )
            return

        if hasattr(self, 'search_article_url'):
            yield scrapy.Request(
                self.search_article_url,
                callback=self.parse_search_article,
                meta=dict(
                    prov='',
                    city='',
                    categ='',
                    prop='',
                    first_time=True,
                    seller=True,
                ),
            )
            return

        source_path = os.path.join(os.path.dirname(__file__), '..', '..', 'resources', 'spain.csv')
        with open(source_path, 'rb') as f:
            data = f.read()

        buff = StringIO.StringIO(data)
        # reader = unicode_csv_reader(buff)
        reader = csv.reader(buff)
        first_row = True
        provinces = set()
        for row in reader:
            if first_row:
                first_row = False
                continue
            provinces.add(row[1])

        for prov in provinces:
            new_prov = unidecode.unidecode(unicode(prov.decode('utf8'))).replace(' ', '-')
            for categ, proptypes in self.possible_choices.items():
                for prop in proptypes:
                    the_prov = new_prov.replace('/', '-').lower()
                    url = 'https://www.yaencontre.com/{}/{}/{}-provincia'.format(categ, prop, the_prov)
                    yield scrapy.Request(
                        url,
                        callback=self.parse_muni_article,
                        meta={
                            'categ': categ,
                            'prop': prop,
                            'prov': the_prov,
                        }
                    )

    def parse_muni_article(self, response):
        # parse_muni_article
        for muni_url in response.xpath('//*[@class="d-ellipsis"]/text()[contains(.,"Localidades")]/../../..//div[@class="toogle-item space-between"]/a/@href').extract():
            city = urlparse.urlparse(muni_url).path.split('/')[-1]
            # self.crawler.stats.set_value('{prov}/{city}/{categ}/{prop}/visited'.format(
            #     prov=response.meta.get('prov', ''),
            #     city=city,
            #     categ=response.meta.get('categ', ''),
            #     prop=response.meta.get('prop', ''),
            # ), 0)
            yield scrapy.Request(
                response.urljoin(muni_url),
                callback=self.parse_search_article,
                meta=dict(
                    prov=response.meta.get('prov', ''),
                    city=city,
                    categ=response.meta.get('categ', ''),
                    prop=response.meta.get('prop', ''),
                    first_time=True,
                ),
            )

    def parse_search_article(self, response):
        # parse_search_article
        if response.meta.get('first_time'):
            current_city_count = int(
                (response.css('h1.d-ellipsis::text').re_first('\d+\.?\d*') or '0').replace('.', ''))

            # self.crawler.stats.set_value('{prov}/{city}/{categ}/{prop}/should'.format(
            #     prov=response.meta.get('prov', ''),
            #     city=response.meta.get('city', ''),
            #     categ=response.meta.get('categ', ''),
            #     prop=response.meta.get('prop', ''),
            # ), int((response.css('h1.d-ellipsis::text').re_first('\d+\.?\d*') or '0').replace('.', '')))
        listings = response.xpath(
            '//article[contains(@class,"ThinProperty")]//h2[@itemprop="name"]//a/@href').extract() \
                   or response.css('.ThinPropertyList .title a::attr(href)').extract()
        for prop in listings:
            if prop:
                url = response.urljoin(prop)
                if url not in self.unique_urls:
                    self.unique_urls.add(url)
                    self.crawler.stats.inc_value('parse_search_article/listing_requested')
                    if not self.debug_search:
                        yield scrapy.Request(
                            url,
                            callback=self.parse_item_page,
                            meta=dict(
                                prov=response.meta.get('prov', ''),
                                city=response.meta.get('city', ''),
                                categ=response.meta.get('categ', ''),
                                prop=response.meta.get('prop', ''),
                            ),
                        )

        if not listings:
            a = 1

        if not response.meta.get('seller'):
            for url in response.css('.paginator a::attr(href)').extract():
                yield scrapy.Request(
                    response.urljoin(url),
                    callback=self.parse_search_article,
                    priority=40,
                    meta=dict(
                        prov=response.meta.get('prov', ''),
                        city=response.meta.get('city', ''),
                        categ=response.meta.get('categ', ''),
                        prop=response.meta.get('prop', ''),
                    ),
                )

            # extra barrios
            if response.meta.get('first_time') and current_city_count > 600:
                info = re.search(self.initial_json_re, response.text)
                if info:
                    info_json = load_valid_json(info.group(1))
                    for breadcrumb in info_json.get('breadcrumb', {}):
                        if int(breadcrumb.get('count') or 0) > 600:
                            yield scrapy.Request(
                                url=response.urljoin(breadcrumb['url']),
                                callback=self.parse_search_article,
                                meta=dict(
                                    prov=response.meta.get('prov', ''),
                                    city=response.meta.get('city', ''),
                                    categ=response.meta.get('categ', ''),
                                    prop=response.meta.get('prop', ''),
                                    barrio=True,
                                ),
                            )

                        # barrio_count = barrio.get('value', {}).get('count', 0)
                        # if barrio_count > 600:
                        #     for sub_barrio in barrio['children']:
                        #         yield scrapy.Request(
                        #             url=response.urljoin(sub_barrio['value']['url']),
                        #             callback=self.parse_search_article,
                        #             meta=dict(
                        #                 prov=response.meta.get('prov', ''),
                        #                 city=response.meta.get('city', ''),
                        #                 categ=response.meta.get('categ', ''),
                        #                 prop=response.meta.get('prop', ''),
                        #                 barrio=True,
                        #             ),
                        #         )

        else:
            self.crawler.stats.inc_value('seller/seller_in_meta')
            script = response.css('#ssr_initialState::text').extract_first()
            if script:
                self.crawler.stats.inc_value('seller/seller_in_meta_script_present')
                info = re.search(self.initial_json_re, script)
                if info:
                    self.crawler.stats.inc_value('seller/seller_in_meta_info_present')
                    info_json = load_valid_json(info.group(1))
                    if info_json:
                        self.crawler.stats.inc_value('seller/seller_in_meta_valid_json')
                        if info_json['realEstateAgency'].get('url'):
                            loader = YaEncontreProfessionalItemLoader(response=response)
                            external_id = info_json['realEstateAgency'].get('url').split('/')[-1]
                            loader.add_value('external_id', external_id)
                            loader.add_value('url', response.urljoin(info_json['realEstateAgency'].get('url')))
                            loader.add_value('seller_name', info_json['realEstateAgency'].get('name'))

                            if info_json['realEstateAgency'].get('address'):
                                if info_json['realEstateAgency']['address'].get('qualifiedName'):
                                    location = info_json['realEstateAgency']['address']['qualifiedName']
                                    location_parts = re.match(r'(\d+), (.+) \((.+)\)', location)
                                    if location_parts:
                                        loader.add_value('zip', location_parts.group(1))
                                        loader.add_value('city', location_parts.group(2))
                                        loader.add_value('region', location_parts.group(3))
                                    else:
                                        loader.add_value('city', location)
                                if info_json['realEstateAgency']['address'].get('geoLocation'):
                                    lat = str(info_json['realEstateAgency']['address']['geoLocation'].get('lat'))
                                    lon = str(info_json['realEstateAgency']['address']['geoLocation'].get('lon'))
                                    loader.add_value('address', lat + ' ' + lon)

                            if info_json['realEstateAgency'].get('virtualPhoneNumber'):
                                loader.add_value('phone', info_json['realEstateAgency']['virtualPhoneNumber'].split(' ')[1])
                            elif info_json['realEstateAgency'].get('phoneNumber'):
                                loader.add_value('phone', info_json['realEstateAgency']['phoneNumber'])

                            if info_json['realEstateAgency'].get('schedule'):
                                contact_times = []
                                for time in info_json['realEstateAgency']['schedule'][0].get('schedule'):
                                    contact_times.append(time.get('startTime') + '-' + time.get('endTime'))
                                loader.add_value('contact_time', '\n'.join(contact_times))

                            loader.add_value('type_dealer', 'Agencia acreditada')
                            loader.add_value('website', info_json['realEstateAgency'].get('web'))
                            loader.add_value('rent_counter', info_json['dataLayer']['dimensions']['resultsCount'])
                            loader.add_value('insert_date', date.today().strftime('%Y/%-m/%-d'))

                            # SELLER_DICT[external_id] = loader.load_item()
                            self.crawler.stats.inc_value('seller/agency_yielded_count')
                            yield loader.load_item()
                            # self.crawler.stats.inc_value('seller/{reference}'.format(
                            #     reference=external_id,
                            # ))
                            # self.crawler.stats.inc_value('seller_count')
                        if info_json['realEstateAgency']['commercialId']:
                            self.crawler.stats.inc_value('seller/agency_commercial_id_found_2')
                            yield scrapy.Request(
                                url=self.pagination_url.format(agency=info_json['realEstateAgency']['commercialId'],
                                                               page=2),
                                callback=self.parse_pagination_api,
                                meta={
                                    'agency': info_json['realEstateAgency']['commercialId'],
                                    'first_time': True,
                                }
                            )
                    else:
                        self.crawler.stats.inc_value('seller/seller_in_meta_invalid_json')
                        self.logger.warning('seller/seller_in_meta_invalid_json: %s', response.url)
                else:
                    self.logger.warning('seller/seller_in_meta_info_not_present: %s', response.url)
                    self.crawler.stats.inc_value('seller/seller_in_meta_info_not_present')
            else:
                self.crawler.stats.inc_value('seller/seller_in_meta_script_not_present')
                self.logger.warning('seller/seller_in_meta_script_not_present: %s', response.url)

        self.crawler.stats.set_value('search/count', len(self.unique_urls))

    def parse_pagination_api(self, response):
        response_json = json.loads(response.body)
        if response_json:
            for item in response_json['result']['items']:
                url = urlparse.urljoin(self.base_url, item['realEstate']['url'])
                self.unique_urls.add(url)
                if not self.debug_search:
                    yield scrapy.Request(
                        url,
                        callback=self.parse_item_page,
                    )
            if response.meta.get('first_time'):
                for page in range(response_json['result']['currentPage'], response_json['result']['numPages'] + 1):
                    yield scrapy.Request(
                        url=self.pagination_url.format(agency=response.meta['agency'], page=page + 1),
                        callback=self.parse_pagination_api,
                        meta={
                            'agency': response.meta['agency'],
                        }
                    )
        self.crawler.stats.set_value('search/count', len(self.unique_urls))

    def parse_item_page(self, response):
        # self.crawler.stats.inc_value('{prov}/{city}/{categ}/{prop}/visited'.format(
        #     prov=response.meta.get('prov', ''),
        #     city=response.meta.get('city', ''),
        #     categ=response.meta.get('categ', ''),
        #     prop=response.meta.get('prop', ''),
        # ))
        loader = YaencontrePropertyItemLoader(response=response)

        loader.add_value('ad_url', response.url)
        reference_id = response.xpath('//section[contains(@class, "ownerBox")]//p[contains(text(), "Ref. del inmueble")]//text()').re_first('(\d.*\d)')

        loader.add_value('reference_id', reference_id)
        loader.add_value('ad_external_id', reference_id)
        loader.add_xpath('ad_title', '//h1[@itemprop="name"]/text()')

        seller_url = response.xpath('//section[contains(@class, "ownerBox")]//a[@itemprop="url"]/@href').extract_first()
        if reference_id:
            seller_reference = reference_id
            loader.add_value('seller_reference', seller_reference)
        else:
            seller_reference = seller_url.split('/')[-1] if seller_url else ''
            loader.add_value('seller_reference', seller_reference)
        loader.add_value('seller_url', response.urljoin(seller_url))
        seller_type = ''

        if response.xpath('//div[@class="real-estate-agency"]//div[contains(text(), "Anunciante particular")]'):
            seller_type = 'Particular'
            self.crawler.stats.inc_value('seller/private_advertiser')

        elif response.xpath('//div[@class="real-estate-agency"]'):
            seller_type = 'Agencia acreditada'
            self.crawler.stats.inc_value('seller/agency')
        else:
            seller_type = 'Other'
            self.crawler.stats.inc_value('seller/other')
        loader.add_value('seller_type', seller_type)

        loader.add_xpath('description', '//section[@class="extras-sections__description"]//div/text()')
        loader.add_xpath('transaction_type', '//div[@class="breadcrumb-link1"]//a[2]/text()')
        loader.add_xpath('subtype', '//div[@class="breadcrumb-link1"]//a[3]/text()')
        loader.add_xpath('address', '//span[@class="streetAdress"]/text()')
        loader.add_xpath('location_municipio', '//div[@class="breadcrumb-link1"]//a[5]/text()')
        loader.add_xpath('location_provincia', '//div[@class="breadcrumb-link1"]//a[4]/text()', re='(.+) \(')

        address = response.xpath('//div[@class="details-info"]//h1/text()').extract_first()
        address_data = address.split(',')[-1].split(' en ') if address else ''
        if len(address_data) > 2:
            loader.add_value('location_distrito', address_data[1])
        if len(address_data) > 1:
            loader.add_value('location_barrio', address_data[0])

        script = response.css('#ssr_initialState::text').extract_first()
        if script:
            self.crawler.stats.inc_value('seller/seller_in_meta_script_present')
            info = re.search(self.initial_json_re, script)
            if info:
                self.crawler.stats.inc_value('seller/seller_in_meta_info_present')
                info_json = load_valid_json(info.group(1))
                if info_json:
                    try:
                        ph = info_json.get('details', {}).get('item', {}).get('owner', {}).get('data', {}).get(
                            'phoneNumber')
                        loader.add_value('phone', ph)
                    except:
                        pass

        loader.add_xpath('zip_code', '//span[@itemprop="streetAddress"]/text()', re='(\d+)')

        price = ''.join(response.css('#ModalDetails .header-info span.price::text').re('\d+'))
        loader.add_value('price', price)

        currency = response.css('#ModalDetails .header-info span.price meta[itemprop="priceCurrency"]').xpath('./@content').extract_first()
        if currency:
            loader.add_value('currency', currency)

        previous_price = ''.join(response.css('#ModalDetails .header-info span.previous-price-text').re('\d+'))
        if previous_price and previous_price.isdigit() and price and price.isdigit():
            price_down = int(previous_price)-int(price)
            if price_down > 0:
                loader.add_value('price_down', str(price_down))

        loader.add_xpath('floor', '//section[@class="extras-sections__characteristics"]//span[contains(text(), "pisos")]//preceding-sibling::div/text()')

        has_garden = response.css('.icon-garden')
        if has_garden:
            loader.add_value('garden', u'Sí')

        has_pool = response.css('.icon-swimmingPool')
        if has_pool:
            loader.add_value('swimming_pool', u'Sí')

        if response.xpath('//section[@class="extras-sections__oportunity"]//span[contains(text(), "obra nueva")]'):
            loader.add_value('promo', u'Sí')

        updated = response.xpath('//section[contains(@class, "ownerBox")]//p[contains(text(), "actualiza")]').re_first(r'(?<=: ).*$')
        if updated:
            updated = re.findall('\d+', updated)
            loader.add_value('updated_date', updated[2]+'/'+updated[1]+'/'+updated[0])

        loader.add_css('images', '.counter-photos ::text', re='\d+')

        loader.add_css('n_rooms', '.icon-room + span ::text')
        loader.add_css('n_bathrooms', '.icon-bath + span ::text')
        loader.add_css('total_built_size', '.icon-meter + span ::text', re='\d+')
        loader.add_xpath('energetic_certification', '//section[contains(h3/text(),"Certificado energ")]//*[contains(@class, "label-calificate")]/text()')

        if response.css('.icon-lift'):
            loader.add_value('elevator', u'Sí')

        # if SELLER_DICT.get(seller_reference):
        #     SELLER_DICT[seller_reference]["rent_counter"] += 1
        #     # self.crawler.stats.inc_value('seller/{reference}'.format(
        #     #     reference=seller_reference,
        #     # ))
        #     self.crawler.stats.inc_value('seller_count')
        if seller_type == 'Agencia acreditada':
            for x in self.parse_seller_info(response):
                yield x

        yield loader.load_item()

    def parse_seller_info(self, response):
        seller_info = (re.findall(self.initial_json_re, response.text) or ['{}'])[0]
        seller_json = load_valid_json(seller_info)
        if seller_json:
            urls = []
            if seller_json.get('url'):
                self.crawler.stats.inc_value('seller/seller_info_url')
                urls.append(seller_json.get('url'))

            if seller_json.get('data', {}).get('url'):
                self.crawler.stats.inc_value('seller/seller_info_data_url')
                urls.append(seller_json.get('data', {}).get('url'))
            if seller_json.get('details', {}).get('item', {}).get('owner', {}).get('data', {}).get('url'):
                self.crawler.stats.inc_value('seller/owner_url')
                urls.append(seller_json.get('details', {}).get('item', {}).get('owner', {}).get('data', {}).get('url'))
            for url in urls:
                yield scrapy.Request(
                    url=response.urljoin(url),
                    callback=self.parse_search_article,
                    meta={
                        'seller': True,
                    }
                )
            cid = seller_json.get('details', {}).get('item', {}).get('owner', {}).get('data', {}).get('commercialId',
                                                                                                      '')
            if cid:
                self.crawler.stats.inc_value('seller/agency_commercial_id_found')
                yield scrapy.Request(
                    url=self.pagination_url.format(
                        agency=cid,
                        page=1
                    ),
                    callback=self.parse_pagination_api,
                    meta={
                        'agency': cid,
                        'first_time': True,
                    }
                )
            else:
                self.logger.warning('CommercialID not found in seller_json: %s', response.url)
                self.crawler.stats.inc_value('seller/commercial_id_missing')

        else:
            self.logger.warning('seller_info json not valid: %s', response.url)
            self.crawler.stats.inc_value('seller/seller_info_json_not_valid')
