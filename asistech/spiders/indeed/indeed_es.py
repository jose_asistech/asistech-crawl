# -*- coding: utf-8 -*-
import json
import re

import scrapy
import w3lib

from asistech.items import IndeedJobItemLoader, IndeedCompanyItemLoader
from pkg_resources import resource_filename


AD_PARAM_CACHE = set()


def convert_date(relative_date):
    return relative_date


def convert_job_url(url):
    # Convert URLs like this one:
    # https://www.indeed.es/rc/clk?jk=4b02eb84a48e365f&fccid=4b6b88f4297099e5
    # into:
    # https://www.indeed.es/ver-oferta?jk=4b02eb84a48e365f
    job_key = w3lib.url.url_query_parameter(url, 'jk')
    if job_key:
        result = 'https://www.indeed.es/ver-oferta?jk=%s' % job_key
        return result


class IndeedESSpider(scrapy.Spider):
    name = 'indeed_es'
    start_urls = [
        'https://www.indeed.es/',
    ]
    allowed_domains = [
        'indeed.es',
    ]
    custom_settings = {
        # 'COOKIES_ENABLED': True,
        # 'COOKIES_DEBUG': True,
        'USER_AGENT': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:40.0) Gecko/20100101 Firefox/40.0',
        # 'DEFAULT_REQUEST_HEADERS': {
        #     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        #     'Accept-Encoding': 'gzip, deflate',
        #     'Accept-Language': 'es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3',
        # },
        'CONCURRENT_REQUESTS': 1,
        'DUPEFILTER_DEBUG': True,
        'ITEM_PIPELINES': {
            'asistech.pipelines.IndeedDuplicatesPipeline': 200,
            'asistech.pipelines.InsertDatePipeline': 300,
            'asistech.pipelines.DiscardItemsWithoutFields': 500,
            'asistech.pipelines.DefaultValuePipeline': 900,
            'asistech.pipelines.FlattenListPipeline': 1000,
        },
        'DOWNLOADER_MIDDLEWARES': {
            'asistech.middlewares.ProxyMiddleware': 740,
            'scrapy.downloadermiddlewares.retry.RetryMiddleware': None,
            'asistech.middlewares.FixedRetryMiddleware': 500,
        },
        'RETRY_HTTP_CODES': [500, 502, 503, 504, 408, 999],
        'SPIDERMON_VALIDATION_SCHEMAS': [
            resource_filename('asistech', 'spidermon/schemas/indeed_es.json')
        ],
    }

    # def __init__(self, *args, **kwargs):
    #     super(IndeedESSpider, self).__init__(*args, **kwargs)

    def parse(self, response):
        yield scrapy.Request(
            'https://www.indeed.es/browsejobs',
            callback=self.parse_index,
        )

    def parse_index(self, response):
        for url in response.xpath('//table[@id="categories"]//a/@href').extract():
            url = response.urljoin(url)
            yield scrapy.Request(
                url, callback=self.parse_category_index,
            )

    def parse_category_index(self, response):
        for url in response.xpath('//table[@id="titles"]//a[@class="action"]/@href').extract():
            url = response.urljoin(url)
            yield scrapy.Request(
                url, callback=self.parse_provinces,
            )

    def parse_provinces(self, response):
        for url in response.xpath('//table[@id="states"]//a/@href').extract():
            url = response.urljoin(url)
            yield scrapy.Request(
                url, callback=self.parse_cities,
            )

    def parse_cities(self, response):
        cities = response.xpath('//table[@id="cities"]//a/@href').extract()
        if not cities:
            # Follow the main link for this province
            main_link = response.xpath('//table[@id="browsejobs_main_content"]//h1/a/@href').extract_first()
            url = response.urljoin(main_link)
            # url = 'https://www.indeed.es/Ofertas-de-Programador-en-Madrid,-Madrid'
            yield scrapy.Request(
                url, callback=self.parse_job_listing,
            )
        else:
            for url in cities:
                url = response.urljoin(url)
                # url = 'https://www.indeed.es/Ofertas-de-Programador-en-Madrid,-Madrid'
                yield scrapy.Request(
                    url, callback=self.parse_job_listing,
                )

    def parse_job_listing(self, response):
        # Main section
        for row in response.xpath('//*[@id="resultsCol"]/div[has-class("row") and (@itemtype="http://schema.org/JobPosting" or @data-tn-component="organicJob")]'):
            loader = IndeedJobItemLoader(selector=row)

            url_offer = row.xpath('.//div[@class="title"]/a/@href').extract_first()
            if url_offer:
                loader.add_value('url_offer', response.urljoin(url_offer))
            loader.add_xpath('title_offer', './/div[@class="title"]/a//text()')
            loader.add_xpath('synopsis_offer', './/div[has-class("summary")]//text()')
            loader.add_xpath('salary_offer', './/span[has-class("salary")]//text()')
            loader.add_xpath('city_offer', './/span[has-class("location")]/text()', re=r'^([^,]+)')
            loader.add_xpath('province_offer', './/span[has-class("location")]/text()', re=r'^[^,]+, (.+)')
            loader.add_xpath('source_offer', './/span[has-class("result-link-source")]/text()')
            loader.add_value('sponsored_offer', 'False')
            date = row.xpath('.//span[has-class("date")]/text()').extract_first(default='')
            loader.add_value('firstdate_offer', convert_date(date))
            loader.add_xpath('name_company', './/span[has-class("company")]//text()')

            item = loader.load_item()

            url_company = row.xpath('.//span[has-class("company")]/a/@href').extract_first(default='')
            if url_company:
                url_company = response.urljoin(url_company)
                id_company = ''
                m = re.search(r'/cmp/(.+?)/?$', url_company)
                if m:
                    id_company = m.group(1)
                if not id_company:
                    self.logger.warning('No ID found in company URL: %s', url_company)

                item['id_company'] = id_company

                # Add an additional request to scrape the company profile
                meta = {
                    'id_company': id_company,
                    'url_company': url_company,
                    'name_company': item.get('name_company', ''),
                }
                yield scrapy.Request(
                    url_company,
                    callback=self.parse_company,
                    meta=meta,
                )

            indeed_offer = row.xpath(u'.//span[has-class("iaLabel") and contains(text(), "Solicita fácilmente")]').extract_first()
            if indeed_offer:
                item['original_offer'] = 'True'
                if not item.get('url_offer'):
                    self.logger.warning("Offer listed as Indeed, but no link available (1): %s", response.url)
                    yield item
                else:
                    # Follow the link to get additional info
                    meta = {'item': item}
                    url = item['url_offer']
                    if re.search(r'/rc/clk\?.*jk=.+', url):
                        url = convert_job_url(item['url_offer'])
                    if url:
                        yield scrapy.Request(
                            url,
                            callback=self.parse_indeed_details,
                            errback=self.handle_details_error,
                            meta=meta,
                        )
                    else:
                        self.logger.warning("URL modification failed %s", item['url_offer'])
                        yield item
            else:
                item['original_offer'] = 'False'
                yield item

        # for row in response.xpath('//*[@id="resultsCol"]/div//div[has-class("row") and @data-jk]'):
        #     loader = IndeedJobItemLoader(selector=row)
        #
        #     url_offer = row.xpath('.//a[has-class("jobtitle")]/@href').extract_first()
        #     if url_offer:
        #         loader.add_value('url_offer', response.urljoin(url_offer))
        #     loader.add_xpath('title_offer', './/a[has-class("jobtitle")]/@title')
        #     loader.add_xpath('synopsis_offer', './/span[has-class("summary")]//text()')
        #     loader.add_xpath('salary_offer', './/div/span[has-class("no-wrap")]/text()', re=r'^(?:\s|\n)*\d[\d.]+.+')
        #     loader.add_xpath('city_offer', './/span[has-class("location")]/text()', re=r'^([^,]+)')
        #     loader.add_xpath('province_offer', './/span[has-class("location")]/text()', re=r'^[^,]+, (.+)')
        #     loader.add_xpath('source_offer', './/span[contains(text(), "Empleo patrocinado por")]/b/text()')
        #     loader.add_value('sponsored_offer', 'True')
        #     date = row.xpath('.//span[has-class("date")]/text()').extract_first(default='')
        #     loader.add_value('firstdate_offer', convert_date(date))
        #     loader.add_xpath('name_company', './/span[has-class("company")]//text()')
        #
        #     item = loader.load_item()
        #
        #     url_company = row.xpath('.//span[has-class("company")]/a/@href').extract_first(default='')
        #     if url_company:
        #         url_company = response.urljoin(url_company)
        #         id_company = ''
        #         m = re.search(r'/cmp/(.+?)/?$', url_company)
        #         if m:
        #             id_company = m.group(1)
        #         if not id_company:
        #             self.logger.error('No ID found in company URL: %s', url_company)
        #
        #         item['id_company'] = id_company
        #
        #         # Add an additional request to scrape the company profile
        #         meta = {
        #             'id_company': id_company,
        #             'url_company': url_company,
        #             'name_company': item.get('name_company', ''),
        #         }
        #         yield scrapy.Request(
        #             url_company,
        #             callback=self.parse_company,
        #             meta=meta,
        #         )
        #
        #     indeed_offer = row.xpath(u'.//span[has-class("iaLabel") and contains(text(), "Solicita fácilmente")]').extract_first()
        #     if indeed_offer:
        #         item['original_offer'] = 'True'
        #         if not item.get('url_offer'):
        #             self.logger.warning("Offer listed as Indeed, but no link available (2): %s", response.url)
        #             yield item
        #         else:
        #             # Follow the link to get additional info
        #             meta = {'item': item}
        #             url = item['url_offer']
        #             if re.search(r'/rc/clk\?.*jk=.+', url):
        #                 url = convert_job_url(item['url_offer'])
        #             if url:
        #                 if self.follow_job_url(url):
        #                     yield scrapy.Request(
        #                         url,
        #                         callback=self.parse_indeed_details,
        #                         errback=self.handle_details_error,
        #                         meta=meta,
        #                     )
        #             else:
        #                 self.logger.warning("URL modification failed %s", item['url_offer'])
        #                 yield item
        #     else:
        #         item['original_offer'] = 'False'
        #         yield item

        # Next page
        next_page = response.xpath('//div[has-class("pagination")]/a[.//span[contains(text(), "Siguiente")]]/@href').extract_first()
        if next_page:
            url = response.urljoin(next_page)
            if '?' in url and 'filter=' not in url:
                filter_param = w3lib.url.url_query_parameter(url, 'filter')
                if not filter_param:
                    # Add filter=0 to get omitted ads
                    # url = w3lib.url.add_or_replace_parameter(url, 'filter', '0')
                    url = url + '&filter=0'
            yield scrapy.Request(
                url,
                callback=self.parse_job_listing,
            )

        # # Similar omitted ads
        # omitted_ads = response.xpath('//p[has-class("dupetext")]/a/@href').extract_first()
        # if omitted_ads:
        #     url = response.urljoin(omitted_ads)
        #     yield scrapy.Request(
        #         url,
        #         callback=self.parse_job_listing,
        #     )

    def parse_company(self, response):
        id_company = response.meta['id_company']
        url_company = response.meta['url_company']
        name_company = response.meta['name_company']

        loader = IndeedCompanyItemLoader(response=response)

        loader.add_value('id_company', id_company)
        loader.add_value('url_company', url_company)
        loader.add_xpath('name_company', '//h2[@itemprop="name"]//text()')
        if not loader.get_output_value('name_company'):
            loader.add_value('name_company', name_company)


        loader.add_xpath('rating_company', '//div[@id="cmp-header-rating"]/span/text()')
        loader.add_xpath('valorations_company', '//a[contains(text(),"Valoraciones")]/div/text()')

        loader.add_xpath('rating_eq_company','//span[contains(text(),"Equilibrio vida personal y laboral")]/preceding-sibling::span[2]/text()')
        loader.add_xpath('rating_sal_company','//span[contains(text(),"Salario/Beneficios contractuales")]/preceding-sibling::span[2]/text() ')
        loader.add_xpath('rating_est_company','//span[contains(text(),"Estabilidad laboral")]/preceding-sibling::span[2]/text()')
        loader.add_xpath('rating_ges_company', u'//span[contains(text(),"Gestión" )]/preceding-sibling::span[2]/text()')
        loader.add_xpath('rating_cult_company', '//span[contains(text(),"Cultura")]/preceding-sibling::span[2]/text()')
        loader.add_xpath('numjobs_company', '//li[@data-tn-element="jobs-tab"]//div[has-class("cmp-note")]/text()')

        url = response.xpath('//li[@class="cmp-menu--about"]//a/@href').extract()
        if len(url) > 0:
            url = response.urljoin(url[0])
            yield scrapy.Request(url , callback=self.parse_company_more_info, meta=dict(itemloader=loader,))
        else:
            self.logger.warning(
                'unable to extract url from //li[@class="cmp-menu--about"]//a/@href: %s', response.url
            )

    def parse_company_more_info(self,response):

        loader = response.meta.get('itemloader', None)

        info_company = response.xpath('//div[@id="cmp-content"]/div[@id="cmp-about"]//text()').extract()
        loader.add_value('info_company', info_company)

        office_company = response.xpath('//dl[@id="cmp-company-details-sidebar"]/dt[contains(text(), "Oficina principal")]/following-sibling::dd[1]//text()').extract()
        loader.add_value('office_company',office_company)

        revenue_company = response.xpath('//dl[@id="cmp-company-details-sidebar"]/dt[contains(text(), "Ingresos")]/following-sibling::dd[1]//text()').extract()
        loader.add_value('revenue_company',revenue_company)

        employee_company = response.xpath('//dl[@id="cmp-company-details-sidebar"]/dt[contains(text(), "Empleados")]/following-sibling::dd[1]//text()').extract()
        loader.add_value('employee_company',employee_company)

        sector_company = response.xpath('//dl[@id="cmp-company-details-sidebar"]/dt[contains(text(), "Sector")]/following-sibling::dd[1]//text()').extract()
        loader.add_value('sector_company',sector_company)

        web_company = response.xpath('//dl[@id="cmp-company-details-sidebar"]/dt[contains(text(), "Enlaces")]/following-sibling::dd[1]//a/@href').extract()
        loader.add_value('web_company',web_company)


        item = loader.load_item()
        yield item


    def parse_indeed_details(self, response):
        item = response.meta['item']
        try:
            text = response.body_as_unicode()
        except AttributeError:
            self.logger.info("Binary response ignored: %s", response.url)
        else:
            self.logger.info("Parsing additional details: %s", response.url)

            loader = IndeedJobItemLoader(item=item, response=response)

            loader.add_xpath('description_offer', '//div[@id="jobDescriptionText"]//text()')
            desc_paragraph_selectors = response.xpath('//div[@id="jobDescriptionText"]//text()').extract()
            desc_paragraph_lines = [line for line in desc_paragraph_selectors]

            # loader.add_xpath('zip_code_offer', u'(//div[@id="jobDescriptionText"]//p[contains(text(), "Ubicación del empleo")]/following-sibling::ul)[1]//text()', re=r'(\d{5})')
            # loader.add_xpath('education_offer', u'(//div[@id="jobDescriptionText"]//p[contains(text(), "Educación requerida")]/following-sibling::ul)[1]//text()')
            # loader.add_xpath('experience_type_offer', u'(//div[@id="jobDescriptionText"]//p[contains(text(), "Experiencia requerida")]/following-sibling::ul)[1]//text()', re=r'^([^:]+):')
            # loader.add_xpath('experience_years_offer', u'(//div[@id="jobDescriptionText"]//p[contains(text(), "Experiencia requerida")]/following-sibling::ul)[1]//text()', re=r'(\d+) [Aa]')
            # loader.add_xpath('languages_offer', u'//div[@id="jobDescriptionText"]//b[contains(text(), "Idioma")]//following-sibling::text()')
            # loader.add_xpath('job_type_offer', '//div[@id="jobDescriptionText"]//p/text()', re=r'Tipo de puesto: (.+)')

            item = loader.load_item()
            item['url_offer'] = response.url

        yield item

    def handle_details_error(self, failure):
        request = failure.request
        item = request.meta['item']

        yield item

    def follow_job_url(self, url):
        if 'ad=' not in url:
            return True

        ad_key = w3lib.url.url_query_parameter(url, 'ad')
        if not ad_key:
            return True

        ad_key = ad_key[:149]
        if ad_key in AD_PARAM_CACHE:
            self.logger.info("Duplicate ad parameter detected: %s, avoiding request to: %s", ad_key, url)
            return False
        else:
            AD_PARAM_CACHE.add(ad_key)
            return True
